<?php
/**
 * Hooks for share socials
 *
 * @package Teckzone
 */

if ( ! function_exists( 'teckzone_addons_share_link_socials' ) ) :
	function teckzone_addons_share_link_socials( $socials, $title, $link, $media ) {
		$socials_html = '';
		if ( $socials ) {
			if ( in_array( 'facebook', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-facebook teckzone-facebook" title="%s" href="http://www.facebook.com/sharer.php?u=%s&t=%s" target="_blank"><i class="icon_social_facebook"></i></a></li>',
					esc_attr( $title ),
					urlencode( $link ),
					urlencode( $title )
				);
			}

			if ( in_array( 'twitter', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-twitter teckzone-twitter" href="http://twitter.com/share?text=%s&url=%s" title="%s" target="_blank"><i class="icon_social_twitter"></i></a></li>',
					urlencode( $title ),
					urlencode( $link ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'pinterest', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-pinterest teckzone-pinterest" href="http://pinterest.com/pin/create/button?media=%s&url=%s&description=%s" title="%s" target="_blank"><i class="icon_social_pinterest"></i></a></li>',
					urlencode( $media ),
					urlencode( $link ),
					urlencode( $title ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'linkedin', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-linkedin teckzone-linkedin" href="http://www.linkedin.com/shareArticle?url=%s&title=%s" title="%s" target="_blank"><i class="icon_social_linkedin"></i></a></li>',
					urlencode( $link ),
					urlencode( $title ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'google', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-google-plus teckzone-google-plus" href="https://plus.google.com/share?url=%s&text=%s" title="%s" target="_blank"><i class="icon_social_googleplus"></i></a></li>',
					urlencode( $link ),
					urlencode( $title ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'tumblr', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-tumblr teckzone-tumblr" href="http://www.tumblr.com/share/link?url=%s" title="%s" target="_blank"><i class="icon_social_tumblr"></i></a></li>',
					urlencode( $link ),
					esc_attr( $title )
				);
			}
		}

		if ( $socials_html ) {
			return sprintf( '<ul class="teckzone-social-share socials-inline">%s</ul>', $socials_html );
		}
		?>
		<?php
	}

endif;

