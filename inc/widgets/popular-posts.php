<?php

class Teckzone_Popular_Posts_Widget extends WP_Widget {
	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor
	 *
	 * @return Teckzone_Popular_Posts_Widget
	 */
	function __construct() {
		$this->defaults = array(
			'title' => '',
			'limit' => 4,
			'hightlight'  => 1,
		);

		parent::__construct(
			'popular-posts-widget',
			esc_html__( 'Teckzone - Popular Posts', 'teckzone' ),
			array(
				'classname'   => 'teckzone-popular-posts-widget',
				'description' => esc_html__( 'Advanced popular posts widget.', 'teckzone' )
			)
		);
	}

	/**
	 * Display widget
	 *
	 * @param array $args     Sidebar configuration
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		extract( $args );

		$query_args = array(
			'posts_per_page'      => intval( $instance['limit'] ),
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		if ( ! $query->have_posts() ) {
			return;
		}

		echo wp_kses_post( $before_widget );

		if ( $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) ) {
			echo wp_kses_post( $before_title ) . $title . wp_kses_post( $after_title );
		}

		$class = '';

		if ( $instance['hightlight'] ) {
			$class = 'post-hightlight';
		}

		echo '<div class="tz-popular-posts '. esc_attr( $class ) .'">';
		while ( $query->have_posts() ) : $query->the_post();
		$current = $query->current_post + 1;
			?>
			<div class="popular-item">
				<?php if( $instance['hightlight'] && $current == 1 ) : ?>

					<div class="popular-image">
						<?php
							if ( function_exists( 'teckzone_post_format_image' ) ) {
								echo teckzone_post_format_image('grid', 'teckzone-blog-grid');
							}
						?>
					</div>
					<div class="entry-box">
						<div class="entry-meta">
							<?php
								if ( function_exists( 'teckzone_meta_date' ) ) {
									echo teckzone_meta_date();
								}
							?>
						</div>
					<?php endif;?>
					<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h4>' ); ?>
					<?php if( $instance['hightlight'] && $current == 1 ) : ?>
					</div>
					<?php endif;?>
			</div>
			<?php
		endwhile;
		echo '</div>';
		wp_reset_postdata();

		echo wp_kses_post( $after_widget );

	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings
	 * @param array $old_instance Old widget settings
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$new_instance['title'] = strip_tags( $new_instance['title'] );
		$new_instance['limit'] = intval( $new_instance['limit'] );
		$new_instance['hightlight']  = ! empty( $new_instance['hightlight'] );

		return $new_instance;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>

		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" size="2"
				   value="<?php echo intval( $instance['limit'] ); ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php esc_html_e( 'Number Of Posts', 'teckzone' ); ?></label>
		</p>

		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'hightlight' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'hightlight' ) ); ?>" type="checkbox"
				   value="1" <?php checked( $instance['hightlight'] ); ?>>
			<label for="<?php echo esc_attr( $this->get_field_id( 'hightlight' ) ); ?>"><?php esc_html_e( 'Show Hightlight First Post', 'teckzone' ); ?></label>
		</p>
		<?php
	}
}
