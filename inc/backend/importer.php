<?php
/**
 * Hooks for importer
 *
 * @package Teckzone
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function teckzone_vc_addons_importer() {
	return array(
		array(
			'name'       => 'Home Marketplace',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-marketplace/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-marketplace/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-marketplace/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-marketplace/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Marketplace',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Local Shop',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-local-shop/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-local-shop/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-local-shop/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-local-shop/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Local Shop',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Wide',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-wide/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-wide/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-wide/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-wide/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Wide',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Full Width',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-full-width/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-full-width/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-full-width/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-full-width/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Full Width',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Sidebar',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-sidebar/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-sidebar/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-sidebar/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-sidebar/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Sidebar',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Categories',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-categories/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Categories',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Categories - Flash Sale',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-flash-sale/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-flash-sale/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-categories-flash-sale/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-flash-sale/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Categories - Flash Sale',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Categories - TV & Video',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tv/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tv/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tv/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tv/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Categories - TV & Video',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Categories - Cellphone',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-cellphone/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-cellphone/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-categories-cellphone/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-cellphone/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Categories - Cellphone',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Categories - Tablet',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tablet/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tablet/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tablet/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-tablet/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Categories - Tablet',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Home Categories - Computer',
			'preview'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-computer/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-computer/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/importer/teckzone/home-categories-computer/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/importer/teckzone/home-categories-computer/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Categories - Computer',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 300,
					'height' => 300,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
	);
}

add_filter( 'soo_demo_packages', 'teckzone_vc_addons_importer', 20 );

add_action( 'save_post', 'teckzone_woocommerce_clear_cache' );
add_action( 'wp_trash_post', 'teckzone_woocommerce_clear_cache' );
add_action( 'before_delete_post', 'teckzone_woocommerce_clear_cache' );
add_action( 'woocommerce_recorded_sales', 'teckzone_woocommerce_update_deal_sales' );
add_action( 'woocommerce_scheduled_sales', 'teckzone_woocommerce_clear_cache' );

function teckzone_woocommerce_clear_cache() {
	delete_transient( 'wc_layered_nav_counts_product_brand' );
	delete_transient( 'tz_woocommerce_cached_products' );
	delete_transient( 'tz_woocommerce_product_deals' );
}

function teckzone_woocommerce_update_deal_sales($order_id) {
	$order_post = get_post( $order_id );

	if( ! function_exists('tawc_is_deal_product') ) {
		return;
	}

	// Only apply for the main order
	if ( $order_post->post_parent != 0 ) {
		return;
	}

	$order = wc_get_order( $order_id );

	if ( sizeof( $order->get_items() ) > 0 ) {
		foreach ( $order->get_items() as $item ) {
			if ( $product_id = $item->get_product_id() ) {
				if ( ! tawc_is_deal_product( $product_id ) ) {
					continue;
				}

				$current_sales = get_post_meta( $product_id, '_deal_sales_counts', true );
				$deal_quantity = get_post_meta( $product_id, '_deal_quantity', true );
				$new_sales     = $current_sales + absint( $item['qty'] );

				// Reset deal sales and remove sale price when reach to limit sale quantity
				if ( $new_sales >= $deal_quantity ) {
					delete_transient( 'tz_woocommerce_product_deals' );
				}
			}
		}
	}
}
