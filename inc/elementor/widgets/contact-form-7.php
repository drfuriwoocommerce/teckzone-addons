<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Controls_Stack;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Contact_Form_7 extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-contact-form-7';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Teckzone - Contact Form 7', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-mail';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_control(
			'form',
			[
				'label'   => esc_html__( 'Contact Form 7', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => $this->get_contact_form(),
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->add_control(
			'description',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->end_controls_section();

		/**
		 * Style
		 */
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'general_align',
			[
				'label'       => esc_html__( 'Text Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'desktop_default' => 'center',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'toggle'          => false,
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors'   => [
							'{{WRAPPER}} .teckzone-contact-form-7 .form-title' => 'text-align: {{VALUE}};',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors'   => [
							'{{WRAPPER}} .teckzone-contact-form-7 .form-title' => 'text-align: {{VALUE}};',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors'   => [
							'{{WRAPPER}} .teckzone-contact-form-7 .form-title' => 'text-align: {{VALUE}};',
						],
					],
				]
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-contact-form-7 h2',
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-contact-form-7 h2' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'title_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-contact-form-7 h2' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();

		// Desc
		$this->start_controls_section(
			'section_desc_style',
			[
				'label' => __( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'desc_typography',
				'selector' => '{{WRAPPER}} .techzone-contact-form-7 .form-desc',
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-contact-form-7 .form-desc' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'desc_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-contact-form-7 .form-desc' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();

		// Form
		$this->start_controls_section(
			'section_form_style',
			[
				'label' => __( 'Form', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'height',
			[
				'label' => __( 'Textarea Field Height', 'teckzone' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 60,
						'max' => 300,
					],
				],
				'default' => [
					'size' => 107,
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-contact-form-7 textarea' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'form_style',
			[
				'label'       => esc_html__( 'Style', 'teckzone' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'default' => esc_html__( 'Style 1', 'teckzone' ),
					'style-2'  => esc_html__( 'Style 2', 'teckzone' ),
				],
				'default'     => 'default',
				'label_block' => true,
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'techzone-contact-form-7 teckzone-contact-form-7',
			$settings['form_style'] == 'style-2' ? 'tz-form-style-2' : ''
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );


		$title = $settings['title'] ? sprintf('<h2 class="form-title">%s</h2>', $settings['title']) : '';
		$desc = $settings['description'] ? sprintf('<div class="form-desc">%s</div>', $settings['description']) : '';

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php echo $title . $desc; ?>
			<?php if ( $settings['form'] ) :
				echo do_shortcode( '[contact-form-7 id="' . esc_attr( $settings['form'] ) . '"]' );
			endif; ?>
		</div>
		<?php
	}

	/**
	 * Render widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Get Contact Form
	 */
	protected function get_contact_form() {
		$mail_forms    = get_posts( 'post_type=wpcf7_contact_form&posts_per_page=-1' );
		$mail_form_ids = array(
			'' => esc_html__( 'Select Form', 'teckzone' ),
		);
		foreach ( $mail_forms as $form ) {
			$mail_form_ids[$form->ID] = $form->post_title;
		}

		return $mail_form_ids;
	}
}