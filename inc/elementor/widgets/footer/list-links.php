<?php

namespace TeckzoneAddons\Elementor\Widgets\Footer;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * List Links widget
 */
class List_Links extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-list-links';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Links', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-link';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_footer' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
		$this->collapse_effect();
	}

	// Content
	protected function section_content() {
		$this->section_tag_content();
	}

	protected function section_tag_content() {

		$this->start_controls_section(
			'section_images',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the title', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'separator'   => 'after',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'name',
			[
				'label'       => esc_html__( 'Name', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'tag', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your name link', 'teckzone' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$repeater->add_control(
			'css_classes',
			[
				'label'        => __( 'CSS Classes', 'teckzone' ),
				'type'         => Controls_Manager::TEXT,
				'default'      => '',
				'dynamic'      => [
					'active' => true,
				],
				'prefix_class' => '',
				'title'        => __( 'Add your custom class WITHOUT the dot. e.g: my-class', 'teckzone' ),
				'label_block'  => false,
				'classes'      => 'elementor-control-direction-ltr',
			]
		);

		$this->add_control(
			'group_setting',
			[
				'label'         => esc_html__( 'Links Group', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'name' => __( 'Link #1', 'teckzone' ),
					], [
						'name' => __( 'Link #2', 'teckzone' ),
					], [
						'name' => __( 'Link #3', 'teckzone' ),
					], [
						'name' => __( 'Link #4', 'teckzone' ),
					], [
						'name' => __( 'Link #5', 'teckzone' ),
					], [
						'name' => __( 'Link #6', 'teckzone' ),
					], [
						'name' => __( 'Link #7', 'teckzone' ),
					],
				],
				'title_field'   => '{{{ name }}}',
				'prevent_empty' => false,
			]
		);

		$this->end_controls_section();
	}

	protected function collapse_effect() {
		$this->start_controls_section(
			'section_collapse_effect',
			[ 'label' => esc_html__( 'Collapse Effect', 'teckzone' ) ]
		);
		$this->add_control(
			'enable_collapse_effect',
			[
				'label'        => esc_html__( 'Enable', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_off'    => esc_html__( 'Off', 'teckzone' ),
				'label_on'     => esc_html__( 'On', 'teckzone' ),
				'return_value' => 'yes',
				'default'      => ''
			]
		);
		$this->add_control(
			'collapse_effect_on',
			[
				'label'       => __( 'Collapse Effect On', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'multiple'    => true,
				'label_block' => 'true',
				'default'     => [ 'mobile' ],
				'options'     => [
					'desktop' => __( 'Desktop', 'teckzone' ),
					'tablet'  => __( 'Tablet', 'teckzone' ),
					'mobile'  => __( 'Mobile', 'teckzone' ),
				]
			]
		);
		$this->add_control(
			'content_status',
			[
				'label'        => esc_html__( 'Content', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_off'    => esc_html__( 'Close', 'teckzone' ),
				'label_on'     => esc_html__( 'Open', 'teckzone' ),
				'return_value' => 'yes',
				'default'      => 'yes',
				'separator'    => 'before'
			]
		);
		$this->end_controls_section();
	}

	// Style
	protected function section_style() {
		$this->section_style_content();
		$this->section_style_title();
		$this->section_style_name();
	}

	protected function section_style_name() {
		// Item
		$this->start_controls_section(
			'section_item_style',
			[
				'label' => esc_html__( 'Links', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'tag_typography',
				'selector'  => '{{WRAPPER}} .teckzone-list-links .name',
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-list-links ul li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'tag_content_settings' );

		$this->start_controls_tab( 'tab_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'name_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-list-links .name' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'tab_hover', [ 'label' => esc_html__( 'Hover', 'teckzone' ) ] );

		$this->add_control(
			'name_color_hover',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-list-links .name:hover' => 'color: {{VALUE}}; box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_style_title() {
		// Title
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-list-links .title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'heading_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-list-links .title' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_typography',
				'selector' => '{{WRAPPER}} .teckzone-list-links .title',
			]
		);

		$this->add_responsive_control(
			'heading_icon_size',
			[
				'label'     => esc_html__( 'Icon Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-list-links .title .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}}',
				],
				'separator' => 'before'
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_content() {
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'content_align',
			[
				'label'     => esc_html__( 'Align', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-list-links' => 'text-align: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-list-links' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator'  => 'before',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */

	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-list-links',
		];

		$data_effect = [
			'collapse_effect'    => $settings['enable_collapse_effect'],
			'collapse_effect_on' => $settings['collapse_effect_on'],
			'content_status'     => $settings['content_status'],
		];

		$this->add_render_attribute(
			'wrapper',
			[
				'class' => $classes,
				'data-effect' => wp_json_encode($data_effect)
			]
		);

		$icon = $settings['enable_collapse_effect'] == 'yes' ?'<span class="teckzone-icon"><i aria-hidden="true" class="icon-chevron-down"></i></span>' : '';

		$title = $settings['title'] ? sprintf( '<h6 class="title">%s%s</h6>', $settings['title'],$icon ) : '';

		$output = [ ];

		$btn_settings = $settings['group_setting'];

		if ( ! empty ( $btn_settings ) ) {

			$output[] = '<ul>';

			foreach ( $btn_settings as $index => $item ) {
				$this->add_render_attribute(
					'item_' . $index,
					[
						'class' => $item['css_classes']
					]
				);

				$link_key = 'link_' . $index;
				$tag_html = $this->get_link_control( $link_key, $item['link'], $item['name'], [ 'class' => 'name' ] );

				$output[] = '<li ' . $this->get_render_attribute_string( 'item_' . $index ) . '>' . $tag_html . '</li>';
			}

			$output[] = '</ul>';
		}

		$this->add_render_attribute( 'list_wrapper', 'class', [
			'list-wrapper'
		] );

		echo sprintf(
			'<div %s>%s<div %s>%s</div></div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$title,
			$this->get_render_attribute_string( 'list_wrapper' ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Render link control output
	 *
	 * @param       $link_key
	 * @param       $url
	 * @param       $content
	 * @param array $attr
	 *
	 * @return string
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}