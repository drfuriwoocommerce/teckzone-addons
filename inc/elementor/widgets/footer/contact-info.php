<?php

namespace TeckzoneAddons\Elementor\Widgets\Footer;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 *  Contact Info widget
 */
class Contact_Info extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-contact-info';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Contact Info', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-info-circle-o';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_footer' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the title', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'separator' => 'after',
			]
		);

		$this->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => esc_html__( 'This is the description.', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your description', 'teckzone' ),
				'rows'        => 10,
			]
		);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_list',
			[ 'label' => esc_html__( 'List Information', 'teckzone' ) ]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->start_controls_tabs( 'ele_repeater' );
		$repeater->start_controls_tab( 'content_re', [ 'label' => esc_html__( 'Content', 'teckzone' ) ] );

		$repeater->add_control(
			'icon_li',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
			]
		);

		$repeater->add_control(
			'desc_li',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the description', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your description', 'teckzone' ),
			]
		);

		$repeater->add_control(
			'after_desc',
			[
				'label'       => esc_html__( 'After Description', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'This is the after description', 'teckzone' ),
			]
		);

		$repeater->end_controls_tab();

		$repeater->start_controls_tab( 'style_re', [ 'label' => esc_html__( 'Style', 'teckzone' ) ] );

		$repeater->add_responsive_control(
			'licus_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info {{CURRENT_ITEM}}' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
				'separator'  => 'after',
			]
		);

		$repeater->add_control(
			'custom_style',
			[
				'label'       => esc_html__( 'Custom', 'teckzone' ),
				'type'        => Controls_Manager::SWITCHER,
				'description' => esc_html__( 'Set custom style that will only icon style.', 'teckzone' ),
			]
		);

		$repeater->add_responsive_control(
			'cusicon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info {{CURRENT_ITEM}} .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-contact-info {{CURRENT_ITEM}} .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'cusicon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info {{CURRENT_ITEM}} .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-contact-info {{CURRENT_ITEM}} .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'cusicon_margin',
			[
				'label'              => esc_html__( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-contact-info {{CURRENT_ITEM}} .teckzone-icon' => 'margin-top: {{TOP}}{{UNIT}};margin-right: {{RIGHT}}{{UNIT}}',
				],
				'separator'  => 'before',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->end_controls_tab();
		$repeater->end_controls_tabs();

		$this->add_control(
			'element',
			[
				'label'         => '',
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'icon_li'         => array(
							'value'   => 'icon-home2',
							'library' => 'linearicons',
						),
						'desc_li'         => esc_html__( 'This is the desc', 'teckzone' ),
						'after_desc'      => '',
					],[
						'icon_li'         => array(
							'value'   => 'icon-telephone',
							'library' => 'linearicons',
						),
						'desc_li'         => esc_html__( 'This is the desc', 'teckzone' ),
						'after_desc'      => esc_html__( 'This is the phone', 'teckzone' ),
					],[
						'icon_li'         => array(
							'value'   => 'icon-envelope',
							'library' => 'linearicons',
						),
						'desc_li'         => esc_html__( 'This is the desc', 'teckzone' ),
						'after_desc'      => '',
					],
				],
				'prevent_empty' => false
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_content_style();
		$this->section_list_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * General
	 */
	protected function section_general_style() {
		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'genera_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-contact-info' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function section_content_style() {
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'settings_style' );

		$this->start_controls_tab( 'style_content_title', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_typography',
				'selector' => '{{WRAPPER}} .teckzone-contact-info .title',
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'title_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
				'separator'  => 'before',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_content_desc', [ 'label' => esc_html__( 'Description', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_desc_typography',
				'selector' => '{{WRAPPER}} .teckzone-contact-info > .desc',
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info > .desc' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'desc_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'separator'  => 'before',
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info > .desc' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_list_style() {
		$this->start_controls_section(
			'section_list_style',
			[
				'label' => __( 'List Information', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'list_spacing',
			[
				'label'     => esc_html__( 'List Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .box-item--wrapper' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'settings_list' );

		$this->start_controls_tab( 'style_list_icon', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-contact-info .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-contact-info .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'icon_margin',
			[
				'label'              => esc_html__( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-contact-info .teckzone-icon' => 'margin-top: {{TOP}}{{UNIT}};margin-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_list_desc', [ 'label' => esc_html__( 'Description', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_lidesc_typography',
				'selector' => '{{WRAPPER}} .teckzone-contact-info .box-desc .desc',
			]
		);

		$this->add_control(
			'lidesc_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .box-desc .desc' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_list_phone', [ 'label' => esc_html__( 'After', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_phone_typography',
				'selector' => '{{WRAPPER}} .teckzone-contact-info .after-desc',
			]
		);

		$this->add_control(
			'phone_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-contact-info .after-desc' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-contact-info',
			]
		);

		$title  = $desc = '';

		if ( $settings['title'] ) {
			$title = '<h5 class="title">' . $settings['title'] . '</h5>';
		}

		if ( $settings['desc'] ) {
			$desc = '<div class="desc">' . $settings['desc'] . '</div>';
		}

		$element = $settings['element'];

		$output = [ ];

		if ( ! empty ( $element ) ) {
			$output[] = '<ul class="list-infor">';

			foreach ( $element as $index => $item ) {

				$icon   = $desc_li = $desc_after = '';
				if ( $item['icon_li'] && ! empty($item['icon_li']['value']) && \Elementor\Icons_Manager::is_migration_allowed() ) {
					ob_start();
					\Elementor\Icons_Manager::render_icon( $item['icon_li'], [ 'aria-hidden' => 'true' ] );
					$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
				}

				if ( $item['desc_li'] && ! empty($item['desc_li']) ) {
					$desc_li = '<div class="desc">' . $item['desc_li'] . '</div>';
				}

				if ( $item['after_desc'] && ! empty($item['after_desc']) ) {
					$desc_after = '<div class="after-desc">' . $item['after_desc'] . '</div>';
				}

				$box_desc = sprintf('<div class="box-desc">%s %s </div>',$desc_li,$desc_after);

				$output[] = sprintf('<li class="box-item--wrapper elementor-repeater-item-%s">%s %s</li>',$item['_id'],$icon,$box_desc);
			}

			$output[] = '</ul>';

		}

		echo sprintf(
			'<div %s>%s %s %s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$title,
			$desc,
			implode('',$output)
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}