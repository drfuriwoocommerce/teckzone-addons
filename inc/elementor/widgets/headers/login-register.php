<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Controls_Stack;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Login_Register extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-login-regis';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Login Register', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-lock-user';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'content_user_login' );

		$this->start_controls_tab( 'content_not_login', [ 'label' => esc_html__( 'Not Logged In', 'teckzone' ) ] );

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-user',
					'library' => 'linearicons',
				],
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'content_login', [ 'label' => esc_html__( 'Logged in', 'teckzone' ) ] );

		$this->add_control(
			'preamble',
			[
				'label'       => esc_html__( 'Preamble', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Hi', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'icon_logged',
			[
				'label'   => esc_html__( 'Icon Logout', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-enter-left',
					'library' => 'linearicons',
				],
			]
		);

		$this->add_control(
			'enable_preamble',
			[
				'label'     => esc_html__( 'Enable Preamble', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'teckzone' ),
				'label_on'  => esc_html__( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'custom_account_menu',
			[
				'label'        => esc_html__( 'Custom Account Menu', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Yes', 'teckzone' ),
				'label_off'    => __( 'No', 'teckzone' ),
				'return_value' => 'yes',
				'default'      => '',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'link_name',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_custom', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'group_setting',
			[
				'label'         => esc_html__( 'Links Group', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_name'   => __( 'Link #1', 'teckzone' ),
						'link_custom' => [
							'url' => '#',
						],
					], [
						'link_name'   => __( 'Link #2', 'teckzone' ),
						'link_custom' => [
							'url' => '#',
						],
					], [
						'link_name'   => __( 'Link #3', 'teckzone' ),
						'link_custom' => [
							'url' => '#',
						],
					],
				],
				'title_field'   => '{{{ link_name }}}',
				'prevent_empty' => false,
				'condition'     => [
					'custom_account_menu' => 'yes',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_not_loggin_style();
		$this->section_loggin_style();
		$this->section_drop_style();
	}

	protected function section_general_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		// Separator
		$this->add_control(
			'sep_style',
			[
				'label'        => __( 'Separator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_responsive_control(
			'item_sep',
			[
				'label'                => esc_html__( 'Separator', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Show', 'teckzone' ),
				'label_off'            => esc_html__( 'Hide', 'teckzone' ),
				'return_value'         => 'yes',
				'default'              => '',
				'selectors_dictionary' => [
					'' => 'display: none',
				],
				'selectors'   => [
					'{{WRAPPER}} .teckzone-login-regis:after' => '{{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'sep_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units' => [ '%', 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-login-regis:after' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'sep_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis:after' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * General
	 */

	protected function section_not_loggin_style() {
		$this->start_controls_section(
			'section_not_loggin_style',
			[
				'label' => __( 'Not Logged in', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'notlog_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-not-login' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'log_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-not-login' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Elements
		$this->add_control(
			'login_elements',
			[
				'label'     => __( 'Elements', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'settings_not_loggin' );

		$this->start_controls_tab( 'style_icon_not_log', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );

		$this->add_responsive_control(
			'icon_notlog_spacing',
			[
				'label'              => esc_html__( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-not-login .user-icon' => 'margin-top: {{TOP}}{{UNIT}};margin-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_notlog_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-not-login .user-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-not-login .user-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_notlog_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-not-login .user-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-not-login .user-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_title_not_log', [ 'label' => esc_html__( 'Text', 'teckzone' ) ] );
		$this->add_responsive_control(
			'not_login_title_alignment',
			[
				'label'           => esc_html__( 'Content Align', 'teckzone' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'         => [
					'row'    => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'column' => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'desktop_default' => 'row',
				'tablet_default'  => 'row',
				'mobile_default'  => 'row',
				'toggle'          => false,
				'selectors'       => [
					'{{WRAPPER}} .teckzone-not-login .title' => 'flex-direction: {{VALUE}}',
				],
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-not-login .title' => 'flex-direction: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-not-login .title' => 'flex-direction: {{VALUE}}',
						],
					],
				]
			]
		);
		$this->add_responsive_control(
			'register_text_css',
			[
				'type' => Controls_Manager::HIDDEN,
				'desktop_default' => 'remove',
				'tablet_default' => 'remove',
				'mobile_default' => 'remove',
				'condition' => [
					'not_login_title_alignment' => 'column',
				],
				'selectors_dictionary' => [
					'remove' => 'display: none',
				],
				'device_args' => [
					Controls_Stack::RESPONSIVE_TABLET => [
						'condition' => [
							'not_login_title_alignment_tablet' => 'column',
						],
						'selectors' => [
							'{{WRAPPER}} .teckzone-not-login .register-text .sep' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'condition' => [
							'not_login_title_alignment_mobile' => 'column',
						],
						'selectors' => [
							'{{WRAPPER}} .teckzone-not-login .register-text .sep' => '{{VALUE}}',
						],
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-not-login .register-text .sep' => '{{VALUE}}',
				],
			]
		);
		$this->add_control(
			'login_text_options',
			[
				'label' => __( 'Login Text', 'teckzone' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Typography', 'teckzone' ),
				'name'     => 'bf_title_not_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .login-text',
			]
		);

		$this->add_control(
			'login_text_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .login-text' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'register_text_options',
			[
				'label' => __( 'Register Text', 'teckzone' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Typography', 'teckzone' ),
				'name'     => 'register_text_typo',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .register-text',
			]
		);

		$this->add_control(
			'register_text_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .register-text' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'register_text_top_spacing',
			[
				'label'     => __( 'Top spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'size_units' => [ 'px' ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-not-login .register-text'     => 'padding-top: {{SIZE}}{{UNIT}}',
				],
				'condition' => [ 
					'not_login_title_alignment' => 'column' 
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_loggin_style() {
		$this->start_controls_section(
			'section_loggin_style',
			[
				'label' => __( 'Logged in', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'log_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-logged-in' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'logaccount_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-logged-in .login-account' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Elements
		$this->add_control(
			'elements',
			[
				'label'     => __( 'Elements', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->start_controls_tabs( 'settings_loggin' );

		$this->start_controls_tab( 'style_icon_loged', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );

		$this->add_responsive_control(
			'icon_loged_spacing',
			[
				'label'              => esc_html__( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-logged-in .user-icon' => 'margin-top: {{TOP}}{{UNIT}};margin-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_loged_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-logged-in .user-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-logged-in .user-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_loged_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-logged-in .user-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-logged-in .user-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Pre
		$this->start_controls_tab( 'style_preamble_log', [ 'label' => esc_html__( 'Preamble', 'teckzone' ) ] );
		$this->add_responsive_control(
			'login_text_alignment',
			[
				'label'           => esc_html__( 'Content Align', 'teckzone' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'         => [
					'row'    => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'column' => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'desktop_default' => 'row',
				'tablet_default'  => 'row',
				'mobile_default'  => 'row',
				'toggle'          => false,
				'selectors'       => [
					'{{WRAPPER}} .teckzone-logged-in .preamble' => 'flex-direction: {{VALUE}}',
				],
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-logged-in .preamble' => 'flex-direction: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-logged-in .preamble' => 'flex-direction: {{VALUE}}',
						],
					],
				]
			]
		);

		$this->add_responsive_control(
			'preamble_spacing',
			[
				'label'     => esc_html__( 'Left Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .preamble' => 'margin-left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Typography', 'teckzone' ),
				'name'     => 'preamble_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .preamble',
			]
		);

		$this->add_control(
			'preamble_log_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .preamble' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'author_options',
			[
				'label' => __( 'Author', 'teckzone' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Typography', 'teckzone' ),
				'name'     => 'author_typo',
				'selector' => '{{WRAPPER}} .teckzone-logged-in .preamble .login-author',
			]
		);
		
		$this->add_control(
			'author_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-logged-in .preamble .login-author' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'author_top_spacing',
			[
				'label'     => __( 'Top spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'size_units' => [ 'px' ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'condition' => [ 
					'login_text_alignment' => 'column' 
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-logged-in .preamble .login-author'     => 'padding-top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();

		// Dropdown
		$this->start_controls_tab( 'style_links_log', [ 'label' => esc_html__( 'Dropdown', 'teckzone' ) ] );
		$this->add_control(
			'normal_items_style',
			[
				'label' => __( 'Normal Items', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Typography', 'teckzone' ),
				'name'     => 'link_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis li:not(.logout) a',
			]
		);

		$this->add_control(
			'link_log_color',
			[
				'label'     => esc_html__( 'Title Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis li:not(.logout)' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'link_log_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis li:not(.logout)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'logout_item',
			[
				'label'     => __( 'Logout Item', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Text', 'teckzone' ),
				'name'     => 'title_logout',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .logout a',
			]
		);

		$this->add_control(
			'title_logout_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'title_logout_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .header-account--content .logout' => 'padding-top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		// Icon
		$this->add_control(
			'logout_item_icon',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'icon_log_spacing',
			[
				'label'     => __( 'Icon Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'icon_log_size',
			[
				'label'     => __( 'Icon Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_log_color',
			[
				'label'     => __( 'Icon Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_drop_style() {
		$this->start_controls_section(
			'style_drop',
			[
				'label' => __( 'Drop Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'link_distance',
			[
				'label'     => esc_html__( 'Distance to top', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .header-account' => 'border-top-width: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-logged-in:before'            => 'top: calc( 120% + {{SIZE}}{{UNIT}} )',
					'{{WRAPPER}} .teckzone-logged-in:hover:before'      => 'top: calc( 100% + {{SIZE}}{{UNIT}} )',
				],
			]
		);

		$this->end_controls_section();

	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-login-regis',
			]
		);

		if ( is_user_logged_in() ) {
			$this->add_render_attribute(
				'wrapper', 'class', [
					'teckzone-logged-in',
				]
			);

			$output_html = $this->login( $settings );
		} else {
			$this->add_render_attribute(
				'wrapper', 'class', [
					'teckzone-not-login',
				]
			);

			$output_html = $this->logout( $settings );
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$output_html
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}

	/**
	 * Login HTML
	 *
	 * @param $settings
	 *
	 * @return string
	 */
	protected function login( $settings ) {
		$icon_logged = $icon = '';

		if ( $settings['icon_logged'] && ! empty( $settings['icon_logged']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon_logged'], [ 'aria-hidden' => 'true' ] );
			$icon_logged = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="user-icon teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$output = [ ];

		$user_id = get_current_user_id();
		$author  = get_user_by( 'id', $user_id );

		$account      = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );
		$account_link = $account;

		if ( function_exists( 'dokan_get_navigation_url' ) && in_array( 'seller', $author->roles ) ) {
			$account_link = dokan_get_navigation_url();
		} elseif ( class_exists( 'WCVendors_Pro' ) && in_array( 'vendor', $author->roles ) ) {
			$dashboard_page_id = get_option( 'wcvendors_dashboard_page_id' );
			$dashboard_page_id = is_array( $dashboard_page_id ) ? $dashboard_page_id[0] : $dashboard_page_id;
			if ( $dashboard_page_id ) {
				$account_link = get_permalink( $dashboard_page_id );
			}

		} elseif ( class_exists( 'WC_Vendors' ) && in_array( 'vendor', $author->roles ) ) {
			$vendor_dashboard_page = get_option( 'wcvendors_vendor_dashboard_page_id' );
			$account_link          = get_permalink( $vendor_dashboard_page );

		} elseif ( class_exists( 'WCMp' ) && in_array( 'dc_vendor', $author->roles ) ) {
			if ( function_exists( 'wcmp_vendor_dashboard_page_id' ) && wcmp_vendor_dashboard_page_id() ) {
				$account_link = get_permalink( wcmp_vendor_dashboard_page_id() );
			}
		} elseif ( function_exists( 'wcfm_is_vendor' ) && wcfm_is_vendor() ) {
			$pages = get_option( "wcfm_page_options" );
			if ( isset( $pages['wc_frontend_manager_page_id'] ) && $pages['wc_frontend_manager_page_id'] ) {
				$account_link = get_permalink( $pages['wc_frontend_manager_page_id'] );
			}
		}

		$elements = $settings['group_setting'];

		$output[] = '<ul class="header-account--content">';
		if ( $settings['custom_account_menu'] == 'yes' && ! empty ( $elements ) ) {

			foreach ( $elements as $index => $item ) {
				$link_name = isset( $item['link_name'] ) && ! empty( $item['link_name'] ) ? sprintf( '<span class="link-name">%s</span>', $item['link_name'] ) : '';

				$link_key  = 'link_' . $index;
				$link_html = $item['link_custom']['url'] ? sprintf( '<li>%s</li>', $this->get_link_control( $link_key, $item['link_custom'], $link_name, '' ) ) : sprintf( '<li>%s</li>', $link_name );

				$output[] = $link_html;
			}
		} else {
			$user_menu = $this->nav_vendor_menu();
			if ( empty( $user_menu ) ) {
				$user_menu = $this->nav_user_menu();
			}

			$output[] = implode( '', $user_menu );
		}

		$output[] = '<li class="logout"><a href="' . esc_url( wp_logout_url( $account ) ) . '">' . $icon_logged . esc_html__( 'Logout', 'teckzone' ) . '</a></li>';
		$output[] = '</ul>';

		$name_user   = sprintf( 
			'<span class="preamble"><span class="preamble-text">%s,</span><span class="login-author">%s</span></span>', 
			esc_html__('Hi','teckzone'),
			$author->display_name 
		);
		$a = sprintf( '<a href="%s" class="login-account">%s %s</a>', $account_link, $icon, $name_user );

		return sprintf( '%s<div class="header-account">%s</div>', $a, implode( '', $output ) );
	}

	/**
	 * Logout HTML
	 *
	 * @param $settings
	 *
	 * @return string
	 */

	protected function logout( $settings ) {
		$icon = $title = '';

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="user-icon teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$title = sprintf( 
			'<span class="title">
				<span class="login-text">%s </span>
				<span class="register-text"><span class="sep">%s </span>%s</span>
			</span>',
			esc_html__('Login','teckzone'),
			esc_html__('or','teckzone'),
			esc_html__( 'Register', 'teckzone' )
		);

		$content = $icon . $title;

		$account_url = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );

		if ( !empty($settings['link']['url']) ) {
			$output = $this->get_link_control( 'link', $settings['link'], $content, '' );
		} else {
			$output = sprintf('<a href="%s">%s</a>',$account_url,$content);
		}

		return $output;
	}

	/**
	 * Get Link Control
	 *
	 * @param       $link_key
	 * @param       $url
	 * @param       $content
	 * @param array $attr
	 *
	 * @return string
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$tag = 'a';

		if ( empty( $attr_default['href'] ) ) {
			$tag = 'span';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $this->get_render_attribute_string( $link_key ), $content );
	}

	/**
	 * Get Vendor Navigation
	 *
	 * @return array
	 */
	protected function vendor_navigation_url() {
		$author = get_user_by( 'id', get_current_user_id() );
		$vendor = array();
		$vendor[] = sprintf(
			'<li class="label"><h3>%s</h3></li>',
			esc_html__( 'Vendor Settings', 'teckzone' )
		);
		$vendor[] = '<li>';
		$vendor[] = '<ul>';
		if ( function_exists( 'dokan_get_navigation_url' ) && in_array( 'seller', $author->roles ) ) {
			$vendor[] = sprintf( '<li><a href="%s">%s</a></li>', esc_url( dokan_get_navigation_url() ), esc_html__( 'Dashboard', 'teckzone' ) );
			$vendor[] = sprintf( '<li><a href="%s">%s</a></li>', esc_url( dokan_get_navigation_url( 'products' ) ), esc_html__( 'Products', 'teckzone' ) );
			$vendor[] = sprintf( '<li><a href="%s">%s</a></li>', esc_url( dokan_get_navigation_url( 'orders' ) ), esc_html__( 'Orders', 'teckzone' ) );
			$vendor[] = sprintf( '<li><a href="%s">%s</a></li>', esc_url( dokan_get_navigation_url( 'edit-account' ) ), esc_html__( 'Settings', 'teckzone' ) );
			if ( function_exists( 'dokan_get_store_url' ) ) {
				$vendor[] = sprintf( '<li><a href="%s">%s</a></li>', esc_url( dokan_get_store_url( get_current_user_id() ) ), esc_html__( 'Visit Store', 'teckzone' ) );
			}
			$vendor[] = sprintf( '<li><a href="%s">%s</a></li>', esc_url( dokan_get_navigation_url( 'withdraw' ) ), esc_html__( 'Withdraw', 'teckzone' ) );
		}

		$vendor[] = '</ul>';
		$vendor[] = '</li>';

		return $vendor;
	}

	/**
	 * Vendor Menu
	 *
	 * @return array
	 */
	protected function nav_vendor_menu() {
		$author      = get_user_by( 'id', get_current_user_id() );
		$vendor_menu = array();

		if ( ! in_array( 'vendor', $author->roles ) && ! in_array( 'seller', $author->roles )
			&& ! in_array( 'dc_vendor', $author->roles ) && ! in_array( 'wcfm_vendor', $author->roles )
		) {
			return $vendor_menu;
		}

		$vendor_menu = $this->vendor_navigation_url();

		return $vendor_menu;
	}

	/**
	 * User Menu
	 *
	 * @return array
	 */
	protected function nav_user_menu() {
		$user_id = get_current_user_id();
		$author  = get_user_by( 'id', $user_id );

		$user_menu = array();

		$orders  = get_option( 'woocommerce_myaccount_orders_endpoint', 'orders' );
		$account = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );
		if ( substr( $account, - 1, 1 ) != '/' ) {
			$account .= '/';
		}
		$orders   = $account . $orders;
		$wishlist = '';
		if ( shortcode_exists( 'yith_wishlist_constructor' ) ) {
			$wishlist = sprintf(
				'<li>
					<a href="%s">%s</a>
				</li>',
				esc_url( get_permalink( get_option( 'yith_wcwl_wishlist_page_id' ) ) ),
				esc_html__( 'My Wishlist', 'teckzone' )
			);
		}

		$user_menu[] = sprintf(
			'<li class="label"><h3>%s</h3></li>',
			esc_html__( 'Hi,', 'teckzone' ) . ' ' . $author->display_name . '!'
		);

		$user_menu[] = sprintf(
			'<li>
				<ul>
					<li>
						<a href="%s">%s</a>
					</li>
					<li>
						<a href="%s">%s</a>
					</li>
					<li>
						<a href="%s">%s</a>
					</li>
					%s
				</ul>
			</li>',
			esc_url( $account ),
			esc_html__( 'Dashboard', 'teckzone' ),
			esc_url( $account . get_option( 'woocommerce_myaccount_edit_account_endpoint', 'edit-account' ) ),
			esc_html__( 'Account Settings', 'teckzone' ),
			esc_url( $orders ),
			esc_html__( 'Orders History', 'teckzone' ),
			$wishlist
		);

		return $user_menu;
	}
}