<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Menu_Department extends Widget_Base {

	public function get_name() {
		return 'techzone-menu-department';
	}

	public function get_title() {
		return esc_html__( 'Teckzone - Menu Department', 'teckzone' );
	}

	public function get_icon() {
		return 'eicon-nav-menu';
	}

	public function get_keywords() {
		return [ 'menu', 'department' ];
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected $nav_menu_index = 1;

	protected function get_nav_menu_index() {
		return $this->nav_menu_index ++;
	}

	private function get_available_menus() {
		$menus = wp_get_nav_menus();

		$options = [ ];

		foreach ( $menus as $menu ) {
			$options[$menu->slug] = $menu->name;
		}

		return $options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'menu_department_section',
			[
				'label' => esc_html__( 'Menu Department', 'teckzone' ),
			]
		);

		$menus = $this->get_available_menus();

		if ( ! empty( $menus ) ) {
			$this->add_control(
				'menu',
				[
					'label'        => __( 'Menu', 'teckzone' ),
					'type'         => Controls_Manager::SELECT,
					'options'      => $menus,
					'default'      => array_keys( $menus )[0],
					'save_default' => true,					
					'description'  => sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'teckzone' ), admin_url( 'nav-menus.php' ) ),
				]
			);
		} else {
			$this->add_control(
				'menu',
				[
					'type'            => Controls_Manager::RAW_HTML,
					'raw'             => '<strong>' . __( 'There are no menus in your site.', 'teckzone' ) . '</strong><br>' . sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'teckzone' ), admin_url( 'nav-menus.php?action=edit&menu=0' ) ),
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
				]
			);
		}
		$this->add_control(
			'menu_dropdown',
			[
				'label'   => esc_html__( 'Open on Homepage', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'hover',
				'options' => [
					'hover'  => esc_html__( 'While Hover', 'teckzone' ),
					'always' => esc_html__( 'Always', 'teckzone' ),
				],
			]
		);
		$this->add_control(
			'department',
			[
				'label'                => esc_html__( 'Department Type', 'teckzone' ),
				'type'                 => Controls_Manager::SELECT,
				'default'              => 'icon',
				'options'              => [
					'icon' => esc_html__( 'Icon Only', 'teckzone' ),
					'text' => esc_html__( 'Additional Text', 'teckzone' ),
				],
				'selectors'            => [
					'{{WRAPPER}} .teckzone-menu-department .teckzone-title' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'icon' => 'display: none;',
					'text' => 'display: block;',
				],
				'separator'    => 'before',
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your text', 'teckzone' ),
				'label_block' => true,
				'default'     => esc_html__( 'Shop By Department', 'teckzone' ),
				'condition'   => [
					'department' => 'text',
				],
			]
		);

		$this->add_control(
			'icon_menu',
			[
				'label'   => esc_html__( 'Icon Menu', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon icon-menu',
					'library' => 'linearicons',
				],
			]
		);

		$this->end_controls_section();

		$this->section_style();

	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_menu_dropdown_style();
	}

	/**
	 * Element in General Style
	 *
	 * Icon
	 */
	protected function section_general_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department' => 'height: {{SIZE}}{{UNIT}};'
				],
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-menu-department' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-menu-department' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Border
		$this->add_control(
			'general_border_options',
			[
				'label'        => __( 'Border', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'general_border',
				'label'    => __( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .teckzone-menu-department',
			]
		);
		$this->add_responsive_control(
			'general_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-menu-department' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();

		// Spacing
		$this->add_responsive_control(
			'general_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .menu-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		// Background Color
		$this->start_controls_tabs( 'style_tabs' );

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);

		$this->add_control(
			'background_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => __( 'Hover', 'teckzone' ),
			]
		);

		$this->add_control(
			'background_color_hover',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->add_control(
			'style_general_divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		// Elements
		$this->start_controls_tabs( 'menu_elements' );

		// Menu Icon
		$this->start_controls_tab(
			'menu_icon',
			[
				'label' => __( 'Menu Icon', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .menu-icon .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};'
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .menu-icon .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-menu-department .menu-icon .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Title
		$this->start_controls_tab(
			'menu_title',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .teckzone-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .teckzone-menu-department .teckzone-title',
			]
		);

		$this->add_responsive_control(
			'title_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .teckzone-title' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		// Dropdown Icon
		$this->start_controls_tab(
			'dropdown_icon',
			[
				'label' => __( 'Dropdown Icon', 'teckzone' ),
			]
		);

		$this->add_control(
			'icon_dropdown',
			[
				'label'                => __( 'Dropdown Arrow', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Show', 'teckzone' ),
				'label_off'            => esc_html__( 'Hide', 'teckzone' ),
				'return_value'         => 'yes',
				'default'              => '',
				'selectors_dictionary' => [
					'' => 'display: none',
				],
				'selectors'            => [
					'{{WRAPPER}} .teckzone-menu-department .teckzone-dropdown-arrow' => '{{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_dropdown_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .teckzone-dropdown-arrow' => 'font-size: {{SIZE}}{{UNIT}};'
				],
				'condition' => [
					'icon_dropdown' => 'yes',
				],
			]
		);

		$this->add_control(
			'icon_dropdown_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .teckzone-dropdown-arrow' => 'color: {{VALUE}};',
				],
				'condition' => [
					'icon_dropdown' => 'yes',
				],
			]
		);

		$this->end_controls_tab();

		// Line
		$this->start_controls_tab(
			'menu_line',
			[
				'label' => __( 'Line', 'teckzone' ),
			]
		);

		$this->add_control(
			'show_line',
			[
				'label'                => __( 'Line', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => __( 'Show', 'teckzone' ),
				'label_off'            => __( 'Hide', 'teckzone' ),
				'return_value'         => 'yes',
				'default'              => '',
				'selectors'            => [
					'{{WRAPPER}} .teckzone-menu-department .line' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					''    => 'display: none;',
					'yes' => 'display: block;',
				],
			]
		);

		$this->add_control(
			'line_position',
			[
				'label'                => esc_html__( 'Position', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'top' => [
						'title' => __( 'Top', 'teckzone' ),
						'icon'  => 'eicon-v-align-top',
					],
					'bottom' => [
						'title' => __( 'Bottom', 'teckzone' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'default'   => 'top',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .line' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'top'    => 'top: -1px; bottom: auto;',
					'bottom' => 'top: auto; bottom: 0;',
				],
				'condition'            => [
					'show_line' => 'yes',
				],
			]
		);

		$this->add_responsive_control(
			'line_height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .line' => 'height: {{SIZE}}{{UNIT}};'
				],
				'condition' => [
					'show_line' => 'yes',
				],
			]
		);

		$this->add_control(
			'line_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .line' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'show_line' => 'yes',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	/**
	 * Element in Tab Style
	 *
	 * Menu Dropdown
	 */
	protected function section_menu_dropdown_style() {
		$this->start_controls_section(
			'section_menu_dropdown_style',
			[
				'label' => __( 'Dropdown Menu', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'menu_dropdown_spacing',
			[
				'label'      => esc_html__( 'Vertical Distance', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu' => 'border-top-width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'menu_dropdown_left_spacing',
			[
				'label'      => esc_html__( 'Horizontal Distance', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 600,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu' => 'left: -{{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'menu_dropdown_width',
			[
				'label'     => __( 'Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 100,
						'max' => 600,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu' => 'min-width: {{SIZE}}{{UNIT}};'
				],
			]
		);

		$this->add_responsive_control(
			'menu_dropdown_v_padding',
			[
				'label'     => __( 'Vertical Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu .teckzone-department-menu' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		// Items
		$this->add_control(
			'menu_items_style',
			[
				'label'     => __( 'Menu Item', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'menu_dropdown_typography',
				'selector' => '{{WRAPPER}} .teckzone-menu-department .department-menu a',
			]
		);

		$this->add_responsive_control(
			'padding_horizontal_menu_item',
			[
				'label'     => __( 'Horizontal Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu ul.teckzone-department-menu > li' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'padding_vertical_menu_item',
			[
				'label'     => __( 'Vertical Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu a' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		// Icon
		$this->add_control(
			'menu_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_responsive_control(
			'menu_dropdown_icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu a.has-icon i' => 'font-size: {{SIZE}}{{UNIT}};'
				],
			]
		);

		//		$this->add_control(
		//			'menu_dropdown_icon_color',
		//			[
		//				'label'     => __( 'Color', 'teckzone' ),
		//				'type'      => Controls_Manager::COLOR,
		//				'default'   => '',
		//				'selectors' => [
		//					'{{WRAPPER}} .teckzone-menu-department .department-menu a.has-icon i' => 'color: {{VALUE}};',
		//				],
		//			]
		//		);

		$this->end_popover();

		//Style
		$this->start_controls_tabs( 'dropdown_menu_item_style' );

		$this->start_controls_tab(
			'menu_item_normal',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);

		$this->add_control(
			'menu_item_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department ul.teckzone-department-menu > li > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu ul.teckzone-department-menu > li' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu ul.teckzone-department-menu > li > a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'menu_item_hover',
			[
				'label' => __( 'Hover', 'teckzone' ),
			]
		);

		$this->add_control(
			'menu_item_hover_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu ul.teckzone-department-menu > li:hover > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_hover_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu ul.teckzone-department-menu > li:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_hover_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department .department-menu ul.teckzone-department-menu > li:hover > a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_render_attribute(
			'wrapper',
			[
				'class' => [
					'teckzone-menu-department teckzone-menu-department--sticky-none',
					$settings['menu_dropdown'] == 'always' ? 'menu-show' : ''
				],
				'data-menu_dropdown' => $settings['menu_dropdown'] == 'always' ? 'yes' : 'no'
			]
		);

		$output = '<span class="line"></span>';
		$icon   = $title = '';

		if ( $settings['icon_menu'] && ! empty( $settings['icon_menu']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon_menu'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon teckzone-icon-menu">' . ob_get_clean() . '</span>';
		}

		$title = $settings['title'] ? sprintf( '<span class="teckzone-title">%s</span>', $settings['title'] ) : '';
		$output .= '<div class="menu-icon">' . $icon . $title . '</div>';
		$output .= apply_filters( 'teckzone_menu_department_dropdown_arrow', '<span class="teckzone-dropdown-arrow"><i class="icon-chevron-down"></i></span>' );

		$args = [
			'echo'        => false,
			'menu'        => $settings['menu'],
			'menu_class'  => 'teckzone-department-menu',
			'menu_id'     => 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
			'fallback_cb' => '__return_empty_string',
			'container'   => '',
			'walker'      => new \Teckzone_Mega_Menu_Walker(),
		];

		// Dropdown Menu.
		$args['menu_id']    = 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id();
		$dropdown_menu_html = wp_nav_menu( $args );

		if ( empty( $dropdown_menu_html ) ) {
			return;
		}

		$this->add_render_attribute(
			'menu-toggle', 'class', [
				'elementor-menu-toggle',
			]
		);

		if ( \Elementor\Plugin::$instance->editor->is_edit_mode() ) {
			$this->add_render_attribute(
				'menu-toggle', [
					'class' => 'elementor-clickable',
				]
			);
		}

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php echo $output; ?>
			<div class="department-menu main-navigation">
				<nav class="teckzone-department-menu--dropdown"><?php echo $dropdown_menu_html; ?></nav>
			</div>
		</div>
		<?php
	}

	protected function _content_template() {

	}
}