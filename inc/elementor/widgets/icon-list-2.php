<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Icon_List_2 extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-icon-list-2';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Icon List 2', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);
		$this->add_responsive_control(
			'genera_columns',
			[
				'label'           => esc_html__( 'Columns', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					'1' => esc_html__( '1 Column', 'teckzone' ),
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
					'7' => esc_html__( '7 Columns', 'teckzone' ),
				],
				'desktop_default' => '5',
				'tablet_default'  => '3',
				'mobile_default'  => '1',
				'toggle'          => false,
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} .techzone-icon-list-2 .box-item'                              => 'flex: 0 0 calc(1/{{VALUE}}*100%); max-width: calc(1/{{VALUE}}*100%)',
							'{{WRAPPER}} .teckzone-icon-list-2 .box-item.no-separator .item-separator' => 'opacity: 0',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors' => [
							'{{WRAPPER}} .techzone-icon-list-2 .box-item'                                     => 'flex: 0 0 calc(1/{{VALUE}}*100%); max-width: calc(1/{{VALUE}}*100%)',
							'{{WRAPPER}} .teckzone-icon-list-2 .box-item.no-separator-tablet .item-separator' => 'opacity: 0',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors' => [
							'{{WRAPPER}} .techzone-icon-list-2 .box-item'                                     => 'flex: 0 0 calc(1/{{VALUE}}*100%); max-width: calc(1/{{VALUE}}*100%)',
							'{{WRAPPER}} .teckzone-icon-list-2 .box-item.no-separator-mobile .item-separator' => 'opacity: 0',
						],
					],
				]
			]
		);
		
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'type_icon',
			[
				'label'   => __( 'Type', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'icon'  => [
						'title' => __( 'Icon', 'teckzone' ),
						'icon'  => 'fa fa-star',
					],
					'image' => [
						'title' => __( 'Image', 'teckzone' ),
						'icon'  => 'fa fa-picture-o',
					],
				],
				'default' => 'icon',
				'toggle'  => true,
			]
		);

		$repeater->add_control(
			'icon',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'   => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
				'condition' => [
					'type_icon' => 'icon',
				],
			]
		);

		$repeater->add_control(
			'image', [
				'label'     => esc_html__( 'Choose Image', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'default'   => [
					'url' => 'https://via.placeholder.com/270/f8f8f8?text=110x110+Image',
				],
				'condition' => [
					'type_icon' => 'image',
				],
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				'default'   => 'full',
				'separator' => 'none',
				'condition' => [
					'type_icon' => 'image',
				],
			]
		);

		$repeater->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
				'separator'   => 'before',
			]
		);

		$repeater->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->add_control(
			'element',
			[
				'label'         => '',
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'type_icon' => 'icon',
						'icon'      => [ 'value' => 'icon-rocket' ],
						'image'     => 'https://via.placeholder.com/110x110',
						'title'     => esc_html__( 'This is the title', 'teckzone' ),
						'desc'      => esc_html__( 'This is the description', 'teckzone' ),

					],
					[
						'type_icon' => 'icon',
						'icon'      => [ 'value' => 'icon-repeat-one2' ],
						'image'     => 'https://via.placeholder.com/110x110',
						'title'     => esc_html__( 'This is the title 2', 'teckzone' ),
						'desc'      => esc_html__( 'This is the description 2', 'teckzone' ),
					],
					[
						'type_icon' => 'icon',
						'icon'      => [ 'value' => 'icon-shield-check' ],
						'image'     => 'https://via.placeholder.com/110x110',
						'title'     => esc_html__( 'This is the title 3', 'teckzone' ),
						'desc'      => esc_html__( 'This is the description 3', 'teckzone' ),
					],
					[
						'type_icon' => 'icon',
						'icon'      => [ 'value' => 'icon-lifebuoy' ],
						'image'     => 'https://via.placeholder.com/110x110',
						'title'     => esc_html__( 'This is the title 4', 'teckzone' ),
						'desc'      => esc_html__( 'This is the description 4', 'teckzone' ),
					],
				],
				'title_field'   => '{{{ title }}}',
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_icon_style();
		$this->section_title_style();
		$this->section_desc_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * General
	 */
	protected function section_general_style() {
		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'Icon Lists', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'general_align',
			[
				'label'       => esc_html__( 'Text Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'desktop_default' => 'center',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'toggle'          => false,
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors'   => [
							'{{WRAPPER}} .techzone-icon-list-2 .box-item' => 'text-align: {{VALUE}};',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors'   => [
							'{{WRAPPER}} .techzone-icon-list-2 .box-item' => 'text-align: {{VALUE}};',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors'   => [
							'{{WRAPPER}} .techzone-icon-list-2 .box-item' => 'text-align: {{VALUE}};',
						],
					],
				]
			]
		);

		$this->add_responsive_control(
			'align_item',
			[
				'label'       => esc_html__( 'Align Item', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'flex-start'   => [
						'title' => esc_html__( 'Top', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Bottom', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'desktop_default' => 'flex-start',
				'tablet_default'  => 'flex-start',
				'mobile_default'  => 'flex-start',
				'toggle'          => false,
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors'   => [
							'{{WRAPPER}} .teckzone-icon-list-2 .box-item__wrapper' => 'align-items: {{VALUE}};',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors'   => [
							'{{WRAPPER}} .teckzone-icon-list-2 .box-item__wrapper' => 'align-items: {{VALUE}};',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors'   => [
							'{{WRAPPER}} .teckzone-icon-list-2 .box-item__wrapper' => 'align-items: {{VALUE}};',
						],
					],
				]
			]
		);

		$this->add_responsive_control(
			'genera_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '50',
					'right'  => '30',
					'bottom' => '50',
					'left'   => '30',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-icon-list-2 .box-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Separator
		$this->add_control(
			'sep_style',
			[
				'label'        => __( 'Separator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'item_sep',
			[
				'label'                => __( 'Separator', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => __( 'Show', 'teckzone' ),
				'label_off'            => __( 'Hide', 'teckzone' ),
				'return_value'         => 'yes',
				'default'              => 'yes',
				'selectors_dictionary' => [
					''    => 'opacity: 0',
					'yes' => 'opacity: 1',
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .box-item .item-separator' => '{{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'sep_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-icon-list-2 .box-item .item-separator' => 'height: {{SIZE}}{{UNIT}}',
				],
				'condition'  => [
					'item_sep' => '1',
				],
			]
		);
		$this->add_control(
			'sep_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .box-item .item-separator' => 'background-color: {{VALUE}}',
				],
				'condition' => [
					'item_sep' => '1',
				],
			]
		);
		$this->end_popover();
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function section_icon_style() {
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'icon_position',
			[
				'label'   => esc_html__( 'Position', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => __( 'Top Center', 'teckzone' ),
						'icon'  => 'eicon-v-align-top',
					],
					'right' => [
						'title' => __( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default' => 'left',
				'toggle'  => false,
			]
		);

		$this->add_responsive_control(
			'icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2.techzone-icon-list-2--icon-center .box-item__icon' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'icon_position' => 'center',
				],
			]
		);

		$this->add_responsive_control(
			'icon_spacing_right',
			[
				'label'     => __( 'Margin Right', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2.techzone-icon-list-2--icon-left .box-item__icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'icon_position' => 'left',
				],
			]
		);

		$this->add_responsive_control(
			'icon_spacing_left',
			[
				'label'     => __( 'Margin Left', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2.techzone-icon-list-2--icon-right .box-item__icon' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'icon_position' => 'right',
				],
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-icon-list-2 .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-icon-list-2 .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_title_style() {
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-icon-list-2 .title',
			]
		);

		$this->add_responsive_control(
			'title_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Desc
	 */
	protected function section_desc_style() {
		$this->start_controls_section(
			'section_desc_style',
			[
				'label' => __( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-icon-list-2 .desc' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'desc_typography',
				'selector' => '{{WRAPPER}} .techzone-icon-list-2 .desc',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$class    = [
			'teckzone-icon-list-2 techzone-icon-list-2',
			'teckzone-icon-list-2--align-' . $settings['general_align'],
			'techzone-icon-list-2--align-' . $settings['general_align'],
			'teckzone-icon-list-2--icon-' . $settings['icon_position'],
			'techzone-icon-list-2--icon-' . $settings['icon_position'],
		];

		$this->add_render_attribute( 'wrapper', 'class', $class );

		$element = $settings['element'];

		$output = [ ];

		if ( ! empty ( $element ) ) {
			$output[] = '<div class="list-flex">';
			$i        = 1;
			foreach ( $element as $index => $item ) {

				$icon = $value = $title = $unit = $desc = '';
				if ( $item['type_icon'] == 'image' && $item['image'] ) {
					$icon = Group_Control_Image_Size::get_attachment_image_html( $item );
					$icon = $icon ? sprintf( '<div class="box-img">%s</div>', $icon ) : '';
				}

				if ( $item['type_icon'] == 'icon' && $item['icon'] ) {
					if ( $item['icon'] && ! empty( $item['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
						ob_start();
						\Elementor\Icons_Manager::render_icon( $item['icon'], [ 'aria-hidden' => 'true' ] );
						$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
					}
				}

				if ( $item['title'] ) {
					$title = '<h5 class="title">' . $item['title'] . '</h5>';
				}

				if ( $item['desc'] ) {
					$desc = '<div class="desc">' . $item['desc'] . '</div>';
				}

				$mod        = $i % $settings['genera_columns'];
				$mod_tablet = $i % $settings['genera_columns_tablet'];
				$mod_mobile = $i % $settings['genera_columns_mobile'];

				$item_classes = [ 'box-item' ];
				if ( $mod == 0 ) {
					$item_classes[] = 'no-separator';
				}

				if ( $mod_tablet == 0 ) {
					$item_classes[] = 'no-separator-tablet';
				}

				if ( $mod_mobile == 0 ) {
					$item_classes[] = 'no-separator-mobile';
				}

				$output[] = sprintf(
					'<div class="%s">
						<div class="box-item__wrapper">
							<div class="box-item__icon">%s</div>
							<div class="box-item__content">%s %s</div>
						</div>
						<div class="item-separator"></div>
					</div>',
					esc_attr( implode( ' ', $item_classes ) ),
					$icon,
					$title,
					$desc
				);

				$i ++;
			}

			$output[] = '</div>';
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}