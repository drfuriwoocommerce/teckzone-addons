<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Utils;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Timeline extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-timeline';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Timeline', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-time-line';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$section = apply_filters( 'teckzone_timeline_section_number', 10 );

		for ( $i = 1; $i <= $section; $i ++ ) {
			$this->start_controls_section(
				'section_section_' . $i,
				[ 'label' => sprintf( '%s %s', esc_html__( 'Event', 'teckzone' ), $i ) ]
			);

			$this->add_control(
				'year_' . $i,
				[
					'label'       => esc_html__( 'Year', 'teckzone' ),
					'type'        => Controls_Manager::TEXT,
					'default'     => 2005 + $i,
					'label_block' => true,
				]
			);
			$this->add_control(
				'columns_' . $i,
				[
					'label'   => esc_html__( 'Event Columns', 'teckzone' ),
					'type'    => Controls_Manager::SELECT,
					'default' => '2',
					'options' => [
						'2' => esc_html__( '2 Columns', 'teckzone' ),
						'3' => esc_html__( '3 Columns', 'teckzone' ),
					],
				]
			);

			$repeater = new \Elementor\Repeater();
			$repeater->add_control(
				'image_' . $i,
				[
					'label'   => esc_html__( 'Choose Image', 'teckzone' ),
					'type'    => Controls_Manager::MEDIA,
					'dynamic' => [
						'active' => true,
					],
					'default' => [
						'url' => Utils::get_placeholder_image_src(),
					],
				]
			);
			$repeater->add_group_control(
				Group_Control_Image_Size::get_type(),
				[
					'name'      => 'image',
					// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
					'default'   => 'full',
					'separator' => 'none',
				]
			);

			$repeater->add_control(
				'title_' . $i,
				[
					'label'       => esc_html__( 'Title', 'teckzone' ),
					'type'        => Controls_Manager::TEXT,
					'default'     => esc_html__( 'Event Title', 'teckzone' ),
					'placeholder' => esc_html__( 'This is title', 'teckzone' ),
					'label_block' => true,
				]
			);
			$repeater->add_control(
				'desc_' . $i,
				[
					'label'       => esc_html__( 'Description', 'teckzone' ),
					'type'        => Controls_Manager::TEXTAREA,
					'default'     => esc_html__( 'This is the description. Sed elit quam, iaculis sed semper sit amet udin vitae nibh. at magna akal semperFusce commodo molestie', 'teckzone' ),
					'placeholder' => esc_html__( 'Enter the Description', 'teckzone' ),
					'label_block' => true,
				]
			);
			$this->add_control(
				'event_' . $i,
				[
					'label'   => '',
					'type'    => Controls_Manager::REPEATER,
					'fields'  => $repeater->get_controls(),
					'default' => [
						[
							'image_' . $i => [
								'url' => 'https://via.placeholder.com/60',
							],
							'title_' . $i => esc_html__( 'Event Title', 'teckzone' ),
							'desc_' . $i  => esc_html__( 'This is the description. Sed elit quam, iaculis sed semper sit amet udin vitae nibh. at magna akal semperFusce commodo molestie', 'teckzone' ),
						],
						[
							'image_' . $i => [
								'url' => 'https://via.placeholder.com/60',
							],
							'title_' . $i => esc_html__( 'Event Title', 'teckzone' ),
							'desc_' . $i  => esc_html__( 'This is the description. Sed elit quam, iaculis sed semper sit amet udin vitae nibh. at magna akal semperFusce commodo molestie', 'teckzone' ),
						],
					],
				]
			);

			$this->end_controls_section();
		}

		// Timeline Settings

		$this->start_controls_section(
			'section_settings',
			[
				'label' => __( 'Timeline Settings', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'slidesToShow',
			[
				'label'           => __( 'Timeline to show', 'elementor' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'step'            => 1,
				'default' => 9,
			]
		);

		$this->add_responsive_control(
			'slidesToScroll',
			[
				'label'   => __( 'Timeline to scroll', 'elementor' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'default' => 1,
			]
		);
		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 50,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'teckzone' ),
			]
		);

		//Responsive Settings
		$this->_register_responsive_settings_controls();

		$this->end_controls_section();
	}

	protected function _register_responsive_settings_controls() {
		$this->add_control(
			'responsive_settings_divider',
			[
				'label' => __( 'Responsive Settings', 'teckzone' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'responsive_breakpoint', [
				'label' => __( 'Breakpoint', 'teckzone' ) . ' (px)',
				'description' => __( 'Below this breakpoint the options below will be triggered', 'teckzone' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 1200,
				'min'             => 320,
				'max'             => 1920,
			]
		);

		$repeater->add_control(
			'responsive_slidesToShow',
			[
				'label'           => esc_html__( 'Timeline to show', 'teckzone' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'max'             => 10,
				'default' => 3,
			]
		);
		$repeater->add_control(
			'responsive_slidesToScroll',
			[
				'label'           => esc_html__( 'Timeline to scroll', 'teckzone' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'max'             => 10,
				'default' => 3,
			]
		);

		$this->add_control(
			'carousel_responsive_settings',
			[
				'label' => __( 'Settings', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default' => [
					[
						'responsive_breakpoint' => 1025,
						'responsive_slidesToShow' => 2,
						'responsive_slidesToScroll' => 1,
					],
					[
						'responsive_breakpoint' => 768,
						'responsive_slidesToShow' => 2,
						'responsive_slidesToScroll' => 1,
					],
				],
				'title_field' => '{{{ responsive_breakpoint }}}' . 'px',
				'prevent_empty' => false,
			]
		);
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_content_style();
		$this->section_item_style();
		$this->section_avatar_style();
		$this->section_title_style();
		$this->section_desc_style();
		$this->section_timeline_style();
		$this->section_point_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Content
	 */
	protected function section_content_style() {
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'content_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__events' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Item
	 */
	protected function section_item_style() {
		$this->start_controls_section(
			'section_item_style',
			[
				'label' => __( 'Item', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'item_background_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event .event-wrapper' => 'background-color: {{VALUE}};',
				],
				'default'   => '',
			]
		);
		$this->add_responsive_control(
			'item_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event-content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'item_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '20',
					'right'  => '40',
					'bottom' => '30',
					'left'   => '40',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-timeline .timeline__event .event-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'item_border_radius',
			[
				'label'      => __( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '3',
					'right'  => '3',
					'bottom' => '3',
					'left'   => '3',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-timeline .timeline__event .event-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Avatar
	 */
	protected function section_avatar_style() {
		$this->start_controls_section(
			'section_avatar_style',
			[
				'label' => __( 'Avatar', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'avatar_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 60,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event-avatar' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'avatar_border_radius',
			[
				'label'     => __( 'Border Radius', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'%' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event-avatar img' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_title_style() {
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-timeline .timeline__event-title',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event-title' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'title_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event-title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Description
	 */
	protected function section_desc_style() {
		$this->start_controls_section(
			'section_desc_style',
			[
				'label' => __( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline .timeline__event-desc' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'desc_typography',
				'selector' => '{{WRAPPER}} .techzone-timeline .timeline__event-desc',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Timeline
	 */
	protected function section_timeline_style() {
		$this->start_controls_section(
			'section_timeline_style',
			[
				'label' => __( 'Timeline', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'timeline_spacing',
			[
				'label'     => __( 'Top Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date .slick-track' => 'padding-top: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'timeline_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date li:before' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'timeline_height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 5,
						'min' => 1,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date li:before' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Point
	 */
	protected function section_point_style() {
		$this->start_controls_section(
			'section_point_style',
			[
				'label' => __( 'Point', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'point_background_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date a:before' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'point_size',
			[
				'label'     => __( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date a:before' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'point_year',
			[
				'label'     => __( 'Year', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'point_typography',
				'selector' => '{{WRAPPER}} .techzone-timeline ul.timeline__date a',
			]
		);
		$this->add_responsive_control(
			'point_spacing',
			[
				'label'     => __( 'Top Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 40,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date a' => 'padding-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_point_style' );
		$this->start_controls_tab(
			'point_year_normal',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);
		$this->add_control(
			'point_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab(
			'point_year_active',
			[
				'label' => __( 'Active', 'teckzone' ),
			]
		);
		$this->add_control(
			'point_active_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-timeline ul.timeline__date a.active'        => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-timeline ul.timeline__date a.active:before' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-timeline teckzone-tabs',
			'techzone-timeline',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );
		$section       = apply_filters( 'teckzone_timeline_section_number', 10 );
		$output        = [ ];
		$output_time   = [ ];
		$output_events = [ ];

		for ( $i = 1; $i <= $section; $i ++ ) {
			$active = $i == 1 ? 'active' : '';

			if ( $settings["year_$i"] ) {
				$output_time[] = sprintf( '<li><a href="#" class="%s">%s</a></li>', esc_attr( $active ), $settings["year_$i"] );
			}
			$columns      = intval( $settings["columns_$i"] );
			$output_event = [ ];
			$event        = $settings["event_$i"];

			if ( ! empty ( $event ) ) {
				foreach ( $event as $index => $item ) {
					$event_css = [
						'timeline__event-content',
						'col-md-' . 12 / $columns,
						'col-sm-' . 12 / $columns,
						'col-xs-12',
					];
					$avatar    = Group_Control_Image_Size::get_attachment_image_html( $item, 'image_' . $i );
					$title     = $item["title_$i"] ? '<h3 class="timeline__event-title">' . $item["title_$i"] . '</h3>' : '';
					$desc      = $item["desc_$i"] ? '<div class="timeline__event-desc">' . $item["desc_$i"] . '</div>' : '';
					$avatar    = $avatar ? '<div class="timeline__event-avatar">' . $avatar . '</div>' : '';

					$output_event[] = sprintf( 
						'<div class="%s"><div class="event-wrapper">%s%s%s</div></div>', 
						esc_attr( implode( ' ', $event_css ) ), 
						$avatar, 
						$title, 
						$desc 
					);
				}
			}


			if ( $output_event ) {
				$output_events[] = sprintf( '<div class="timeline__event tabs-panel row %s">%s</div>', esc_attr( $active ), implode( '', $output_event ) );
			}
		}

		if ( $output_events ) {
			$output[] = sprintf( '<div class="timeline__events tabs-content">%s</div>', implode( '', $output_events ) );
		}

		if ( $output_time ) {
			$settings['navigation'] = 'none';
			$settings['autoplay'] = false;
			$settings['infinite'] = false;
			$settings['autoplay_speed'] = 0;

			$this->add_render_attribute( 'date-wrapper', 'class', ['timeline__date tabs-nav'] );
			$this->add_render_attribute( 'date-wrapper', 'data-slick', wp_json_encode( Elementor::get_data_slick( $settings ) ) );

			$output[] = sprintf( '<ul %s>%s</ul>', $this->get_render_attribute_string( 'date-wrapper' ),implode( '', $output_time ) );
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}