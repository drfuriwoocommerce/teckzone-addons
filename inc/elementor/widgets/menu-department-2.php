<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Menu_Department_2 extends Widget_Base {

	public function get_name() {
		return 'techzone-menu-department-2';
	}

	public function get_title() {
		return esc_html__( 'Menu Department 2', 'teckzone' );
	}

	public function get_icon() {
		return 'eicon-nav-menu';
	}

	public function get_keywords() {
		return [ 'menu', 'department' ];
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected $nav_menu_index = 1;

	protected function get_nav_menu_index() {
		return $this->nav_menu_index ++;
	}

	private function get_available_menus() {
		$menus = wp_get_nav_menus();

		$options = [ ];

		foreach ( $menus as $menu ) {
			$options[$menu->slug] = $menu->name;
		}

		return $options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'menu_department_section',
			[
				'label' => esc_html__( 'Menu Department', 'teckzone' ),
			]
		);

		$menus = $this->get_available_menus();

		if ( ! empty( $menus ) ) {
			$this->add_control(
				'menu',
				[
					'label'        => __( 'Menu', 'teckzone' ),
					'type'         => Controls_Manager::SELECT,
					'options'      => $menus,
					'default'      => array_keys( $menus )[0],
					'save_default' => true,					
					'description'  => sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'teckzone' ), admin_url( 'nav-menus.php' ) ),
				]
			);
		} else {
			$this->add_control(
				'menu',
				[
					'type'            => Controls_Manager::RAW_HTML,
					'raw'             => '<strong>' . __( 'There are no menus in your site.', 'teckzone' ) . '</strong><br>' . sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'teckzone' ), admin_url( 'nav-menus.php?action=edit&menu=0' ) ),
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
				]
			);
		}

		$this->end_controls_section();

		$this->section_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Menu Dropdown
	 */
	protected function section_style() {
		$this->start_controls_section(
			'section_menu_dropdown_style',
			[
				'label' => __( 'Dropdown Menu', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'menu_dropdown_border',
				'label'    => __( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .teckzone-menu-department-2 .department-menu',
			]
		);
		$this->add_responsive_control(
			'menu_dropdown_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Items
		$this->add_control(
			'menu_items_style',
			[
				'label'     => __( 'Menu Item', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'menu_dropdown_typography',
				'selector' => '{{WRAPPER}} .teckzone-menu-department-2 .department-menu a',
			]
		);

		$this->add_responsive_control(
			'padding_horizontal_menu_item',
			[
				'label'     => __( 'Horizontal Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'padding_vertical_menu_item',
			[
				'label'     => __( 'Vertical Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu a' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		// Border
		$this->add_control(
			'menu_item_border_style',
			[
				'label'        => __( 'Border', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_responsive_control(
			'menu_dropdown_item_border_width',
			[
				'label'     => __( 'Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li > a' => 'border-bottom-width: {{SIZE}}{{UNIT}};'
				],
			]
		);

		$this->add_control(
			'menu_dropdown_item_border_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li > a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		//Style
		$this->start_controls_tabs( 'dropdown_menu_item_style' );

		$this->start_controls_tab(
			'menu_item_normal',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);

		$this->add_control(
			'menu_item_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 ul.teckzone-department-menu > li > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li > a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'menu_item_hover',
			[
				'label' => __( 'Hover', 'teckzone' ),
			]
		);

		$this->add_control(
			'menu_item_hover_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li:hover > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_hover_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'menu_item_hover_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-menu-department-2 .department-menu ul.teckzone-department-menu > li:hover > a' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_render_attribute(
			'wrapper',
			[
				'class' => [
					'teckzone-menu-department-2',
				],
			]
		);

		$args = [
			'echo'        => false,
			'menu'        => $settings['menu'],
			'menu_class'  => 'teckzone-department-menu',
			'menu_id'     => 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
			'fallback_cb' => '__return_empty_string',
			'container'   => '',
			'walker'      => new \Teckzone_Mega_Menu_Walker(),
		];

		// Dropdown Menu.
		$args['menu_id']    = 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id();
		$dropdown_menu_html = wp_nav_menu( $args );

		if ( empty( $dropdown_menu_html ) ) {
			return;
		}

		$this->add_render_attribute(
			'menu-toggle', 'class', [
				'elementor-menu-toggle',
			]
		);

		if ( \Elementor\Plugin::$instance->editor->is_edit_mode() ) {
			$this->add_render_attribute(
				'menu-toggle', [
					'class' => 'elementor-clickable',
				]
			);
		}

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="department-menu main-navigation">
				<nav class="teckzone-department-menu--dropdown"><?php echo $dropdown_menu_html; ?></nav>
			</div>
		</div>
		<?php
	}

	protected function _content_template() {

	}
}