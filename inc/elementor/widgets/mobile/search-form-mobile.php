<?php

namespace TeckzoneAddons\Elementor\Widgets\Mobile;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Search_Form_Mobile extends Widget_Base {

	public function get_name() {
		return 'techzone-search-form-mobile';
	}

	public function get_title() {
		return esc_html__( 'Teckzone - Search Form Mobile', 'teckzone' );
	}

	public function get_icon() {
		return 'eicon-site-search';
	}

	public function get_keywords() {
		return [ 'search', 'form' ];
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_mobile' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected function _register_controls() {
		$this->_register_open_search_panel_controls();
		$this->_register_search_panel_controls();
		$this->_register_search_content_controls();
		$this->_register_hot_words_controls();
		$this->_register_search_ajax_controls();
		$this->_register_search_results_style();
	}

	protected function _register_open_search_panel_controls() {
		$this->start_controls_section(
			'open_search_panel',
			[
				'label' => esc_html__( 'Search Icon', 'teckzone' ),
			]
		);
		$this->add_control(
			'open_search_panel_icon',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'   => [ ],
			]
		);
		$this->add_control(
			'divider_2b',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_control(
			'search_label',
			[
				'label'       => esc_html__( 'Label', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);
		$this->add_control(
			'search_panel_position',
			[
				'label'   => __( 'Search Panel Position', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'right' => [
						'title' => __( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'   => 'right',
				'separator' => 'before',
			]
		);
		$this->end_controls_section();

		// Style
		$this->start_controls_section(
			'open_search_panel_style',
			[
				'label' => __( 'Search Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'search_panel_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'search_panel_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs('icon_style_tabs');
		$this->start_controls_tab(
			'icon_style_tab',
			[
				'label' => __( 'Icon', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'search_panel_icon_size',
			[
				'label'      => esc_html__( 'Font Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => - 100,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel i'   => 'font-size: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'search_panel_icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel i'   => 'color: {{VALUE}};',
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'search_panel_icon_active_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel.active i'   => 'color: {{VALUE}};',
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel.active svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'label_style_tab',
			[
				'label' => __( 'Label', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'menu_label_typography',
				'selector' => '{{WRAPPER}} .tz-search-form--mobile .open-search-panel .search-label',
			]
		);
		$this->add_responsive_control(
			'label_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel .search-label' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'label_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel .search-label'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'label_active_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .open-search-panel.active .search-label'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();
		
		$this->end_controls_section();
	}

	protected function _register_search_panel_controls() {
		$this->start_controls_section(
			'panel_general_style',
			[
				'label' => __( 'Search Panel', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'search_panel_content_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .search-panel-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function _register_search_content_controls() {
		$this->start_controls_section(
			'top_content_section',
			[
				'label' => esc_html__( 'Search Content', 'teckzone' ),
			]
		);

		$this->start_controls_tabs( 'top_content_tabs' );

		// Search Form Tab
		$this->start_controls_tab(
			'search_form_tab',
			[
				'label' => __( 'Search Form', 'teckzone' ),
			]
		);
		$this->add_control(
			'search_for',
			[
				'label'   => esc_html__( 'Search For', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					''         => esc_html__( 'Everything', 'teckzone' ),
					'products' => esc_html__( 'Only Products', 'teckzone' ),
				],
			]
		);

		$this->add_control(
			'placeholder',
			[
				'label'   => esc_html__( 'Placeholder', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'I\'m shopping for...', 'teckzone' ),
			]
		);

		$this->add_control(
			'button_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [ ],
			]
		);

		$this->end_controls_tab();

		// Close Search Panel Button Tab
		$this->start_controls_tab(
			'close_search_tab',
			[
				'label' => __( 'Close Button', 'teckzone' ),
			]
		);

		$this->add_control(
			'close_search_type',
			[
				'label'   => esc_html__( 'Type', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'text' => [
						'title' => esc_html__( 'Text', 'teckzone' ),
						'icon'  => 'fa fa-font',
					],
					'icon' => [
						'title' => esc_html__( 'Icon', 'teckzone' ),
						'icon'  => 'fa fa-eyedropper',
					],
				],
				'default' => 'text',
				'toggle'  => true,
			]
		);
		$this->add_control(
			'close_search_text',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Cancel', 'teckzone' ),
				'label_block' => true,
				'condition'   => [
					'close_search_type' => 'text'
				]
			]
		);
		$this->add_control(
			'close_search_icon',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'   => [ ],
				'condition' => [
					'close_search_type' => 'icon'
				]
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();


		$this->end_controls_section();

		// Style
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'Search Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'top_content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .top-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'top_content_bg',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .top-content' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->start_controls_tabs( 'top_content_style_tabs', [ 'separator' => 'before' ] );

		// Search Form Style Tab
		$this->start_controls_tab(
			'search_form_style_tab',
			[
				'label' => __( 'Search Form', 'teckzone' ),
			]
		);
		$this->add_control(
			'general_height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .product-cat'   => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-search-form--mobile .search-field'  => 'height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-search-form--mobile .search-submit' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'form_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''       => esc_html__( 'Default', 'teckzone' ),
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .search-field' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'form_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .search-field' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'form_border_width',
			[
				'label'      => __( 'Border Width', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile .search-field' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_placeholder',
				'selector' => '{{WRAPPER}} .tz-search-form--mobile .search-field',
			]
		);

		$this->add_control(
			'search_field_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .search-field'               => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-search-form--mobile .search-field::placeholder ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Icon Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .search-submit .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-search-form--mobile .search-submit .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_tab();

		// Close Search Panel Button Style Tab
		$this->start_controls_tab(
			'close_search_style_tab',
			[
				'label' => __( 'Close Button', 'teckzone' ),
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'close_search_placeholder',
				'selector' => '{{WRAPPER}} .tz-search-form--mobile .top-content .close-search-panel',
			]
		);

		$this->add_control(
			'close_search_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .top-content .close-search-panel' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'close_search_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .top-content .close-search-panel' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function _register_hot_words_controls() {
		$this->start_controls_section(
			'hot_words_section',
			[
				'label' => esc_html__( 'Hot Words', 'teckzone' ),
			]
		);
		$this->add_control(
			'enable_hot_words',
			[
				'label'     => esc_html__( 'Enable', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'teckzone' ),
				'label_on'  => esc_html__( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'hot_words_title',
			[
				'label'       => esc_html__( 'Label', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Trending Now:', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'hot_words_title_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [ ],

			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'text',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Hot Word', 'teckzone' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],

			]
		);
		$this->add_control(
			'hot_words_content',
			[
				'label'         => esc_html__( 'List Items', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'text' => esc_html__( 'Item #1', 'teckzone' ),
						'link' => '#'
					],
					[
						'text' => esc_html__( 'Item #2', 'teckzone' ),
						'link' => '#'
					],
					[
						'text' => esc_html__( 'Item #3', 'teckzone' ),
						'link' => '#'
					]
				],
				'title_field'   => '{{{ text }}}',
				'prevent_empty' => false,
				'separator'     => 'before',
			]
		);

		$this->end_controls_section();

		// Style
		$this->start_controls_section(
			'style_hot_words',
			[
				'label' => __( 'Hot Words', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'hot_words_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .hot-words' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'hot_words_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'default'    => [ ],
				'selectors'  => [
					'{{WRAPPER}} .tz-search-form--mobile ul.hot-words' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'hot_words_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile ul.hot-words' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->start_controls_tabs( 'settings_hot_word' );

		$this->start_controls_tab( 'style_title_word', [ 'label' => esc_html__( 'Label', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_word_typography',
				'selector' => '{{WRAPPER}} .tz-search-form--mobile .title-words',
			]
		);

		$this->add_control(
			'title_word_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .title-words' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title_word_icon_color',
			[
				'label'     => esc_html__( 'Icon Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .title-words .teckzone-icon'     => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-search-form--mobile .title-words .teckzone-icon svg' => 'fill: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title_word_icon_size',
			[
				'label'     => esc_html__( 'Icon Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .title-words .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-search-form--mobile .title-words .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'title_word_icon_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .title-words .teckzone-icon' => 'padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_key_word', [ 'label' => esc_html__( 'Key Words', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_hot_key_typography',
				'selector' => '{{WRAPPER}} .tz-search-form--mobile .hot-words li:not(.title-words)',
			]
		);
		$this->add_responsive_control(
			'item_key_word_bot_spacing',
			[
				'label'     => esc_html__( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile ul.hot-words li:not( .title-words )' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'item_key_word_padding',
			[
				'label'              => esc_html__( 'Padding', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'left', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .tz-search-form--mobile .hot-words li' => 'padding-left: {{LEFT}}{{UNIT}};padding-right: {{RIGHT}}{{UNIT}}',
					'{{WRAPPER}} .tz-search-form--mobile .hot-words'    => 'margin-left: -{{LEFT}}{{UNIT}};margin-right: -{{RIGHT}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'key_word_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .hot-words li:not(.title-words) a' => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-search-form--mobile .hot-words li:not(.title-words)'   => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'key_word_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .hot-words li:not(.title-words) a' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .tz-search-form--mobile .hot-words li:not(.title-words)'   => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function _register_search_ajax_controls() {
		$this->start_controls_section(
			'ajax_search_section',
			[
				'label' => esc_html__( 'AJAX Search', 'teckzone' ),
			]
		);
		$this->add_control(
			'enable_ajax_search',
			[
				'label'        => esc_html__( 'Enable AJAX Search', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_off'    => esc_html__( 'Off', 'teckzone' ),
				'label_on'     => esc_html__( 'On', 'teckzone' ),
				'return_value' => 'yes',
				'default'      => '',
			]
		);
		$this->add_control(
			'ajax_search_number',
			[
				'label'       => esc_html__( 'AJAX Search Results Number', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'description' => esc_html__( 'The number of maximum results in the search box', 'teckzone' ),
				'default'     => 6,
				'min'         => 0,
			]
		);
		$this->end_controls_section();
	}

	protected function _register_search_results_style() {
		$this->start_controls_section(
			'style_search_results_form_',
			[
				'label' => __( 'Search Results', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'vertical_padding',
			[
				'label'     => esc_html__( 'Vertical Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .search-wrapper .search-results ul li' => 'padding-top: {{SIZE}}{{UNIT}};padding-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'horizontal_padding',
			[
				'label'     => esc_html__( 'Horizontal Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-search-form--mobile .search-wrapper .search-results' => 'padding-left: {{SIZE}}{{UNIT}};padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'tz-search-form tz-search-form--mobile',
				'tz-search-form--mobile-' . $settings['search_panel_position'],
				empty( $settings['search_for'] ) ? 'search-every-thing' : 'search-products',
			]
		);

		$data_settings = [
			'ajax_search' => $settings['enable_ajax_search'],
			'search_for'  => $settings['search_for'],
		];

		if ( $settings['enable_ajax_search'] == 'yes' ) {
			$data_settings['ajax_search_number'] = $settings['ajax_search_number'];
		}

		$this->add_render_attribute(
			'wrapper', 'data-settings', wp_json_encode( $data_settings )
		);

		$post_type_html = '';
		if ( $settings['search_for'] == 'products' ) {
			$post_type_html = '<input type="hidden" name="post_type" value="product">';
		}

		$lang = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : false;
		if ( $lang ) {
			$post_type_html .= '<input type="hidden" name="lang" value="' . $lang . '"/>';
		}

		// Search submit icon
		$button = '<i class="icon-magnifier"></i>';
		if ( $settings['button_icon']['value'] && ! empty( $settings['button_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['button_icon'], [ 'aria-hidden' => 'true' ] );
			$button = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$hot_words_icon = '';
		if ( $settings['hot_words_title_icon']['value'] && ! empty( $settings['hot_words_title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['hot_words_title_icon'], [ 'aria-hidden' => 'true' ] );
			$hot_words_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		// Hot words
		$element_key = $settings['hot_words_content'];
		$output_key  = [ ];
		if ( ! empty ( $element_key ) && $settings['enable_hot_words'] ) {
			$output_key[] = '<ul class="hot-words">';
			$output_key[] = '<li class="title-words">' . $hot_words_icon . $settings['hot_words_title'] . '</li>';
			foreach ( $element_key as $index => $item ) {
				$link_key = 'link_' . $index;

				$output_key[] = sprintf( '<li>%s</li>', $this->get_link_control( $link_key, $item['link'], $item['text'], [ 'class' => 'word' ] ) );
			}
			$output_key[] = '</ul>';
		}

		// Search Icon
		$open_search_icon = '<i class="icon-magnifier"></i>';
		if ( $settings['open_search_panel_icon']['value'] && ! empty( $settings['open_search_panel_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['open_search_panel_icon'], [ 'aria-hidden' => 'true' ] );
			$open_search_icon = ob_get_clean();
		}

		// Close search icon
		$close_search_icon = '';
		if ( $settings['close_search_type'] == 'icon' && $settings['close_search_icon']['value'] && ! empty( $settings['close_search_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['close_search_icon'], [ 'aria-hidden' => 'true' ] );
			$close_search_icon = ob_get_clean();
		}

		if ( $settings['close_search_type'] == 'text' && $settings['close_search_text'] ) {
			$close_search_icon = $settings['close_search_text'];
		}

		$label = $settings['search_label'] ? '<span class="search-label">' . $settings['search_label'] . '</span>' : '';

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<a href="#" class="open-search-panel"><?php echo $open_search_icon . $label ?></a>

			<div class="search-panel-content">
				<div class="top-content">
					<form method="get" class="form-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<div class="search-inner-content">
							<div class="search-wrapper">
								<span class="loading-icon"></span>
								<input type="text" name="s" class="search-field" autocomplete="off"
								       placeholder="<?php echo $settings['placeholder']; ?>">
								<?php echo $post_type_html; ?>

								<a href="#" class="close-search-results"><i class="icon-cross2"></i></a>
							</div>
						</div>
						<button class="search-submit" type="submit"><?php echo $button; ?></button>
					</form>
					<a href="#" class="close-search-panel"><?php echo $close_search_icon ?></a>
					<div class="search-results woocommerce"></div>
				</div>
				<?php echo implode( '', $output_key ) ?>
			</div>
			<div class="teckzone-off-canvas-layer"></div>
		</div>
		<?php
	}

	protected function _content_template() {

	}

	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$tag = 'a';

		if ( empty( $attr_default['href'] ) ) {
			$tag = 'span';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $this->get_render_attribute_string( $link_key ), $content );
	}
}
