<?php

namespace TeckzoneAddons\Elementor\Widgets\Mobile;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Cart widget
 */
class Cart_Mobile extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'teckzone-cart-mobile';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Cart Mobile', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-cart-light';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_mobile' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->_register_mini_cart_icon_controls();
		$this->_register_cart_panel_controls();
		$this->_register_top_content_controls();
		$this->_register_bottom_content_controls();
	}

	protected function _register_mini_cart_icon_controls() {
		$this->start_controls_section(
			'section_mini_cart_icon',
			[ 'label' => esc_html__( 'Mini Cart Icon', 'teckzone' ) ]
		);

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-cart',
					'library' => 'linearicons',
				],
			]
		);
		$this->add_control(
			'divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_control(
			'cart_behaviour',
			[
				'label'   => __( 'Cart Behaviour', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'panel',
				'options' => [
					'panel' => __( 'Open the cart panel', 'teckzone' ),
					'link'  => __( 'Open the cart page', 'teckzone' ),
				],
			]
		);
		$this->add_control(
			'cart_url',
			[
				'label'   => __( 'URL', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'page'   => __( 'Cart Page', 'teckzone' ),
					'custom' => __( 'Custom', 'teckzone' ),
				],
				'default'   => 'page',
				'condition' => [
					'cart_behaviour' => 'link'
				]
			]
		);
		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
				'conditions'     => [
					'terms' => [
						[
							'name'  => 'cart_behaviour',
							'value' => 'link'
						],
						[
							'name'  => 'cart_url',
							'value' => 'custom'
						],
					],
				],
			]
		);
		$this->add_control(
			'divider_2',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_control(
			'enable_count',
			[
				'label'        => esc_html__( 'Count', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_off'    => esc_html__( 'Hide', 'teckzone' ),
				'label_on'     => esc_html__( 'Show', 'teckzone' ),
				'return_value' => 'yes',
				'default'      => 'yes'
			]
		);
		$this->add_control(
			'divider_2b',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_control(
			'mini_cart_label',
			[
				'label'       => esc_html__( 'Label', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);
		$this->end_controls_section(); // End Icon Content

		$this->start_controls_section(
			'style_mini_cart_icon',
			[
				'label' => __( 'Mini Cart Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'cart_panel_position',
			[
				'label'   => __( 'Cart Panel Position', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'right' => [
						'title' => __( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default' => 'right',
			]
		);
		$this->add_responsive_control(
			'cart_icon_padding',
			[
				'label'       => esc_html__( 'Padding', 'teckzone' ),
				'type'        => Controls_Manager::DIMENSIONS,
				'size_units'  => [ 'px', 'em', '%' ],
				'placeholder' => [ 
					'top'    => '8',
					'right'  => '12',
					'bottom' => '8',
					'left'   => '12'
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->start_controls_tabs('mini_cart_icon_tabs');

		$this->start_controls_tab(
			'icon_cart_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_active_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url.active .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url.active .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'count_style',
			[
				'label' => __( 'Count', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'count_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [ 
					'top'    => '3',
					'right'  => '6',
					'bottom' => '3',
					'left'   => '6'
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'count_border_radius',
			[
				'label'      => __( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'placeholder' => [ 
					'top'    => '3',
					'right'  => '3',
					'bottom' => '3',
					'left'   => '3'
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],

			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_num',
				'selector' => '{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter',
			]
		);

		$this->add_control(
			'count_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'count_bcolor',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter' => 'background-color: {{VALUE}};',
				],
				'separator' => 'after',
			]
		);

		$this->add_responsive_control(
			'count_pos_x',
			[
				'label'      => esc_html__( 'Position X', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => - 100,
						'max' => 100,
					],
					'%'  => [
						'min' => - 100,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter' => 'right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'count_pos_y',
			[
				'label'      => esc_html__( 'Position Y', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => - 100,
						'max' => 100,
					],
					'%'  => [
						'min' => - 100,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-item-counter' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();

		// Label
		$this->start_controls_tab(
			'label_style',
			[
				'label' => __( 'Label', 'teckzone' ),
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'mini_cart_label_typography',
				'selector' => '{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-cart-label',
			]
		);
		$this->add_responsive_control(
			'label_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-cart-label' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'label_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url .mini-cart-label'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'label_active_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-url.active .mini-cart-label'     => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function _register_cart_panel_controls() {
		$this->start_controls_section(
			'panel_general_style',
			[
				'label' => __( 'Mini Cart Panel', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'mini_cart_panel_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function _register_top_content_controls() {
		// Content
		$this->start_controls_section(
			'top_content_section',
			[
				'label' => esc_html__( 'Mini Cart Panel Header', 'teckzone' ),
			]
		);
		$this->add_control(
			'mini_cart_text',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'label_block' => true,
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Shopping Cart', 'teckzone' ),
				'placeholder' => esc_html__( 'Mini Cart Header Text', 'teckzone' ),
			]
		);
		$this->end_controls_section(); // End

		// Style
		$this->start_controls_section(
			'top_content_style',
			[
				'label' => __( 'Mini Cart Header', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'mini_cart_header_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [ 
					'top'    => '25',
					'right'  => '20',
					'bottom' => '25',
					'left'   => '20'
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-header' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'mini_cart_header_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-header' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->start_controls_tabs('mini_cart_header_tabs');

		$this->start_controls_tab(
			'mini_cart_text_style',
			[
				'label' => __( 'Text', 'teckzone' ),
			]
		);

		$this->add_control(
			'mini_cart_text_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-header__text' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'mini_cart_text_typography',
				'selector' => '{{WRAPPER}} .teckzone-cart-mobile .mini-cart-header__text',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'mini_cart_header_icon_style',
			[
				'label' => __( 'Icon', 'text_domain' ),
			]
		);

		$this->add_control(
			'mini_cart_header_icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-header .close-cart-panel' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'mini_cart_header_icon_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-header .close-cart-panel' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function _register_bottom_content_controls() {
		$this->start_controls_section(
			'bottom_content_style',
			[
				'label' => __( 'Mini Cart Panel Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'mini_cart_content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [ 
					'top'    => '84',
					'right'  => '20',
					'bottom' => '326',
					'left'   => '20'
				],
				'description'      => esc_html__( 'Padding top should be equal Mini Cart Panel Header height. Padding bottom should be equal Mini Cart Footer height.', 'teckzone' ),
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-content'            => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 {{LEFT}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-cart-mobile .widget_shopping_cart_content' => 'padding-bottom: {{BOTTOM}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-footer'             => 'left: {{LEFT}}{{UNIT}};right: {{RIGHT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'mini_cart_content_footer_padding',
			[
				'label'      => esc_html__( 'Mini Footer Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'allowed_dimensions' => [ 'top', 'bottom' ],
				'placeholder' => [ 
					'top'    => '150',
					'right'  =>  _x( 'Auto', 'Cart Mobile Widget', 'teckzone' ),
					'bottom' => '35',
					'left'   =>  _x( 'Auto', 'Cart Mobile Widget', 'teckzone' ),
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-cart-mobile .mini-cart-footer' => 'padding-top: {{TOP}}{{UNIT}};padding-bottom: {{BOTTOM}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-cart-mobile',
				'teckzone-cart-mobile--' . $settings['cart_behaviour'],
				'teckzone-cart-mobile--' . $settings['cart_panel_position'],
			]
		);

		$mini_content = '';
		$cart_count   = 0;
		if ( ! is_admin() ) {
			if ( ! function_exists( 'woocommerce_mini_cart' ) ) {
				return;
			}
	
			ob_start();
			woocommerce_mini_cart();
			$mini_cart    = ob_get_clean();
			$mini_content = sprintf( 
				'<div class="mini-cart-content">
					<div class="mini-cart-content__wrapper">
						<div class="mini-cart-header">
							<span class="mini-cart-header__text">%s</span>
							<a href="#" class="close-cart-panel"><i class="icon-arrow-right"></i></a>
						</div>
						<div class="widget_shopping_cart_content">%s</div>
					</div>
				</div>',
				esc_html( $settings['mini_cart_text'] ),
				$mini_cart 
			);
	
			$cart_count = intval( WC()->cart->cart_contents_count );
		}

		$icon = '';
		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$html_count = $settings['enable_count'] ? sprintf( '<span class="mini-item-counter teckzone-mini-cart-counter">%s</span>', $cart_count ) : '';
	
		$url = wc_get_cart_url();
		if ( 'custom' == $settings['cart_url'] && ! empty( $settings['link']['url'] ) ) {
			$url = $settings['link']['url'];
		}

		$label = $settings['mini_cart_label'] ? '<span class="mini-cart-label">' . $settings['mini_cart_label'] . '</span>' : '';

		$mini_cart_url = sprintf(
			'<a class="mini-cart-url" href="%s">%s%s%s</a>',
			esc_url( $url ),
			$icon,
			$html_count,
			$label
		);

		echo sprintf(
			'<div %s>
			%s%s
			<div class="teckzone-off-canvas-layer"></div>
			</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$mini_cart_url,
			$mini_content
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}