<?php

namespace TeckzoneAddons\Elementor\Widgets\Mobile;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Menu_Mobile extends Widget_Base {

	public function get_name() {
		return 'techzone-menu-mobile';
	}

	public function get_title() {
		return esc_html__( 'Teckzone - Menu Mobile', 'teckzone' );
	}

	public function get_icon() {
		return 'eicon-menu-bar';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_mobile' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected $nav_menu_index = 1;

	protected function get_nav_menu_index() {
		return $this->nav_menu_index ++;
	}

	private function get_available_menus() {
		$menus = wp_get_nav_menus();

		$options = [ ];

		foreach ( $menus as $menu ) {
			$options[ $menu->slug ] = $menu->name;
		}

		return $options;
	}

	protected function _register_controls() {
		$this->_register_menu_layout_controls();
		$this->_register_menu_panel_controls();
		$this->_register_top_content_controls();
		$this->_register_bottom_content_controls();
	}

	protected function _register_menu_layout_controls() {
		// Content
		$this->start_controls_section(
			'menu_department_section',
			[
				'label' => esc_html__( 'Menu', 'teckzone' ),
			]
		);

		$menus = $this->get_available_menus();

		if ( ! empty( $menus ) ) {
			$this->add_control(
				'menu',
				[
					'label'        => __( 'Menu', 'teckzone' ),
					'type'         => Controls_Manager::SELECT,
					'options'      => $menus,
					'default'      => array_keys( $menus )[0],
					'save_default' => true,
					'separator'    => 'after',
					'description'  => sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'teckzone' ), admin_url( 'nav-menus.php' ) ),
				]
			);
		} else {
			$this->add_control(
				'menu',
				[
					'type'            => Controls_Manager::RAW_HTML,
					'raw'             => '<strong>' . __( 'There are no menus in your site.', 'teckzone' ) . '</strong><br>' . sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'teckzone' ), admin_url( 'nav-menus.php?action=edit&menu=0' ) ),
					'separator'       => 'after',
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
				]
			);
		}

		$this->add_control(
			'icon_menu',
			[
				'label'   => esc_html__( 'Icon Menu', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon icon-menu',
					'library' => 'linearicons',
				],
			]
		);

		$this->add_control(
			'divider_2b',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$this->add_control(
			'menu_label',
			[
				'label'       => esc_html__( 'Label', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->add_control(
			'divider_2c',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'menu_position',
			[
				'label'   => __( 'Menu Position', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'right' => [
						'title' => __( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'   => 'left',
			]
		);

		$this->add_control(
			'indicator',
			[
				'label'     => __( 'Indicator', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'chevron',
				'options'   => [
					'chevron' => __( 'Chevron', 'teckzone' ),
					'plus'    => __( 'Plus', 'teckzone' ),
				],
				'separator' => 'before',
			]
		);

		$this->end_controls_section(); // End

		// Style
		$this->section_icon_style();
		$this->section_style();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_menu_style();
		$this->section_submenu_style();
	}

	protected function section_icon_style() {
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'menu_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs('icon_style_tabs');
		$this->start_controls_tab(
			'icon_style_tab',
			[
				'label' => __( 'Icon', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_active_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon.active .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon.active .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'label_style_tab',
			[
				'label' => __( 'Label', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'menu_label_typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .menu-mobile-label',
			]
		);
		$this->add_responsive_control(
			'label_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .menu-mobile-label' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'label_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon .menu-mobile-label'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'label_active_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-icon.active .menu-mobile-label'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();
		
		$this->end_controls_section();
	}

	protected function section_menu_style() {
		$this->start_controls_section(
			'section_menu_style',
			[
				'label' => __( 'Menu', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'menu_item_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_menu_typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li > a',
			]
		);

		// Color
		$this->start_controls_tabs( 'settings_menu_item' );

		$this->start_controls_tab( 'style_item_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );
		$this->add_control(
			'menu_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile ul.menu > li > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->start_controls_tab( 'style_item_active', [ 'label' => esc_html__( 'Active', 'teckzone' ) ] );

		$this->add_control(
			'menu_active_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li.active > a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->add_control(
			'menu_divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		// Separator
		$this->add_control(
			'border_settings',
			[
				'label'        => __( 'Border', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'border_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li' => 'border-bottom-width: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'border_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li' => 'border-bottom-color: {{VALUE}};',
				],
			]
		);
		$this->end_popover();

		$this->add_control(
			'menu_divider_2',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		// Indicator
		$this->add_control(
			'indicator_settings',
			[
				'label'        => __( 'Indicator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'indicator_font_size',
			[
				'label'      => __( 'Font Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li.menu-item-has-children > .toggle-menu-children' => 'font-size: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'indicator_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li.menu-item-has-children > .toggle-menu-children' => 'width: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'indicator_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li.menu-item-has-children > .toggle-menu-children' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'indicator_offset',
			[
				'label'      => __( 'Vertical Offset', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => - 50,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor ul.menu > li.menu-item-has-children > .toggle-menu-children' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_section();
	}

	protected function section_submenu_style() {
		$this->start_controls_section(
			'section_submenu_style',
			[
				'label' => __( 'Submenu', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'submenu_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor li li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'submenu__typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile li li a',
			]
		);

		$this->add_control(
			'submenu_menu_divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		// Indicator
		$this->add_control(
			'submenu_indicator_settings',
			[
				'label'        => __( 'Indicator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'submenu_indicator_font_size',
			[
				'label'      => __( 'Font Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor li li.menu-item-has-children > .toggle-menu-children' => 'font-size: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'submenu_indicator_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor li li.menu-item-has-children > .toggle-menu-children' => 'width: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'submenu_indicator_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor li li.menu-item-has-children > .toggle-menu-children' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'submenu_indicator_offset',
			[
				'label'      => __( 'Vertical Offset', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => - 50,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor li li.menu-item-has-children > .toggle-menu-children' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_section();
	}

	protected function _register_menu_panel_controls() {
		$this->start_controls_section(
			'panel_general_style',
			[
				'label' => __( 'Menu Panel', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'menu_panel_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .menu-mobile-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function _register_top_content_controls() {
		// Content
		$this->start_controls_section(
			'top_content_section',
			[
				'label' => esc_html__( 'Top Content', 'teckzone' ),
			]
		);
		$this->add_control(
			'top_content_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);
		$this->add_control(
			'top_content_reverse',
			[
				'label'                => esc_html__( 'Content Reverse', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_off'            => esc_html__( 'No', 'teckzone' ),
				'label_on'             => esc_html__( 'Yes', 'teckzone' ),
				'default'              => '',
				'return_value'         => 'yes',
				'selectors'            => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'yes' => 'flex-direction: row-reverse',
				],
				'separator' => 'before',
			]
		);
		$this->add_control(
			'top_content_els',
			[
				'label'     => __( 'Content', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'default',
				'options'   => [
					'default' => __( 'Default', 'teckzone' ),
					'custom'  => __( 'Custom', 'teckzone' ),
				],
			]
		);
		$this->add_control(
			'author',
			[
				'label'                => esc_html__( 'Author', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_off'            => esc_html__( 'Hide', 'teckzone' ),
				'label_on'             => esc_html__( 'Show', 'teckzone' ),
				'default'              => 'yes',
				'return_value'         => 'yes',
				'selectors'            => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content .author' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'' => 'display:none',
				],
				'condition' => [
					'top_content_els' => 'default'
				]
			]
		);
		$this->add_control(
			'login',
			[
				'label'                => esc_html__( 'Login & Register', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_off'            => esc_html__( 'Hide', 'teckzone' ),
				'label_on'             => esc_html__( 'Show', 'teckzone' ),
				'default'              => 'yes',
				'return_value'         => 'yes',
				'selectors'            => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content .login' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'' => 'display:none',
				],
				'condition' => [
					'top_content_els' => 'default'
				]
			]
		);
		$this->add_control(
			'top_content_custom_text',
			[
				'label'       => esc_html__( 'Custom Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => '',
				'label_block' => true,
				'condition' => [
					'top_content_els' => 'custom'
				]
			]
		);
		$this->add_control(
			'top_content_custom_link', [
				'label'         => esc_html__( 'Custom Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
				'condition' => [
					'top_content_els' => 'custom'
				]
			]
		);
		$this->end_controls_section(); // End

		// Style
		$this->start_controls_section(
			'top_content_style',
			[
				'label' => __( 'Top Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'top_content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'top_content_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content'                      => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content .login a'             => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content .custom-content a'    => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content .custom-content span' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'top_content_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content' => 'background-color: {{VALUE}}',
				],
			]
		);
		$this->start_controls_tabs( 'top_content_tabs', [ 'separator' => 'before' ] );

		$this->start_controls_tab( 'style_arrow_item', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );
		$this->add_responsive_control(
			'arrow_font_size',
			[
				'label'      => __( 'Font Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .top-content .go-back' => 'font-size: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_author_item', [ 'label' => esc_html__( 'Author', 'teckzone' ) ] );
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'author_typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile--elementor .top-content .author',
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab( 'style_login_item', [ 'label' => esc_html__( 'Login & Register', 'teckzone' ) ] );
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'login_typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile--elementor .top-content .login',
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab( 'style_custom_item', [ 'label' => esc_html__( 'Custom Content', 'teckzone' ) ] );
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'custom_content_typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile--elementor .top-content .custom-content',
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();
		$this->end_controls_section();
	}

	protected function _register_bottom_content_controls() {
		// Content
		$this->start_controls_section(
			'bottom_content_section',
			[
				'label' => esc_html__( 'Bottom Content', 'teckzone' ),
			]
		);
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);

		$repeater->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => '',
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'bottom_content',
			[
				'label'         => '',
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'icon'  => [ 'value' => 'icon-history' ],
						'title' => esc_html__( 'Recent viewed products', 'teckzone' ),
						'link'  => [
							'url' => '#',
						],
					],
					[
						'icon'  => [ 'value' => 'icon-cube' ],
						'title' => esc_html__( 'Become a vendor', 'teckzone' ),
						'link'  => [
							'url' => '#',
						],
					],
					[
						'icon'  => [ 'value' => 'icon-question-circle' ],
						'title' => esc_html__( 'Help & Contact', 'teckzone' ),
						'link'  => [
							'url' => '#',
						],
					],
					[
						'icon'  => [ 'value' => 'icon-telephone' ],
						'title' => esc_html__( 'HOTLINE: (+965) 7492-4277 (Free)', 'teckzone' ),
						'link'  => [
							'url' => '#',
						],
					],
				],
				'title_field'   => '{{{ title }}}',
				'prevent_empty' => false
			]
		);
		$this->end_controls_section(); // End

		// Style
		$this->start_controls_section(
			'bottom_content_style',
			[
				'label' => __( 'Bottom Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'bottom_content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'bottom_content_bg_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content'                           => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .tz-menu-mobile--elementor.tz-menu-mobile .menu-mobile-wrapper:after' => 'background-color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'bottom_content_item',
			[
				'label'     => __( 'Item', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->start_controls_tabs( 'bottom_content_tabs' );

		$this->start_controls_tab( 'style_icon_item', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );
		$this->add_control(
			'bottom_icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content .teckzone-icon' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'bottom_icon_size',
			[
				'label'      => __( 'Font Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'bottom_icon_spacing',
			[
				'label'      => __( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content .teckzone-icon' => 'padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'bottom_icon_offset',
			[
				'label'      => __( 'Offset', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => - 50,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content .teckzone-icon' => 'transform: translateY({{SIZE}}{{UNIT}})',
				],
			]
		);
		$this->end_controls_tab();
		$this->start_controls_tab( 'style_title_item', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );
		$this->add_control(
			'bottom_title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content .bottom-content--item a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'bottom_title_typography',
				'selector' => '{{WRAPPER}} .tz-menu-mobile--elementor .bottom-content .bottom-content--item a',
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();
		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$class = [
			'tz-menu-mobile tz-menu-mobile--elementor',
			'tz-menu-mobile--indicator-' . $settings['indicator'],
			'tz-menu-mobile--' . $settings['menu_position'],
		];

		$args = [
			'echo'        => false,
			'menu'        => $settings['menu'],
			'menu_class'  => 'tz-nav-mobile-menu menu',
			'menu_id'     => 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
			'fallback_cb' => '__return_empty_string',
			'container'   => '',
		];

		if ( class_exists( 'Teckzone_Mobile_Menu_Walker' ) ) {
			$args['walker'] = new \Teckzone_Mobile_Menu_Walker();
		}

		$menu_html = wp_nav_menu( $args );

		if ( empty( $menu_html ) ) {
			return;
		}

		$icon = '<span class="teckzone-icon"><i class="icon-menu"></i></span>';

		if ( $settings['icon_menu'] && ! empty( $settings['icon_menu']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon_menu'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$label = $settings['menu_label'] ? '<span class="menu-mobile-label">' . $settings['menu_label'] . '</span>' : '';
		$icon = '<div class="menu-icon menu-icon-js">' . $icon . $label . '</div>';

		// Top content
		$top_icon = '<i class="icon-arrow-left"></i>';

		if ( $settings['top_content_icon'] && ! empty( $settings['top_content_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['top_content_icon'], [ 'aria-hidden' => 'true' ] );
			$top_icon =  ob_get_clean();
		}

		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();
			$author  = get_user_by( 'id', $user_id );
			$author = sprintf( '<div class="author"><i class="icon-user"></i>%s<span>%s</span></div>', esc_html__( 'Hi,', 'teckzone' ), $author->display_name );
		} else {
			$author = $this->logout();
		}

		$custom_text = $settings['top_content_custom_text'];
		$custom_link = $settings['top_content_custom_link'];
		$custom_content = sprintf(
			'<div class="custom-content">%s</div>', 
			$this->get_link_control( 'custom_link', $custom_link, $custom_text , [ ])
		);

		// Bottom Content
		$bottom_content = [ ];

		if ( ! empty( $settings['bottom_content'] ) ) {
			$bottom_content[] = '<div class="bottom-content">';

			foreach ( $settings['bottom_content'] as $index => $item ) {
				$link_key    = 'link_' . $index;
				$bottom_icon = $title = '';

				if ( $item['icon'] && ! empty( $item['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
					ob_start();
					\Elementor\Icons_Manager::render_icon( $item['icon'], [ 'aria-hidden' => 'true' ] );
					$bottom_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
				}

				if ( $item['title'] ) {
					$title = $this->get_link_control( $link_key, $item['link'], wp_kses_post( $item['title'] ), [ ] );
				}

				$bottom_content[] = sprintf( '<div class="bottom-content--item">%s%s</div>', $bottom_icon, $title );
			}

			$bottom_content[] = '</div>';
		} else {
			$class[] = 'tz-menu-mobile--no-bottom-content';
		}

		$this->add_render_attribute( 'wrapper', 'class', $class );

		echo sprintf(
			'<div %s>
				%s
				<div class="menu-mobile-wrapper">
					<div class="primary-menu-mobile">
						<div class="top-content">
							<span class="go-back close-canvas-mobile-panel">%s</span>
							%s
						</div>
						<nav class="menu-content">%s</nav>
						%s
					</div>
				</div>
				<div class="teckzone-off-canvas-layer"></div>
				
			</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$icon,
			$top_icon,
			$settings['top_content_els'] == 'default' ? $author : $custom_content,
			$menu_html,
			implode( '', $bottom_content )
		);
	}

	protected function _content_template() {

	}

	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}

	protected function logout() {
		$title = sprintf( 
			'<span class="title">
				<span class="login-text">%s </span>
				<span class="register-text"><span class="sep">%s </span>%s</span>
			</span>',
			esc_html__('Login','teckzone'),
			esc_html__('or','teckzone'),
			esc_html__( 'Register', 'teckzone' )
		);

		$account_url = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );

		$output = sprintf('<div class="login"><a href="%s"><i class="icon-user"></i>%s</a></div>',$account_url,$title);

		return $output;
	}
}