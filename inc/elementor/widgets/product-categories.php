<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Image_Size;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Product_Categories extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-product-categories';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Product Categories', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-product-categories';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_heading_content();
		$this->section_cat_content();
	}

	protected function section_heading_content() {
		// Settings Heading
		$this->start_controls_section(
			'section_heading',
			[ 'label' => esc_html__( 'Heading', 'teckzone' ) ]
		);
		$this->add_responsive_control(
			'header_alignment',
			[
				'label'           => esc_html__( 'Content Align', 'teckzone' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'         => [
					'row'    => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'column' => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'desktop_default' => 'row',
				'tablet_default'  => 'row',
				'mobile_default'  => 'column',
				'toggle'          => false,
				'selectors'       => [
					'{{WRAPPER}} .teckzone-product-categories .cat-header' => 'flex-direction: {{VALUE}}',
				],
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-product-categories .cat-header' => 'flex-direction: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-product-categories .cat-header' => 'flex-direction: {{VALUE}}',
						],
					],
				]
			]
		);
		$this->add_responsive_control(
			'content_position',
			[
				'label'           => __( 'Vertical Align', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					''              => __( 'Default', 'teckzone' ),
					'flex-start'    => __( 'Start', 'teckzone' ),
					'center'        => __( 'Center', 'teckzone' ),
					'flex-end'      => __( 'End', 'teckzone' ),
					'stretch' 		=> __( 'Stretch', 'teckzone' ),
					'baseline'  	=> __( 'Baseline', 'teckzone' ),
				],
				'desktop_default' => '',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .teckzone-product-categories .cat-header' => 'align-items: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'           => __( 'Horizontal Align', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					''              => __( 'Default', 'teckzone' ),
					'flex-start'    => __( 'Start', 'teckzone' ),
					'center'        => __( 'Center', 'teckzone' ),
					'flex-end'      => __( 'End', 'teckzone' ),
					'space-between' => __( 'Space Between', 'teckzone' ),
					'space-around'  => __( 'Space Around', 'teckzone' ),
					'space-evenly'  => __( 'Space Evenly', 'teckzone' ),
				],
				'desktop_default' => '',
				'tablet_default'  => 'space-between',
				'mobile_default'  => 'space-between',
				'selectors'       => [
					'{{WRAPPER}} .teckzone-product-categories .cat-header' => 'justify-content: {{VALUE}}',
				],
			]
		);
		$this->start_controls_tabs( 'section_heading_tabs' );

		$this->start_controls_tab(
			'heading_title_tab',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is the title', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'title_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'linearicons',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'heading_link_tab',
			[
				'label' => __( 'Link', 'teckzone' ),
			]
		);

		$this->add_control(
			'link_text',
			[
				'label'       => esc_html__( 'Link Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'See all categories', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'link_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-chevron-right',
					'library' => 'linearicons',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section(); // End

		// Heading Style
		$this->start_controls_section(
			'section_heading_style',
			[
				'label' => esc_html__( 'Header', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'header_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		// Style Tabs
		$this->start_controls_tabs( 'heading_style_tabs' );

		$this->start_controls_tab(
			'heading_title_style_tab',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);
		$this->add_responsive_control(
			'title_margin',
			[
				'label'              => __( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'size_units'         => [ 'px' ],
				'allowed_dimensions' => [ 'right', 'bottom' ],
				'placeholder'        => [
					'top'    => _x( 'Auto', 'Product Categories Widget', 'teckzone' ),
					'right'  => '',
					'bottom' => '',
					'left'   => _x( 'Auto', 'Product Categories Widget', 'teckzone' ),
				],
				'selectors'          => [
					'{{WRAPPER}} .techzone-product-categories .cat-header h2' => 'margin-right: {{RIGHT}}{{UNIT}}; margin-bottom: {{BOTTOM}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-product-categories .cat-header h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header h2' => 'color: {{VALUE}};',
				],
			]
		);
		// Icon
		$this->add_control(
			'title_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_control(
			'title_icon_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header h2 .teckzone-icon' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_font_size',
			[
				'label'      => esc_html__( 'Font size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-product-categories .cat-header h2 .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_right_spacing',
			[
				'label'      => esc_html__( 'Right Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-product-categories .cat-header h2 .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();
		$this->end_controls_tab();

		$this->start_controls_tab(
			'heading_link_style_tab',
			[
				'label' => __( 'Link', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'link_typography',
				'selector' => '{{WRAPPER}} .techzone-product-categories .cat-header .cats-link',
			]
		);

		$this->add_control(
			'link_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'link_color_hover',
			[
				'label'     => __( 'Hover Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link:hover span'       => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link:focus span'       => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link:hover .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link:focus .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'link_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-header .cats-link .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_cat_content() {
		// Categories Settings
		$this->start_controls_section(
			'section_content', [ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);
		$this->add_responsive_control(
			'columns',
			[
				'label'           => esc_html__( 'Columns', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					'1'  => esc_html__( '1 Column', 'teckzone' ),
					'2'  => esc_html__( '2 Columns', 'teckzone' ),
					'3'  => esc_html__( '3 Columns', 'teckzone' ),
					'4'  => esc_html__( '4 Columns', 'teckzone' ),
					'5'  => esc_html__( '5 Columns', 'teckzone' ),
					'6'  => esc_html__( '6 Columns', 'teckzone' ),
					'7'  => esc_html__( '7 Columns', 'teckzone' ),
					'8'  => esc_html__( '8 Columns', 'teckzone' ),
					'9'  => esc_html__( '9 Columns', 'teckzone' ),
					'10' => esc_html__( '10 Columns', 'teckzone' ),
				],
				'desktop_default' => '9',
				'tablet_default'  => '5',
				'mobile_default'  => '3',
				'toggle'          => false,
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-product-categories .group-item' => 'flex: 0 0 calc(1/{{VALUE}}*100%); max-width: calc(1/{{VALUE}}*100%)',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-product-categories .group-item' => 'flex: 0 0 calc(1/{{VALUE}}*100%); max-width: calc(1/{{VALUE}}*100%)',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-product-categories .group-item' => 'flex: 0 0 calc(1/{{VALUE}}*100%); max-width: calc(1/{{VALUE}}*100%)',
						],
					],
				]
			]
		);
		$this->add_control(
			'source',
			[
				'label'       => esc_html__( 'Source', 'teckzone' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'default' => esc_html__( 'Default', 'teckzone' ),
					'custom'  => esc_html__( 'Custom', 'teckzone' ),
				],
				'default'     => 'default',
				'label_block' => true,
			]
		);
		$this->add_control(
			'display',
			[
				'label'       => esc_html__( 'Categories Display', 'teckzone' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'all'    => esc_html__( 'All', 'teckzone' ),
					'parent' => esc_html__( 'Parent Categories Only', 'teckzone' ),
				],
				'default'     => 'parent',
				'label_block' => true,
				'condition'   => [
					'source' => 'default',
				],
			]
		);
		$this->add_control(
			'number',
			[
				'label'       => esc_html__( 'Categories to show', 'teckzone' ),
				'description' => esc_html__( 'Set 0 to show all categories', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 18,
				'condition'   => [
					'source' => 'default',
				],
			]
		);
		$this->add_control(
			'orderby',
			[
				'label'       => esc_html__( 'Order by', 'teckzone' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'name'  => esc_html__( 'Name', 'teckzone' ),
					'id'    => esc_html__( 'ID', 'teckzone' ),
					'count' => esc_html__( 'Count', 'teckzone' ),
				],
				'default'     => 'name',
				'label_block' => true,
				'condition'   => [
					'source' => 'default',
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'       => esc_html__( 'Order', 'teckzone' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'ASC'  => esc_html__( 'ASC', 'teckzone' ),
					'DESC' => esc_html__( 'DESC', 'teckzone' ),
				],
				'default'     => 'ASC',
				'label_block' => true,
				'condition'   => [
					'source' => 'default',
				],
			]
		);

		$this->add_control(
			'product_cat',
			[
				'label'       => esc_html__( 'Product Categories', 'teckzone' ),
				'placeholder' => esc_html__( 'Click here and start typing...', 'teckzone' ),
				'type'        => 'tzautocomplete',
				'default'     => '',
				'label_block' => true,
				'multiple'    => true,
				'source'      => 'product_cat',
				'sortable'    => true,
				'condition'   => [
					'source' => 'custom',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'label' => esc_html__( 'Category Image Size', 'teckzone' ),
				'name'      => 'image',
				// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'full',
				'separator' => 'before',
			]
		);

		$this->end_controls_section(); // End

		// Categories Style
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'item_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '20',
					'right'  => '20',
					'bottom' => '20',
					'left'   => '20',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-product-categories .group-item a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->start_controls_tabs( 'tabs_content_border_style' );

		$this->start_controls_tab(
			'content_border_normal',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);
		$this->add_control(
			'content_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .group-item' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'content_border_hover',
			[
				'label' => __( 'Hover', 'teckzone' ),
			]
		);
		$this->add_control(
			'content_border_color_hover',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .group-item a:hover:after' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		// Item
		$this->add_responsive_control(
			'item_spacing',
			[
				'label'     => __( 'Thumbnail Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .group-item img' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		// Cat info
		$this->start_controls_tabs( 'item_style_tabs' );
		$this->start_controls_tab(
			'cat_name_style_tab',
			[
				'label' => __( 'Category Name', 'teckzone' ),
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'item_typography',
				'selector' => '{{WRAPPER}} .techzone-product-categories .cat-name',
			]
		);
		$this->add_control(
			'item_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-name' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'item_hover_color',
			[
				'label'     => __( 'Hover Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .group-item a:hover .cat-name' => 'color: {{VALUE}}; box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
					'{{WRAPPER}} .techzone-product-categories .group-item a:focus .cat-name' => 'color: {{VALUE}}; box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'cat_count_style_tab',
			[
				'label' => __( 'Category Count', 'teckzone' ),
			]
		);

		$this->add_control(
			'item_count',
			[
				'label'        => esc_html__( 'Count', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'teckzone' ),
				'label_off'    => esc_html__( 'Hide', 'teckzone' ),
				'default'      => '',
				'return_value' => 'yes',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'item_count_typography',
				'selector'  => '{{WRAPPER}} .techzone-product-categories .cat-count',
				'condition' => [
					'item_count' => 'yes',
				],
			]
		);
		$this->add_control(
			'item_count_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-count' => 'color: {{VALUE}};',
				],
				'condition' => [
					'item_count' => 'yes',
				],
			]
		);
		$this->add_responsive_control(
			'item_count_top_spacing',
			[
				'label'     => __( 'Margin top', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-product-categories .cat-count' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'item_count' => 'yes',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-product-categories techzone-product-categories',
			]
		);

		$source = $settings['source'];
		$title  = $link = $icon = $title_icon = '';
		$output = [ ];

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['link_icon'] && ! empty( $settings['link_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['link_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['title'] ) {
			$title = '<h2>' . $title_icon . $settings['title'] . '</h2>';
		}

		if ( $settings['link_text'] ) {
			$link_text = '<span class="link-text">' . $settings['link_text'] . '</span>' . $icon;
			$link      = $this->get_link_control( 'link', $settings['link'], $link_text, [ 'class' => 'cats-link' ] );
		}

		if ( $title || $link ) {
			$output[] = sprintf( '<div class="cat-header">%s%s</div>', $title, $link );
		}

		$term_args = [
			'taxonomy' => 'product_cat',
		];

		if ( $source == 'default' ) {
			$display = $settings['display'];
			$parent  = '';
			if ( $display == 'parent' ) {
				$parent = 0;
			}

			$term_args['orderby'] = $settings['orderby'];
			$term_args['order']   = $settings['order'];
			$term_args['number']  = $settings['number'];
			$term_args['parent']  = $parent;

		} else {
			$cats                 = explode( ',', $settings['product_cat'] );
			$term_args['slug']    = $cats;
			$term_args['orderby'] = 'slug__in';
		}

		$terms = get_terms( $term_args );

		if ( ! is_wp_error( $terms ) && $terms ) {
			$output[] = '<ul class="product-cat-group">';
			$i        = 1;
			foreach ( $terms as $term ) {
				$thumbnail_id = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );
				$count        = '';

				if ( $settings['item_count'] == 'yes' ) {
					$count = '<span class="cat-count">';
					$count .= sprintf( _n( '%s Item', '%s Items', $term->count, 'teckzone' ), number_format_i18n( $term->count ) );
					$count .= '</span>';
				}

				// Image Size
				if ( $settings['image_size'] != 'custom' ) {
					$image_size = $settings['image_size'];
				} else {
					$image_size = [
						$settings['image_custom_dimension']['width'],
						$settings['image_custom_dimension']['height'],
					];
				}

				$item_classes = [ 'group-item' ];

				$output[] = sprintf(
					'<li class="%s">
						<a href="%s">
						%s
						<span class="cat-name">%s</span>
						%s
						</a>
					</li>',
					esc_attr( implode( ' ', $item_classes ) ),
					esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
					wp_get_attachment_image( $thumbnail_id, $image_size ),
					esc_html( $term->name ),
					$count
				);

				$i ++;
			}
			$output[] = '</ul>';
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Render link control output
	 *
	 * @param       $link_key
	 * @param       $url
	 * @param       $content
	 * @param array $attr
	 *
	 * @return string
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$tag = 'a';

		if ( empty( $attr_default['href'] ) ) {
			$tag = 'span';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $this->get_render_attribute_string( $link_key ), $content );
	}
}