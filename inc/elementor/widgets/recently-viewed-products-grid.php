<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Recently_Viewed_Products_Grid extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-recently-viewed-products-grid';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Recently Viewed Products Grid', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-posts-grid';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'section_product',
			[ 'label' => esc_html__( 'Products', 'teckzone' ) ]
		);

		$this->add_control(
			'per_page',
			[
				'label'   => esc_html__( 'Total Products', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 10,
				'min'     => 2,
				'max'     => 50,
				'step'    => 1,
			]
		);

		$this->add_control(
			'columns',
			[
				'label'   => esc_html__( 'Columns', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
				],
				'default' => '5',
				'toggle'  => false,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_pagination',
			[ 'label' => esc_html__( 'Pagination', 'teckzone' ) ]
		);
		$this->add_control(
			'pagination',
			[
				'label'        => esc_html__( 'Show Pagination', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'teckzone' ),
				'label_off'    => esc_html__( 'Hide', 'teckzone' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);
		$this->end_controls_section();

		// Style
		$this->start_controls_section(
			'section_pagination_style',
			[
				'label' => esc_html__( 'Pagination', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'alignment',
			[
				'label'     => esc_html__( 'Alignment', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'       => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => 'left',
				'selectors' => [
					'{{WRAPPER}} .tz-recently-products-grid .numeric-navigation' => 'text-align: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .tz-recently-products-grid .numeric-navigation' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : [ ];
		$viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );

		if ( empty( $viewed_products ) ) {
			return '';
		}

		$classes = [
			'tz-product-recently-viewed tz-recently-products-grid woocommerce'
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$output = [ ];

		$per_page   = intval( $settings['per_page'] );
		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'posts_per_page'      => $per_page,
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'fields'              => 'ids',
			'post__in'            => $viewed_products,
			'orderby'             => 'post__in',
		);
		if ( $settings['pagination'] == 'yes' ) {
			$paged                       = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$offset                      = ( $paged - 1 ) * $per_page;
			$query_args['offset']        = $offset;
			$query_args['no_found_rows'] = false;
		}

		$products = new \WP_Query( $query_args );

		if ( ! $products->have_posts() ) {
			return '';
		}

		global $woocommerce_loop;

		$woocommerce_loop['columns'] = intval( $settings['columns'] );

		ob_start();

		Elementor::get_loop_deals( $products->posts,  'product' );

		$output[] = ob_get_clean();

		if ( $settings['pagination'] == 'yes' ) {
			ob_start();
			$total_pages = $products->max_num_pages;
			$this->pagination_numeric( $total_pages );
			$output[] = ob_get_clean();
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Display numeric pagination
	 *
	 * @param $max_num_pages
	 */
	protected function pagination_numeric( $max_num_pages ) {
		?>
		<nav class="navigation paging-navigation numeric-navigation">
			<?php
			$big  = 999999999;
			$args = array(
				'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'total'     => $max_num_pages,
				'current'   => max( 1, get_query_var( 'paged' ) ),
				'prev_text' => '<i class="icon-chevron-left"></i>',
				'next_text' => '<i class="icon-chevron-right"></i>',
				'type'      => 'plain',
			);

			echo paginate_links( $args );
			?>
		</nav>
		<?php
	}
}