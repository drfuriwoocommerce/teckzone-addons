<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Stack;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;
use TeckzoneAddons\Elementor_AjaxLoader;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Products_With_Category_2 extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-product-with-category-2';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Product with Category 2', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-products';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Content Tab
		$this->register_heading_controls();
		$this->register_content_controls();
		$this->register_sidebar_controls();
		$this->register_products_controls();

		// Style Tab
		$this->register_heading_style_controls();
		$this->register_sidebar_style_controls();
		$this->register_products_style_controls();

		$this->_register_lazy_load_controls();
	}

	/**
	 * Register the widget heading controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_controls() {

		$this->start_controls_section(
			'section_heading',
			[ 'label' => esc_html__( 'Heading', 'teckzone' ) ]
		);
		$this->add_responsive_control(
			'header_alignment',
			[
				'label'           => esc_html__( 'Content Align', 'teckzone' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'         => [
					'row'    => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'column' => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'desktop_default' => 'row',
				'tablet_default'  => 'row',
				'mobile_default'  => 'column',
				'toggle'          => false,
				'selectors'       => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'flex-direction: {{VALUE}}',
				],
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'flex-direction: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'flex-direction: {{VALUE}}',
						],
					],
				]
			]
		);
		$this->add_responsive_control(
			'content_position',
			[
				'label'           => __( 'Vertical Align', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					''              => __( 'Default', 'teckzone' ),
					'flex-start'    => __( 'Start', 'teckzone' ),
					'center'        => __( 'Center', 'teckzone' ),
					'flex-end'      => __( 'End', 'teckzone' ),
					'stretch' 		=> __( 'Stretch', 'teckzone' ),
					'baseline'  	=> __( 'Baseline', 'teckzone' ),
				],
				'desktop_default' => '',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'align-items: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label'           => __( 'Horizontal Align', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					''              => __( 'Default', 'teckzone' ),
					'flex-start'    => __( 'Start', 'teckzone' ),
					'center'        => __( 'Center', 'teckzone' ),
					'flex-end'      => __( 'End', 'teckzone' ),
					'space-between' => __( 'Space Between', 'teckzone' ),
					'space-around'  => __( 'Space Around', 'teckzone' ),
					'space-evenly'  => __( 'Space Evenly', 'teckzone' ),
				],
				'desktop_default' => '',
				'tablet_default'  => 'space-between',
				'mobile_default'  => 'space-between',
				'selectors'       => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'justify-content: {{VALUE}}',
				],
			]
		);
		$this->start_controls_tabs( 'heading_content_settings' );

		$this->start_controls_tab( 'title_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Heading Title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'c_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$this->add_control(
			'title_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'view_all_tab', [ 'label' => esc_html__( 'Link', 'teckzone' ) ] );

		$this->add_control(
			'view_all_text',
			[
				'label'   => esc_html__( 'Text', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'See all', 'teckzone' ),
			]
		);

		$this->add_control(
			'view_all_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$this->add_control(
			'view_all_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-chevron-right',
					'library' => 'fa-solid',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_content_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => esc_html__( 'Content', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'content_align',
			[
				'label'                => esc_html__( 'Content Align', 'teckzone' ),
				'type'                 => Controls_Manager::CHOOSE,
				'options'              => [
					'horizontal' => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'vertical'   => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'default'              => 'horizontal',
				'selectors'            => [
					'{{WRAPPER}} .tz-product-with-category-2 .content-wrapper' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'horizontal' => '',
					'vertical'   => 'flex-direction: column',
				],
			]
		);

		$this->end_controls_section();
	}
	/**
	 * Register the widget sidebar controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_sidebar_controls() {

		$this->start_controls_section(
			'section_sidebar_content',
			[
				'label' => esc_html__( 'Sidebar', 'teckzone' ),
			]
		);
		$this->add_responsive_control(
			'sidebar',
			[
				'label'                => esc_html__( 'Enable', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Yes', 'teckzone' ),
				'label_off'            => esc_html__( 'No', 'teckzone' ),
				'return_value'         => 'yes',
				'desktop_default'      => 'yes',
				'tablet_default'       => 'yes',
				'mobile_default'       => '',
				'selectors_dictionary' => [
					''    => 'display: none',
					'yes' => 'display: block',
				],
				'device_args'          => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => '{{VALUE}}',
						],
					],
				]
			]
		);
//		$this->add_responsive_control(
//			'sidebar_alignment',
//			[
//				'label'           => esc_html__( 'Content Align', 'teckzone' ),
//				'type'            => Controls_Manager::CHOOSE,
//				'options'         => [
//					'row'    => [
//						'title' => esc_html__( 'Horizontal', 'teckzone' ),
//						'icon'  => 'fa fa-ellipsis-h',
//					],
//					'column' => [
//						'title' => esc_html__( 'Vertical', 'teckzone' ),
//						'icon'  => 'fa fa-ellipsis-v',
//					],
//				],
//				'desktop_default' => 'column',
//				'tablet_default'  => 'row',
//				'mobile_default'  => 'row',
//				'toggle'          => false,
//				'selectors'       => [
//					'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => 'flex-direction: {{VALUE}}',
//				],
//				'required'        => true,
//				'device_args'     => [
//					Controls_Stack::RESPONSIVE_TABLET => [
//						'selectors' => [
//							'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => 'flex-direction: {{VALUE}}',
//						],
//					],
//					Controls_Stack::RESPONSIVE_MOBILE => [
//						'selectors' => [
//							'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => 'flex-direction: {{VALUE}}',
//						],
//					],
//				]
//			]
//		);
		$this->add_control(
			'banners_settings',
			[
				'label' => esc_html__( 'Banners', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		$this->add_responsive_control(
			'banner',
			[
				'label'                => esc_html__( 'Enable', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Yes', 'teckzone' ),
				'label_off'            => esc_html__( 'No', 'teckzone' ),
				'return_value'         => 'yes',
				'desktop_default'      => 'yes',
				'tablet_default'       => 'yes',
				'mobile_default'       => '',
				'selectors_dictionary' => [
					''    => 'display: none',
					'yes' => 'display: block',
				],
				'device_args'          => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .images-slider' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .images-slider' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .images-slider' => '{{VALUE}}',
						],
					],
				]
			]
		);
		$this->start_controls_tabs( 'sidebar_content_settings' );

		$this->start_controls_tab( 'banner_images_tab', [ 'label' => esc_html__( 'Images', 'teckzone' ) ] );

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'image',
			[
				'label'   => esc_html__( 'Choose Image', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/455x328/e7eff1?text=455x328+Banner',
				],
			]
		);

		$repeater->add_control(
			'image_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'banners',
			[
				'label'         => esc_html__( 'Images', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'image'      => [
							'url' => 'https://via.placeholder.com/455x328/e7eff1?text=455x328+Banner'
						],
						'image_link' => [
							'url' => '#'
						]
					],
					[
						'image'      => [
							'url' => 'https://via.placeholder.com/455x328/e7eff1?text=455x328+Banner+2'
						],
						'image_link' => [
							'url' => '#'
						]
					]
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'carousel_settings_tab', [ 'label' => esc_html__( 'Carousel Settings', 'teckzone' ) ] );

		$this->add_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'teckzone' ),
					'arrows' => esc_html__( 'Arrows', 'teckzone' ),
					'dots'   => esc_html__( 'Dots', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default' => 'both',
			]
		);
		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 100,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'teckzone' ),
			]
		);

		//Responsive Settings
		$this->_register_responsive_settings_controls();

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'quicklink_settings',
			[
				'label'     => esc_html__( 'Quick Links', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before'
			]
		);
		$this->add_responsive_control(
			'quick_links',
			[
				'label'                => esc_html__( 'Enable', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Yes', 'teckzone' ),
				'label_off'            => esc_html__( 'No', 'teckzone' ),
				'return_value'         => 'yes',
				'desktop_default'      => 'yes',
				'tablet_default'       => 'yes',
				'mobile_default'       => '',
				'selectors_dictionary' => [
					''    => 'display: none',
					'yes' => 'display: flex',
				],
				'device_args'          => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => '{{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => '{{VALUE}}',
						],
					],
				]
			]
		);
		$this->add_control(
			'quicklink_columns',
			[
				'label'   => esc_html__( 'Columns', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'1' => esc_html__( '1 Column', 'teckzone' ),
				],
				'default' => '2',
				'toggle'  => false,
			]
		);
		$this->start_controls_tabs( 'quicklink_settings_tabs' );

		$this->start_controls_tab( 'quicklink_1_tab', [ 'label' => esc_html__( 'Quick Link 1', 'teckzone' ) ] );
		$this->add_control(
			'quicklink_title_1',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Quick link title #1', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'quicklink_url_1', [
				'label'         => esc_html__( 'URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'link_text_1', [
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_url_1', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'links_group_1',
			[
				'label'         => esc_html__( 'Items', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_text_1' => esc_html__( 'Category #1', 'teckzone' ),
						'link_url_1'  => '#',
					],
					[
						'link_text_1' => esc_html__( 'Category #2', 'teckzone' ),
						'link_url_1'  => '#',
					],
					[
						'link_text_1' => esc_html__( 'Category #3', 'teckzone' ),
						'link_url_1'  => '#',
					],

				],
				'title_field'   => '{{{ link_text_1 }}}',
				'prevent_empty' => false
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab( 'quicklink_2_tab', [ 'label' => esc_html__( 'Quick Link 2', 'teckzone' ) ] );
		$this->add_control(
			'quicklink_title_2',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Quick link title #2', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'quicklink_url_2', [
				'label'         => esc_html__( 'URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'link_text_2', [
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_url_2', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'links_group_2',
			[
				'label'         => esc_html__( 'Items', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_text_2' => esc_html__( 'Category #1', 'teckzone' ),
						'link_url_2'  => '#',
					],
					[
						'link_text_2' => esc_html__( 'Category #2', 'teckzone' ),
						'link_url_2'  => '#',
					],
					[
						'link_text_2' => esc_html__( 'Category #3', 'teckzone' ),
						'link_url_2'  => '#',
					],

				],
				'title_field'   => '{{{ link_text_2 }}}',
				'prevent_empty' => false
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Register the widget products controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_products_controls() {

		$this->start_controls_section(
			'section_product_content',
			[
				'label' => esc_html__( 'Products', 'teckzone' ),
			]
		);

		$this->add_control(
			'products',
			[
				'label'   => esc_html__( 'Products', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'recent'       => esc_html__( 'Recent', 'teckzone' ),
					'featured'     => esc_html__( 'Featured', 'teckzone' ),
					'best_selling' => esc_html__( 'Best Selling', 'teckzone' ),
					'top_rated'    => esc_html__( 'Top Rated', 'teckzone' ),
					'sale'         => esc_html__( 'On Sale', 'teckzone' ),
				],
				'default' => 'recent',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'product_cats',
			[
				'label'       => esc_html__( 'Product Categories', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy(),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);

		$this->add_control(
			'product_tags',
			[
				'label'       => esc_html__( 'Product Tags', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy( 'product_tag' ),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);

		$this->add_control(
			'per_page',
			[
				'label'   => esc_html__( 'Products per view', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 8,
				'min'     => 2,
				'max'     => 50,
				'step'    => 1,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label'        => esc_html__( 'Product Columns', 'teckzone' ),
				'type'         => Controls_Manager::SELECT,
				'options'      => [
					'1' => esc_html__( '1 Column', 'teckzone' ),
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
				],
				'toggle'       => false,
				'desktop_default' => '4',
				'tablet_default'  => '4',
				'mobile_default'  => '2',
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product' => 'width: calc(1/{{VALUE}}*100%)',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product' => 'width: calc(1/{{VALUE}}*100%)',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'selectors' => [
							'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product' => 'width: calc(1/{{VALUE}}*100%)',
						],
					],
				]
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'     => esc_html__( 'Order By', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''           => esc_html__( 'Default', 'teckzone' ),
					'date'       => esc_html__( 'Date', 'teckzone' ),
					'title'      => esc_html__( 'Title', 'teckzone' ),
					'menu_order' => esc_html__( 'Menu Order', 'teckzone' ),
					'rand'       => esc_html__( 'Random', 'teckzone' ),
				],
				'default'   => '',
				'condition' => [
					'products' => [ 'top_rated', 'sale', 'featured' ],
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'     => esc_html__( 'Order', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''     => esc_html__( 'Default', 'teckzone' ),
					'asc'  => esc_html__( 'Ascending', 'teckzone' ),
					'desc' => esc_html__( 'Descending', 'teckzone' ),
				],
				'default'   => '',
				'condition' => [
					'products' => [ 'top_rated', 'sale', 'featured' ],
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Register the widget heading style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_style_controls() {

		$this->start_controls_section(
			'section_style_heading',
			[
				'label' => esc_html__( 'Heading', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'heading_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '20',
					'right'  => '20',
					'bottom' => '20',
					'left'   => '20',
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'heading_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'heading_border',
				'label'    => esc_html__( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .header-cat',
			]
		);

		$this->add_control(
			'heading_style_divider',
			[
				'label' => '',
				'type'  => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs( 'heading_title_style_settings' );

		$this->start_controls_tab( 'heading_title_style_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );
		$this->add_responsive_control(
			'title_margin',
			[
				'label'              => __( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'size_units'         => [ 'px' ],
				'allowed_dimensions' => [ 'right', 'bottom' ],
				'placeholder'        => [
					'top'    => _x( 'Auto', 'Products With Category 2 Widget', 'teckzone' ),
					'right'  => '',
					'bottom' => '',
					'left'   => _x( 'Auto', 'Products With Category 2 Widget', 'teckzone' ),
				],
				'selectors'          => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2' => 'margin-right: {{RIGHT}}{{UNIT}}; margin-bottom: {{BOTTOM}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_title_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .header-cat h2',
			]
		);

		$this->add_control(
			'heading_title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'heading_title_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title:hover'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title:hover a' => 'color: {{VALUE}}',
				],
			]
		);

		// Icon
		$this->add_control(
			'title_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_control(
			'title_icon_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2 .teckzone-icon' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_font_size',
			[
				'label'      => esc_html__( 'Font size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2 .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_right_spacing',
			[
				'label'      => esc_html__( 'Right Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2 .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_tab();

		$this->start_controls_tab( 'heading_links_style_tab', [ 'label' => esc_html__( 'Links', 'teckzone' ) ] );

		$this->add_control(
			'heading_links_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link span' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'heading_links_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link:hover span'       => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link:hover .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_links_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .link-text',
			]
		);

		// Icon
		$this->add_control(
			'header_link_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
				'separator'    => 'before',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'header_link_icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'header_link_icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 20,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	/**
	 * Register the widget sidebar style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_sidebar_style_controls() {
		$this->start_controls_section(
			'section_style_sidebar',
			[
				'label' => esc_html__( 'Sidebar', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'sidebar_width',
			[
				'label'      => esc_html__( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'%'  => [
						'min' => 0,
						'max' => 100
					],
					'px' => [
						'min' => 0,
						'max' => 1000
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => 'max-width: {{SIZE}}{{UNIT}}; flex: 0 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'sidebar_position',
			[
				'label'   => esc_html__( 'Position', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'right' => [
						'title' => __( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'   => 'left',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'left'  => 'order: 1;',
					'right' => 'order: 3;',
				],
			]
		);
		$this->end_controls_section();

		// Banner
		$this->start_controls_section(
			'section_style_banners',
			[
				'label' => esc_html__( 'Banners', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'banners_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'arrows_style_divider',
			[
				'label' => esc_html__( 'Arrows', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
				'separator' => 'before'
			]
		);

		// Arrows
		$this->add_control(
			'arrows_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sliders_arrows_size',
			[
				'label'     => __( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_width',
			[
				'label'     => esc_html__( 'Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'width: {{SIZE}}{{UNIT}}',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'sliders_arrow_height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'sliders_arrows_offset',
			[
				'label'     => esc_html__( 'Horizontal Offset', 'teckzone' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-prev-arrow' => 'left: {{VALUE}}px;',
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-next-arrow' => 'right: {{VALUE}}px;',
				],
			]
		);

		$this->end_popover();

		$this->start_controls_tabs( 'sliders_normal_settings' );

		$this->start_controls_tab( 'sliders_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'sliders_arrow_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'sliders_arrow_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sliders_hover', [ 'label' => esc_html__( 'Hover', 'teckzone' ) ] );

		$this->add_control(
			'sliders_arrow_hover_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_hover_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow:hover' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'sliders_arrow_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		// Dots
		$this->add_control(
			'dots_style_divider',
			[
				'label'     => esc_html__( 'Dots', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'dots_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sliders_dots_width',
			[
				'label'     => esc_html__( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button'        => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button:before' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}}',
				],

			]
		);
		$this->add_responsive_control(
			'sliders_dots_top_spacing',
			[
				'label'      => esc_html__( 'Top Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => -200,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .slick-dots'        => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'sliders_dots_bottom_spacing',
			[
				'label'      => esc_html__( 'Bottom Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => -200,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .slick-dots'        => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'sliders_dots_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_dots_active_background',
			[
				'label'     => esc_html__( 'Active Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li.slick-active button:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button:hover:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_section();

		// Quick Link
		$this->start_controls_section(
			'section_style_quicklink',
			[
				'label' => esc_html__( 'Quick Link', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'quicklink_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'quicklink_padding',
			[
				'label'     => esc_html__( 'Padding', 'teckzone' ),
				'type'      => Controls_Manager::DIMENSIONS,
				'placeholder' => [
					'top'    => '40',
					'right'  => '25',
					'bottom' => '32',
					'left'   => '25',
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'quicklink_style_divider',
			[
				'label' => '',
				'type'  => Controls_Manager::DIVIDER,
			]
		);
		$this->start_controls_tabs( 'quicklink_style_settings' );
		$this->start_controls_tab( 'quicklink_title_style_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );
		$this->add_responsive_control(
			'quicklink_title_spacing',
			[
				'label'      => esc_html__( 'Margin Bottom', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'quicklink_title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4 a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'quicklink_title_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4:hover'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4:hover a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'quicklink_title_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .categories-box h4',
			]
		);
		$this->end_controls_tab();

		// Link
		$this->start_controls_tab( 'quicklink_item_style_tab', [ 'label' => esc_html__( 'Items', 'teckzone' ) ] );
		$this->add_responsive_control(
			'quicklink_items_padding',
			[
				'label'              => esc_html__( 'Padding', 'elementor' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'size_units'         => [ 'px', '%' ],
				'allowed_dimensions' => 'vertical',
				'placeholder'        => [
					'top'    => '2',
					'right'  => 'auto',
					'bottom' => '2',
					'left'   => 'auto',
				],
				'selectors'          => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links li'             => 'padding-top: {{TOP}}{{UNIT}}; padding-bottom: {{BOTTOM}}{{UNIT}};',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links li:first-child' => 'padding-top: 0;',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links li:last-child'  => 'padding-bottom: 0;',
				],
			]
		);
		$this->add_control(
			'quicklink_items_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'quicklink_items_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links a:hover' => 'color: {{VALUE}}; box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'quicklink_link_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();
		$this->end_controls_section();
	}

	protected function _register_responsive_settings_controls() {
		$this->add_control(
			'responsive_settings_divider',
			[
				'label' => __( 'Responsive Settings', 'teckzone' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'responsive_breakpoint', [
				'label' => __( 'Breakpoint', 'teckzone' ) . ' (px)',
				'description' => __( 'Below this breakpoint the options below will be triggered', 'teckzone' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 1200,
				'min'             => 320,
				'max'             => 1920,
			]
		);
		$repeater->add_control(
			'responsive_navigation',
			[
				'label'           => esc_html__( 'Navigation', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					'both'   => esc_html__( 'Arrows and Dots', 'teckzone' ),
					'arrows' => esc_html__( 'Arrows', 'teckzone' ),
					'dots'   => esc_html__( 'Dots', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default' => 'dots',
				'toggle'          => false,
			]
		);

		$this->add_control(
			'carousel_responsive_settings',
			[
				'label' => __( 'Settings', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default' => [
					[
						'responsive_breakpoint' => 1025,
						'responsive_navigation' => 'dots',
					],
					[
						'responsive_breakpoint' => 768,
						'responsive_navigation' => 'dots',
					],
				],
				'title_field' => '{{{ responsive_breakpoint }}}' . 'px',
				'prevent_empty' => false,
			]
		);
	}

	/**
	 * Register the widget products style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_products_style_controls() {

		$this->start_controls_section(
			'section_style_products',
			[
				'label' => esc_html__( 'Products', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'products_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .products-box' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-with-category-2 .products-box:after' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'products_item_border_color',
			[
				'label'     => esc_html__( 'Hover Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-inner:hover'   => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-details-hover' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'action_button',
			[
				'label'                => esc_html__( 'Wishlist/Compare Text', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Show', 'teckzone' ),
				'label_off'            => esc_html__( 'Hide', 'teckzone' ),
				'return_value'         => 'yes',
				'default'              => 'yes',
				'selectors_dictionary' => [
					''    => 'display: none',
				],
				'selectors'   => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-button .group a span' => '{{VALUE}}',
				],
			]
		);
		$this->add_control(
			'swatches',
			[
				'label'                => esc_html__( 'Swatches', 'teckzone' ),
				'type'                 => Controls_Manager::SWITCHER,
				'label_on'             => esc_html__( 'Show', 'teckzone' ),
				'label_off'            => esc_html__( 'Hide', 'teckzone' ),
				'return_value'         => 'yes',
				'default'              => '',
				'selectors_dictionary' => [
					''    => 'display: none',
					'yes' => 'display: block',
				],
				'selectors'   => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-thumbnail .tz-attr-swatches' => '{{VALUE}}',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function _register_lazy_load_controls() {
		// Content
		$this->start_controls_section(
			'section_lazy_load',
			[ 'label' => esc_html__( 'Lazy Load', 'teckzone' ) ]
		);
		$this->add_control(
			'lazy_load',
			[
				'label'        => esc_html__( 'Enable', 'teckzone' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'teckzone' ),
				'label_off'    => esc_html__( 'No', 'teckzone' ),
				'default'      => '',
			]
		);
		$this->add_responsive_control(
			'lazy_load_height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 10,
						'max' => 1000,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-elementor-ajax-wrapper .teckzone-loading-wrapper' => 'min-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section(); // End

		// Style
		$this->start_controls_section(
			'section_lazy_load_style',
			[
				'label' => esc_html__( 'Lazy Load', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'loading_width',
			[
				'label'      => esc_html__( 'Loading Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 10,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-elementor-ajax-wrapper .teckzone-loading:after' => 'width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'loading_height',
			[
				'label'      => esc_html__( 'Loading Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 10,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-elementor-ajax-wrapper .teckzone-loading:after' => 'height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'loading_border_color',
			[
				'label'     => esc_html__( 'Loading Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-elementor-ajax-wrapper .teckzone-loading:after' => 'border-color: {{VALUE}} transparent {{VALUE}} transparent;',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'tz-product-with-category-2 woocommerce',
				$settings['lazy_load'] == 'yes' ? '' : 'no-infinite'
			]
		);

		$is_rtl    = is_rtl();
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$this->add_render_attribute( 'wrapper', 'dir', $direction );

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php if ( $settings['lazy_load'] == 'yes' ) : ?>
				<?php
				// AJAX settings
				$this->add_render_attribute(
					'ajax_wrapper', 'class', [
						'tz-product-with-category-2-loading tz-elementor-ajax-wrapper'
					]
				);
				$ajax_settings = [
					'title'							=> $settings['title'],
					'c_link'						=> $settings['c_link'],
					'title_icon'					=> $settings['title_icon'],
					'view_all_text'					=> $settings['view_all_text'],
					'view_all_link'					=> $settings['view_all_link'],
					'view_all_icon'					=> $settings['view_all_icon'],
					'banners'						=> $settings['banners'],
					'quicklink_columns'				=> $settings['quicklink_columns'],
					'quicklink_title_1'				=> $settings['quicklink_title_1'],
					'quicklink_url_1'				=> $settings['quicklink_url_1'],
					'links_group_1'					=> $settings['links_group_1'],
					'quicklink_title_2'				=> $settings['quicklink_title_2'],
					'quicklink_url_2'				=> $settings['quicklink_url_2'],
					'links_group_2'					=> $settings['links_group_2'],
					'columns'						=> $settings['columns'],
					'products' 						=> $settings['products'],
					'product_cats'					=> $settings['product_cats'],
					'product_tags'					=> $settings['product_tags'],
					'per_page' 						=> $settings['per_page'],
					'orderby' 						=> $settings['orderby'],
					'order'							=> $settings['order'],
					'navigation'					=> $settings['navigation'],
					'infinite'						=> $settings['infinite'],
					'autoplay'						=> $settings['autoplay'],
					'autoplay_speed'				=> $settings['autoplay_speed'],
					'speed'							=> $settings['speed'],
					'carousel_responsive_settings'	=> $settings['carousel_responsive_settings'],
				];
				$this->add_render_attribute( 'ajax_wrapper', 'data-settings', wp_json_encode( $ajax_settings ) );
				?>
                <div <?php echo $this->get_render_attribute_string( 'ajax_wrapper' ); ?>>
					<div class="teckzone-loading-wrapper"><div class="teckzone-loading"></div></div>
                </div>
			<?php else : ?>
				<?php Elementor_AjaxLoader::get_products_with_category_2( $settings ); ?>
			<?php endif; ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}
}