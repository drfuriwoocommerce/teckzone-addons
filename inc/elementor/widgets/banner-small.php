<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Background;
use Elementor\Controls_Stack;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Banner Small widget
 */
class Banner_Small extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-banner-small';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Banner Small', 'teckzone' );
	}


	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-banner';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'banner_content_settings' );

		$this->start_controls_tab( 'section_sale', [ 'label' => esc_html__( 'Sale', 'teckzone' ) ] );

		$this->add_control(
			'sale',
			[
				'label'       => esc_html__( 'Number', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your text', 'teckzone' ),
				'label_block' => true,
				'default'     => __( '40', 'teckzone' ),
			]
		);

		$this->add_control(
			'after_unit',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your text', 'teckzone' ),
				'label_block' => true,
				'default'     => __( 'OFF', 'teckzone' ),
			]
		);

		$this->add_control(
			'unit',
			[
				'label'       => esc_html__( 'Unit', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your unit', 'teckzone' ),
				'label_block' => true,
				'default'     => __( '%', 'teckzone' ),
				'separator' => 'after',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'content', [ 'label' => esc_html__( 'Content', 'teckzone' ) ] );

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the title', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => '',
				'placeholder' => esc_html__( 'Enter your description', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'background', [ 'label' => esc_html__( 'Background', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'banners_background',
				'label'    => __( 'Background', 'teckzone' ),
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .techzone-banner-small .banner-featured-image',
				'fields_options'  => [
					'background' => [
						'default' => 'classic',
					],
					'image' => [
						'default'   => [
							'url' => 'https://via.placeholder.com/1170X285/f8f8f8?text=270x285+Image',
						],
					],
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section(); // End

		$this->start_controls_section(
			'section_button',
			[ 'label' => esc_html__( 'Button', 'teckzone' ) ]
		);

		$this->add_control(
			'button_text',
			[
				'label'   => esc_html__( 'View All Text', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Shop all', 'teckzone' ),
			]
		);

		$this->add_control(
			'button_link', [
				'label'         => esc_html__( 'Link URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'button_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-chevron-right',
					'library' => 'linearicons',
				],
			]
		);
		$this->end_controls_section(); // End Button Settings
	}

	// Tab Style
	protected function section_style(){
		$this->section_style_banner();
		$this->section_style_sale();
		$this->section_style_title();
		$this->section_style_desc();
		$this->section_line_style();
		$this->section_button_style();
	}

	protected function section_style_banner(){
		// Banner
		$this->start_controls_section(
			'section_style_banner',
			[
				'label' => esc_html__( 'Banner', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 100,
						'max' => 600,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-content-wrapper' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'banner_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'default'    => [],
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '29',
					'right'  => '30',
					'bottom' => '25',
					'left'   => '30',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-small .banner-content-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'horizontal_align',
			[
				'label'       => __( 'Horizontal Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'flex-start'   => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'eicon-h-align-center',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'desktop_default' => 'center',
				'selectors'       => [
					'{{WRAPPER}} .techzone-banner-small .banner-content-wrapper' => 'justify-content: {{VALUE}}',
				],
				'separator'   => 'before',
			]
		);
		$this->add_responsive_control(
			'vertical_align',
			[
				'label'       => __( 'Vertical Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'flex-start'   => [
						'title' => esc_html__( 'Top', 'teckzone' ),
						'icon'  => 'eicon-v-align-top',
					],
					'center' => [
						'title' => esc_html__( 'Middle', 'teckzone' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Bottom', 'teckzone' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'desktop_default' => 'flex-start',
				'selectors'       => [
					'{{WRAPPER}} .techzone-banner-small .banner-content-wrapper' => 'align-items: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'banner_text_align',
			[
				'label'       => esc_html__( 'Text Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'desktop_default'     => 'center',
				'tablet_default'     => 'center',
				'mobile_default'     => 'center',
				'selectors'   => [
					'{{WRAPPER}} .techzone-banner-small .banner-content-wrapper' => 'text-align: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'banner_content_css_1',
			[
				'label'                => __( 'Banner Content CSS', 'teckzone' ),
				'type'                 => Controls_Manager::HIDDEN,
				'desktop_default'      => 'left',
				'tablet_default'       => 'left',
				'mobile_default'       => 'left',
				'selectors_dictionary' => [
					'left' => 'justify-content: flex-start',
				],
				'device_args'          => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'condition'  => [
							'banner_text_align' => 'left',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'condition'  => [
							'banner_text_align_tablet' => 'left',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'condition'  => [
							'banner_text_align_mobile' => 'left',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
				]
			]
		);
		$this->add_responsive_control(
			'banner_content_css_2',
			[
				'label'                => __( 'Banner Content CSS', 'teckzone' ),
				'type'                 => Controls_Manager::HIDDEN,
				'desktop_default'      => 'right',
				'tablet_default'       => 'right',
				'mobile_default'       => 'right',
				'selectors_dictionary' => [
					'right' => 'justify-content: flex-end',
				],
				'device_args'          => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'condition'  => [
							'banner_text_align' => 'right',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'condition'  => [
							'banner_text_align_tablet' => 'right',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'condition'  => [
							'banner_text_align_mobile' => 'right',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
				]
			]
		);
		$this->add_responsive_control(
			'banner_content_css_3',
			[
				'label'                => __( 'Banner Content CSS', 'teckzone' ),
				'type'                 => Controls_Manager::HIDDEN,
				'desktop_default'      => 'center',
				'tablet_default'       => 'center',
				'mobile_default'       => 'center',
				'selectors_dictionary' => [
					'center' => 'justify-content: center',
				],
				'device_args'          => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'condition'  => [
							'banner_text_align' => 'center',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
					Controls_Stack::RESPONSIVE_TABLET  => [
						'condition'  => [
							'banner_text_align_tablet' => 'center',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
					Controls_Stack::RESPONSIVE_MOBILE  => [
						'condition'  => [
							'banner_text_align_mobile' => 'center',
						],
						'selectors'  => [
							'{{WRAPPER}} .techzone-banner-small .line' => '{{VALUE}}',
						]
					],
				]
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'banner_border',
				'label'     => esc_html__( 'Border', 'teckzone' ),
				'selector'  => '{{WRAPPER}} .techzone-banner-small .banner-content-wrapper',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_sale(){
		// Title
		$this->start_controls_section(
			'section_style_sale',
			[
				'label' => esc_html__( 'Sale', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'banner_sale_border',
				'label'     => esc_html__( 'Border', 'teckzone' ),
				'selector'  => '{{WRAPPER}} .techzone-banner-small .banner-sale',
			]
		);

		$this->add_responsive_control(
			'banner_sale_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'default'    => [],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-small .banner-sale' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);

		$this->start_controls_tabs( 'sale_tabs' );

		$this->start_controls_tab( 'sale_number', [ 'label' => esc_html__( 'Number', 'teckzone' ) ] );

		$this->add_control(
			'sale_number_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .text-1' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-small .text-1',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sale_after_unit', [ 'label' => esc_html__( 'Text', 'teckzone' ) ] );

		$this->add_control(
			'sale_afunit_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .text-2' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_text_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-small .text-2',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sale_unit', [ 'label' => esc_html__( 'Unit', 'teckzone' ) ] );

		$this->add_control(
			'position_y',
			[
				'label'     => esc_html__( 'Left', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units' => [ '%' ,'px'],
				'range'     => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-sale .unit' => 'left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'position_x',
			[
				'label'     => esc_html__( 'Top', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units' => [ '%' ,'px' ],
				'range'     => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-sale .unit' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'sale_unit_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .unit' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_unit_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-small .unit',

			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_style_title(){
		// Title
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'heading_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-title' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-small .banner-title',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_desc(){
		// Description
		$this->start_controls_section(
			'section_style_description',
			[
				'label' => esc_html__( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'description_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-desc' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'description_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .banner-desc' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-small .banner-desc',
			]
		);

		$this->end_controls_section();
	}

	protected function section_line_style() {
		$this->start_controls_section(
			'section_line_style',
			[
				'label' => __( 'Line', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'line_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .line' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
					'%'  => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'    => [ ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-small .line div' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 5,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .line div' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'line_background_color',
			[
				'label'     => __( 'Bachground Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .line div' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function section_button_style() {
		// Heading Style
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_links_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .header-link span' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'heading_links_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .header-link:hover span'       => 'color: {{VALUE}}',
					'{{WRAPPER}} .techzone-banner-small .header-link:hover .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_links_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-small .header-link .link-text',
			]
		);

		// Icon
		$this->add_control(
			'header_link_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'header_link_icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .header-link .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-banner-small .header-link .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'header_link_icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-small .header-link .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_section();
	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute( 'wrapper', 'class', [
			'techzone-banner-small teckzone-banner-small',
		] );

		if ( $settings['button_link']['is_external'] ) {
			$this->add_render_attribute( 'link', 'target', '_blank' );
		}

		if ( $settings['button_link']['nofollow'] ) {
			$this->add_render_attribute( 'link', 'rel', 'nofollow' );
		}

		if ( $settings['button_link']['url'] ) {
			$this->add_render_attribute( 'link', 'href', $settings['button_link']['url'] );
		}

		$sale       = $settings['sale'] ? sprintf('<div class="text-1">%s</div>',$settings['sale']) : '';
		$unit       = $settings['unit'] ? sprintf('<div class="unit">%s</div>',$settings['unit']) : '';
		$af_unit    = $settings['after_unit'] ? sprintf('<div class="text-2">%s</div>',$settings['after_unit']) : '';

		$box_sale = $sale . $af_unit . $unit;

		// Button
		$view_all_icon = $link = '';
		if ( $settings['button_icon'] && ! empty( $settings['button_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['button_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['button_text'] ) {
			$text = '<span class="link-text">' . $settings['button_text'] . '</span>' . $view_all_icon;
			$link = $this->get_link_control( 'header_link', $settings['button_link'], $text, [ 'class' => 'header-link' ] );
		}

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="banner-content-wrapper">
				<div class="banner-featured-image"></div>
				<div class="banner-content">
					<?php if ( $settings['button_link']['url'] ) : ?>
						<a class="link" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
					<?php endif; ?>

					<?php if ( ! empty($settings['title']) ) : ?>
						<h2 class="banner-title"><?php echo $settings['title']; ?></h2>
						<div class="line"><div></div></div>
					<?php endif; ?>
					<?php if ( ! empty($box_sale) ) : ?>
						<div class="banner-sale"><?php echo $box_sale ?></div>
					<?php endif; ?>
					<?php if ( ! empty( $settings['desc'] ) ) : ?><div class="banner-desc"><?php echo $settings['desc']; ?></div><?php endif; ?>
				</div>
			</div>
			<?php echo $link; ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {

	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$tag = 'a';

		if ( empty( $attr_default['href'] ) ) {
			$tag = 'span';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $this->get_render_attribute_string( $link_key ), $content );
	}
}