<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Controls_Stack;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Simple_Text extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-simple-text';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Simple Text', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-animation-text';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_responsive_control(
			'content_alignment',
			[
				'label'           => esc_html__( 'Content Align', 'teckzone' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'         => [
					'row'    => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'column' => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'desktop_default' => 'row',
				'tablet_default'  => 'row',
				'mobile_default'  => 'row',
				'toggle'          => false,
				'selectors'       => [
					'{{WRAPPER}} .teckzone-simple-text a' => 'flex-direction: {{VALUE}}',
				],
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-simple-text a' => 'flex-direction: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}} .teckzone-simple-text a' => 'flex-direction: {{VALUE}}',
						],
					],
				]
			]
		);
		$this->add_responsive_control(
			'content_v_position',
			[
				'label'           => __( 'Vertical Align', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					''              => __( 'Default', 'teckzone' ),
					'flex-start'    => __( 'Start', 'teckzone' ),
					'center'        => __( 'Center', 'teckzone' ),
					'flex-end'      => __( 'End', 'teckzone' ),
					'stretch' 		=> __( 'Stretch', 'teckzone' ),
					'baseline'  	=> __( 'Baseline', 'teckzone' ),
				],
				'desktop_default' => '',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .teckzone-simple-text a' => 'align-items: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_h_position',
			[
				'label'           => __( 'Horizontal Align', 'teckzone' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					''              => __( 'Default', 'teckzone' ),
					'flex-start'    => __( 'Start', 'teckzone' ),
					'center'        => __( 'Center', 'teckzone' ),
					'flex-end'      => __( 'End', 'teckzone' ),
					'space-between' => __( 'Space Between', 'teckzone' ),
					'space-around'  => __( 'Space Around', 'teckzone' ),
					'space-evenly'  => __( 'Space Evenly', 'teckzone' ),
				],
				'desktop_default' => '',
				'tablet_default'  => 'space-between',
				'mobile_default'  => 'space-between',
				'selectors'       => [
					'{{WRAPPER}} .teckzone-simple-text a' => 'justify-content: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
				'separator' => 'before'
			]
		);

		$this->add_control(
			'title',
			[
				'label'           => esc_html__( 'Title', 'teckzone' ),
				'type'            => Controls_Manager::TEXT,
				'default'         => esc_html__( 'This is the title', 'teckzone' ),
				'label_block'     => true,
			]
		);

		$this->add_control(
			'af_title',
			[
				'label'       => esc_html__( 'After Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
				'separator'     => 'before',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'general_align',
			[
				'label'     => esc_html__( 'Align', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'flex-start' => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center'     => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end'   => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text a' => 'justify-content: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-simple-text a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '',
					'right'  => '13',
					'bottom' => '',
					'left'   => '',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-simple-text a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'general_tabs' );

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);

		$this->add_control(
			'color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => __( 'Hover', 'teckzone' ),
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_active_tab',
			[
				'label' => __( 'Active', 'teckzone' ),
			]
		);

		$this->add_control(
			'active_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text a.active' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		// Elements
		$this->add_control(
			'elements',
			[
				'label'     => __( 'Elements', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'general_content_tabs' );

		// Icon
		$this->start_controls_tab(
			'icon_style_tab',
			[
				'label' => __( 'Icon', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'icon_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'placeholder' => [
					'top'    => '',
					'right'  => '7',
					'bottom' => '',
					'left'   => '',
				],
				'default'    => [ ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-simple-text .teckzone-icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-simple-text .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};'
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-simple-text .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Title
		$this->start_controls_tab(
			'title_style_tab',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'title_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top'    => '',
					'right'  => '',
					'bottom' => '',
					'left'   => '',
				],
				'selectors'  => [
					'{{WRAPPER}} .techzone-simple-text .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text .title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-simple-text .title',
			]
		);

		$this->end_controls_tab();

		// After Title
		$this->start_controls_tab(
			'af_title_style_tab',
			[
				'label' => __( 'After Title', 'teckzone' ),
			]
		);

		$this->add_control(
			'aftitle_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text .af-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'aftitle_typography',
				'selector' => '{{WRAPPER}} .techzone-simple-text .af-title',
			]
		);

		$this->end_controls_tab();

		// Sep
		$this->start_controls_tab(
			'sep_style_tab',
			[
				'label' => __( 'Separator', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'line_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 5,
					],
				],
				'default'    => [ ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-simple-text .line' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_height',
			[

				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
					'%'  => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text .line' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'line_background_color',
			[
				'label'     => __( 'Bachground Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-simple-text .line' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$classes = [
			'teckzone-simple-text',
			'techzone-simple-text',
		];

		$page_id = $link_css = '';
		if ( ! empty( $settings['link']['url'] ) ) {
			$page_id = url_to_postid($settings['link']['url']);
		}

		if ( ! empty( $page_id ) && get_queried_object_id() == $page_id ) {
			$link_css = 'active';
		}

		$icon = $title = '';

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$line = '<span class="line"></span>';

		if ( $settings['title'] ) {
			$title .= '<span class="title title--desktop">' . $settings['title'] . '</span>';
		}

		if ( $settings['af_title'] ) {
			$title .= '<span class="af-title">' . $settings['af_title'] . '</span>';
		}
		$title_html = sprintf( '<span class="box-title">%s</span>', $title );

		$output = $icon . $title_html . $line;

		$output_html = $this->get_link_control( 'link', $settings['link'], $output, [ 'class' => $link_css ]);

		$this->add_render_attribute('wrapper', 'class', $classes);

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$output_html
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
		?>
		<#
			var link = settings.link.url ? 'href="' + settings.link.url + '"' : '',
			iconTag = link ? 'a' : 'span',
			title = settings.title,
			af_title = settings.af_title,
			iconHTML = elementor.helpers.renderIcon( view, settings.icon, { 'aria-hidden': true }, 'i' , 'object' );

			#>
			<div class="teckzone-simple-text techzone-simple-text">
				<{{{ iconTag + ' ' + link }}}>
				<# if ( iconHTML && iconHTML.rendered ) { #>
					<span class="teckzone-icon">{{{ iconHTML.value }}}</span>
					<# } #>

					<span class="box-title">
						<# if ( title ){ #>
							<span class="title">{{{title}}}</span>
							<# } #>

								<# if ( af_title ){ #>
									<span class="af-title">{{{ af_title }}}</span>
									<# } #>
					</span>
						<span class="line"></span>
					</{{{ iconTag }}}>
			</div>
		<?php
	}

	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$tag = 'a';

		if ( empty( $attr_default['href'] ) ) {
			$tag = 'span';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $this->get_render_attribute_string( $link_key ), $content );
	}
}