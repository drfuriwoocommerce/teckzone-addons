<?php

namespace TeckzoneAddons;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor_Controls {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->init();
	}

	public function init() {

		// Include plugin files
		$this->includes();

		// Register controls
		add_action( 'elementor/controls/controls_registered', [ $this, 'register_controls' ] );
	}

	public function includes() {
		require_once( TECKZONE_ADDONS_DIR . 'inc/elementor/controls/ajaxloader.php' );
		require_once( TECKZONE_ADDONS_DIR . 'inc/elementor/controls/tzautocomplete.php' );

		require_once( TECKZONE_ADDONS_DIR . 'inc/elementor/elements/column.php' );
		require_once( TECKZONE_ADDONS_DIR . 'inc/elementor/elements/section.php' );
	}

	public function register_controls() {
		$controls_manager = \Elementor\Plugin::$instance->controls_manager;
		$controls_manager->register_control( 'tzautocomplete', new TZautocomplete() );
	}
}

new Elementor_Controls();