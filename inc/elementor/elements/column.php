<?php

use Elementor\Controls_Stack;
use Elementor\Controls_Manager;
use Elementor\Element_Column;
use Elementor\Core\Base\Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class TZ_Element_Column extends Module {

	public function __construct() {

		$this->add_actions();
	}

	public function get_name() {
		return 'tz-content-align';
	}

	/**
	 * @param $element    Controls_Stack
	 * @param $section_id string
	 */
	public function register_controls( Controls_Stack $element, $section_id ) {
		if ( ! $element instanceof Element_Column ) {
			return;
		}
		$required_section_id = 'layout';

		if ( $required_section_id !== $section_id ) {
			return;
		}

		$element->add_responsive_control(
			'_tz_column_min_width',
			[
				'label'        => esc_html__( 'Min Width (px)', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}}',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}}',
						],
					],
				],
				'separator' => 'before',
			]
		);

		$element->add_control(
			'_tz_content_align',
			[
				'label'        => esc_html__( 'Content Align', 'teckzone' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'vertical'   => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
					'horizontal' => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
				],
				'default'      => 'vertical',
				'prefix_class' => 'tz-flex-column-',
			]
		);

		$element->add_responsive_control(
			'_tz_content_wrap',
			[
				'label'        => esc_html__( 'Content Wrap', 'teckzone' ),
				'type'         => Controls_Manager::SELECT,
				'options'   => [
					'wrap'         => esc_html__( 'Wrap', 'teckzone' ),
					'nowrap'       => esc_html__( 'No wrap', 'teckzone' ),
					'wrap-reverse' => esc_html__( 'Wrap Reverse', 'teckzone' ),
				],
				'desktop_default' => 'wrap',
				'tablet_default'  => 'wrap',
				'mobile_default'  => 'wrap',
				'toggle'          => false,
				'required'        => true,
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}} > .elementor-column-wrap > .elementor-widget-wrap' => 'flex-wrap: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}} > .elementor-column-wrap > .elementor-widget-wrap' => 'flex-wrap: {{VALUE}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}} > .elementor-column-wrap > .elementor-widget-wrap' => 'flex-wrap: {{VALUE}}',
						],
					],
				],
				'condition' => [
					'_tz_content_align' => 'horizontal'
				],
			]
		);

		$element->update_control(
			'content_position',
			['prefix_class' => 'tz-column-items-']
		);
	}

	protected function add_actions() {
		add_action( 'elementor/element/before_section_end', [ $this, 'register_controls' ], 10, 2 );
	}
}

new TZ_Element_Column();
