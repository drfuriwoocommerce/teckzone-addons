<?php

use Elementor\Element_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Base\Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class TZ_Element_Section extends Module {

	public function __construct() {

		if ( defined('ELEMENTOR_PRO_VERSION') ) {
			return;
		}

		$this->add_actions();
	}

	public function get_name() {
		return 'tz-section';
	}

	/**
	 * @param $element    Controls_Stack
	 * @param $section_id string
	 */
	public function register_controls( Element_Base $element ) {
		$element->add_control(
			'tz_sticky',
			[
				'label'   => __( 'Sticky', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					''    => __( 'None', 'teckzone' ),
					'top' => __( 'Top', 'teckzone' ),
				],
				'separator'          => 'before',
				'render_type'        => 'none',
				'frontend_available' => true,
			]
		);

		$element->add_control(
			'tz_sticky_offset',
			[
				'label' => __( 'Offset', 'teckzone' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 0,
				'min' => 0,
				'max' => 500,
				'required' => true,
				'condition' => [
					'tz_sticky!' => '',
				],
				'render_type' => 'none',
				'frontend_available' => true,
			]
		);
		$element->add_control(
			'tz_sticky_z_index',
			[
				'label' => __( 'Z-index', 'teckzone' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 0,
				'required' => true,
				'condition' => [
					'tz_sticky!' => '',
				],
				'render_type' => 'none',
				'frontend_available' => true,
			]
		);

		$element->add_control(
			'sticky_divider',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
	}

	/**
	 * @param $element Element_Base
	 */
	public function render_attributes( Element_Base $element ) {
		$settings = $element->get_settings_for_display();
		$classes = [];
		if ( ! empty( $settings['tz_sticky'] ) ) {
			$classes[] = 'tz-sticky-on';
		}
		$element->add_render_attribute( '_wrapper', [
			'class' => $classes,
		] );
	}

	public function register_frontend_scripts() {
		wp_register_script( 'teckzone-sticky', TECKZONE_ADDONS_URL . '/assets/js/plugins/jquery.sticky.js', array( 'jquery' ), '1.0.4', true );
	}

	public function enqueue_frontend_scripts() {
		wp_enqueue_script( 'techzone-elementor-section', TECKZONE_ADDONS_URL . '/assets/js/elementor-section.js',
			[
				'teckzone-sticky',
			],
			'20200515',
			true
		);
	}

	protected function add_actions() {
		add_action( 'elementor/frontend/before_register_scripts', [ $this, 'register_frontend_scripts' ] );
		add_action( 'elementor/frontend/before_enqueue_scripts', [ $this, 'enqueue_frontend_scripts' ] );

		add_action( 'elementor/element/section/section_effects/after_section_start', [ $this, 'register_controls' ], 10, 2  );
		add_action( 'elementor/element/after_add_attributes', [ $this, 'render_attributes' ], 10, 2 );
	}
}

new TZ_Element_Section();
