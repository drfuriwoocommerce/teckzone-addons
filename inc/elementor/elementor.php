<?php

namespace TeckzoneAddons;

/**
 * Integrate with Elementor.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor {
	/**
	 * Instance
	 *
	 * @access private
	 */
	private static $_instance = null;


	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @return Teckzone_Addons_Elementor An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();
	}

	/**
	 * Auto load widgets
	 */
	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		if ( false === strpos( $class, 'Widgets' ) ) {
			return;
		}

		$path     = explode( '\\', $class );
		$filename = strtolower( array_pop( $path ) );

		$folder = array_pop( $path );

		if ( ! in_array( $folder, array( 'Widgets', 'Headers', 'Footer', 'Mobile' ) ) ) {
			return;
		}

		if ( 'Headers' == $folder || 'Footer' == $folder || 'Mobile' == $folder ) {
			$filename = strtolower( $folder ) . '/' . $filename;
		}

		$filename = str_replace( '_', '-', $filename );
		$filename = TECKZONE_ADDONS_DIR . 'inc/elementor/widgets/' . $filename . '.php';

		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	/**
	 * Includes files which are not widgets
	 */
	private function _includes() {
		include_once( TECKZONE_ADDONS_DIR . 'inc/elementor/widgets/ajaxloader.php' );
	}

	/**
	 * Hooks to init
	 */
	protected function add_actions() {

		if ( ! empty( $_REQUEST['action'] ) && 'elementor' === $_REQUEST['action'] && is_admin() ) {
			add_action( 'init', [ $this, 'register_wc_hooks' ], 5 );
		}

		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'styles' ] );
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'scripts' ] );

		add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

		add_action( 'elementor/elements/categories_registered', [ $this, 'add_category' ] );

		add_action( 'elementor/editor/after_enqueue_styles', [ $this, 'editor_styles' ] );

		add_action( 'post_class', [ $this, 'get_product_classes' ], 20, 3 );

		// Products without Load more button
		add_action( 'wc_ajax_nopriv_tz_elementor_load_products', [ $this, 'elementor_load_products' ] );
		add_action( 'wc_ajax_tz_elementor_load_products', [ $this, 'elementor_load_products' ] );

		// Products list
		add_action( 'wc_ajax_nopriv_teckzone_ajax_load_products_list', [ $this, 'ajax_load_products_list' ] );
		add_action( 'wc_ajax_teckzone_ajax_load_products_list', [ $this, 'ajax_load_products_list' ] );

		// Products with Load more button
		add_action( 'wc_ajax_nopriv_teckzone_ajax_load_products', [ $this, 'ajax_load_products' ] );
		add_action( 'wc_ajax_teckzone_ajax_load_products', [ $this, 'ajax_load_products' ] );

		// Products deal with Load more button
		add_action( 'wc_ajax_nopriv_teckzone_ajax_load_products_deal', [ $this, 'ajax_load_products_deal' ] );
		add_action( 'wc_ajax_teckzone_ajax_load_products_deal', [ $this, 'ajax_load_products_deal' ] );

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles' ] );
	}

	public function register_wc_hooks() {
		if ( ! class_exists( 'WooCommerce' ) ) {
			return;
		}
		wc()->frontend_includes();
	}

	public function enqueue_styles() {
		\Elementor\Plugin::$instance->frontend->enqueue_styles();
	}

	public function get_product_classes( $classes, $class, $post_id ) {
		if ( is_admin() && \Elementor\Plugin::$instance->preview->is_preview_mode() ) {
			$post      = get_post( $post_id );
			$classes[] = $post->post_type;
		}

		return $classes;
	}

	/**
	 * Register styles
	 */
	public function styles() {
	}

	/**
	 * Register styles
	 */
	public function scripts() {
		wp_register_script( 'techzone-elementor', TECKZONE_ADDONS_URL . '/assets/js/elementor.js', array( 'jquery' ), '20170530', true );
		wp_register_script( 'teckzone-masonry', TECKZONE_ADDONS_URL . '/assets/js/plugins/jquery.masonryGrid.js', array( 'jquery' ), '1.0.0', true );
		wp_localize_script(
			'techzone-elementor', 'tz_elementor_data', array(
			'ajax_url' => $this->get_endpoint( '%%endpoint%%' ),
		) );
	}

	public function get_endpoint( $request = '' ) {
		return esc_url_raw( apply_filters( 'woocommerce_ajax_get_endpoint', add_query_arg( 'wc-ajax', $request, remove_query_arg( array(
			'remove_item',
			'add-to-cart',
			'added-to-cart',
			'order_again',
			'_wpnonce'
		), home_url( '/', 'relative' ) ) ), $request ) );
	}

	/**
	 * Register styles
	 */
	public function editor_styles() {
		wp_enqueue_style( 'linearicons', TECKZONE_ADDONS_URL . 'assets/css/icons/linearicons.min.css', array(), '1.0.0' );
	}

	/**
	 * Init Controls
	 */
	public function init_controls( $controls_registry ) {
		add_action( 'elementor/icons_manager/additional_tabs', [ $this, 'elementor_custom_icons' ] );

	}

	/**
	 * Init Widgets
	 */
	public function init_widgets() {
		$widgets_manager = \Elementor\Plugin::instance()->widgets_manager;

		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Countdown() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Newsletter() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Counter() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Brands() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Member() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Timeline() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Contact_Form_7() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\FAQs() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Icon_Box() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Icon_Box_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Testimonials() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Image_Grid() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Image_Masonry() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Image_Slides() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Tab_List() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Video() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Quotes() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Slides() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Banner_Medium() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Icon_List() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Icon_List_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Testimonials_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Testimonials_3() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Posts_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Banner_Small() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Images_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Banner_Small_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Banner_Small_3() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Banner_App() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Banner() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Image_Box() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Popular_Tags() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Simple_Text() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Map() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Recently_Viewed_Products() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Recently_Viewed_Products_Grid() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Recently_Viewed_Products_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Recently_Viewed_Products_Carousel_2() );

		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Categories() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Categories_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Categories_3() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Categories_4() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Categories_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Trending_Search_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Trending_Search_Carousel_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Trending_Search_Carousel_3() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Deals_Grid() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Deals_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Product_Deals_Carousel_3() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_With_Category() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_With_Category_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Tab_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Tab_Carousel_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Tab_Carousel_3() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_List_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Carousel() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Carousel_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Grid() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Carousel_With_Category() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Carousel_With_Banner() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Carousel_With_Banner_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Products_Brands() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Quick_Links() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Separator() );

		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Menu_Department_2() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Page_Title() );

		// Headers
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Promotion() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Search_Form() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Login_Register() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Logo() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Menu_Department() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Main_Menu() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Recently_Viewed_Products() );

		if ( defined( 'YITH_WCWL' ) ) {
			$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Wishlist() );
		}

		if ( defined( 'YITH_WOOCOMPARE' ) ) {
			$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Compare() );
		}

		if ( class_exists( 'WOOCS' ) || class_exists( 'Alg_WC_Currency_Switcher' ) ) {
			$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Currencies() );
		}

		if ( class_exists( 'WooCommerce' ) ) {
			$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Headers\Cart() );
		}

		// Footer
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Footer\Contact_Info() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Footer\List_Links() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Footer\Payment() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Footer\Recently_Viewed_Products() );

		// Mobile
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Mobile\Menu_Mobile() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Mobile\Search_Form_Mobile() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Mobile\Cart_Mobile() );
		$widgets_manager->register_widget_type( new \TeckzoneAddons\Elementor\Widgets\Mobile\Go_Back() );
	}

	/**
	 * Add Teckzone category
	 */
	public function add_category( $elements_manager ) {
		$elements_manager->add_category(
			'site_header',
			[
				'title' => esc_html__( 'Teckzone Header', 'teckzone' )
			]
		);

		$elements_manager->add_category(
			'site_footer',
			[
				'title' => esc_html__( 'Teckzone Footer', 'teckzone' )
			]
		);

		$elements_manager->add_category(
			'site_mobile',
			[
				'title' => esc_html__( 'Teckzone Mobile', 'teckzone' )
			]
		);

		$elements_manager->add_category(
			'teckzone',
			[
				'title' => esc_html__( 'Teckzone', 'teckzone' )
			]
		);
	}


	public function elementor_custom_icons( $additional_tabs ) {
		$additional_tabs['linearicons'] = [
			'name'          => 'linearicons',
			'label'         => esc_html__( 'Linearicons', 'teckzone' ),
			'url'           => self::get_asset_url( 'linearicons' ),
			'enqueue'       => [ self::get_asset_url( 'linearicons' ) ],
			'prefix'        => 'icon-',
			'displayPrefix' => 'icon',
			'labelIcon'     => 'icon-pencil4',
			'ver'           => '1.0.0',
			'fetchJson'     => self::get_asset_url( 'linearicons', 'js', false ),
			'native'        => true,
		];

		return $additional_tabs;
	}

	public static function get_asset_url( $filename, $ext_type = 'css', $add_suffix = true ) {

		$url = TECKZONE_ADDONS_URL . 'assets/' . $ext_type . '/icons/' . $filename;

		if ( $add_suffix ) {
			$url .= '.min';
		}

		return $url . '.' . $ext_type;
	}

	/**
	 * Retrieve the list of taxonomy
	 *
	 * @return array Widget categories.
	 */
	public static function get_taxonomy( $taxonomy = 'product_cat' ) {

		$output = array();

		$categories = get_categories(
			array(
				'taxonomy' => $taxonomy
			)
		);

		foreach ( $categories as $category ) {
			$output[ $category->slug ] = $category->name;
		}

		return $output;
	}

	/**
	 * @param array $settings
	 *
	 * @return array Slick Options
	 */
	public static function get_data_slick( $settings = array() ) {
		$carousel_settings = [];

		if ( isset( $settings['navigation'] ) ) {
			$show_dots   = ( in_array( $settings['navigation'], [ 'dots', 'both' ] ) );
			$show_arrows = ( in_array( $settings['navigation'], [ 'arrows', 'both' ] ) );

			$carousel_settings['arrows'] = $show_arrows;
			$carousel_settings['dots']   = $show_dots;
		}

		if ( isset( $settings['autoplay'] ) ) {
			$carousel_settings['autoplay'] = ( 'yes' === $settings['autoplay'] );
		}

		if ( isset( $settings['infinite'] ) ) {
			$carousel_settings['infinite'] = ( 'yes' === $settings['infinite'] );
		}

		if ( isset( $settings['autoplay_speed'] ) ) {
			$carousel_settings['autoplaySpeed'] = absint( $settings['autoplay_speed'] );
		}

		if ( isset( $settings['speed'] ) ) {
			$carousel_settings['speed'] = absint( $settings['speed'] );
		}

		if ( isset( $settings['rows'] ) && $settings['rows'] ) {
			$carousel_settings['rows'] = absint( $settings['rows'] );
		}

		if ( isset( $settings['slidesToShow'] ) && $settings['slidesToShow'] ) {
			$carousel_settings['slidesToShow'] = absint( $settings['slidesToShow'] );
		}

		if ( isset( $settings['slidesToScroll'] ) && $settings['slidesToScroll'] ) {
			$carousel_settings['slidesToScroll'] = absint( $settings['slidesToScroll'] );
		}

		$is_rtl                   = is_rtl();
		$carousel_settings['rtl'] = $is_rtl;

		// Responsive
		if ( isset( $settings['carousel_responsive_settings'] ) && ! empty( $settings['carousel_responsive_settings'] ) ) {
			$responsive_args     = [];
			$responsive_settings = $settings['carousel_responsive_settings'];

			foreach ( $responsive_settings as $setting ) {
				$params = [];

				if ( isset( $setting['responsive_navigation'] ) && $setting['responsive_navigation'] ) {
					$show_dots_res   = ( in_array( $setting['responsive_navigation'], [ 'dots', 'both' ] ) );
					$show_arrows_res = ( in_array( $setting['responsive_navigation'], [ 'arrows', 'both' ] ) );

					$params['arrows'] = $show_arrows_res;
					$params['dots']   = $show_dots_res;
				}

				if ( isset( $setting['responsive_slidesToShow'] ) && $setting['responsive_slidesToShow'] ) {
					$params['slidesToShow'] = absint( $setting['responsive_slidesToShow'] );
				}

				if ( isset( $setting['responsive_slidesToScroll'] ) && $setting['responsive_slidesToScroll'] ) {
					$params['slidesToScroll'] = absint( $setting['responsive_slidesToScroll'] );
				}

				if ( isset( $setting['responsive_rows'] ) && $setting['responsive_rows'] ) {
					$params['rows'] = absint( $setting['responsive_rows'] );
				}

				if ( isset( $setting['responsive_breakpoint'] ) && $setting['responsive_breakpoint'] ) {
					$responsive_args[ $setting['responsive_breakpoint'] ] = [
						'breakpoint' => absint( $setting['responsive_breakpoint'] ),
						'settings'   => $params
					];
				}
			}

			krsort( $responsive_args );

			foreach ( $responsive_args as $value ) {
				$carousel_settings['responsive'][] = $value;
			}
		}

		return $carousel_settings;
	}

	/**
	 * Load products
	 */
	public static function elementor_load_products() {

		$atts = array(
			'columns'      => isset( $_POST['columns'] ) ? intval( $_POST['columns'] ) : '',
			'products'     => isset( $_POST['products'] ) ? $_POST['products'] : '',
			'order'        => isset( $_POST['order'] ) ? $_POST['order'] : '',
			'orderby'      => isset( $_POST['orderby'] ) ? $_POST['orderby'] : '',
			'per_page'     => isset( $_POST['per_page'] ) ? intval( $_POST['per_page'] ) : '',
			'product_cats' => isset( $_POST['product_cats'] ) ? $_POST['product_cats'] : '',
		);

		$products = self::get_products( $atts );

		wp_send_json_success( $products );
	}

	/**
	 * Load products
	 */
	public static function ajax_load_products() {
		check_ajax_referer( 'teckzone_get_products', 'nonce' );

		$settings = $_POST['settings'];

		$atts = [
			'per_page'       => intval( $settings['per_page'] ),
			'columns'        => intval( $settings['columns'] ),
			'product_cats'   => $settings['product_cats'],
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'page'           => isset( $_POST['page'] ) ? $_POST['page'] : 1,
			'load_more'      => isset( $_POST['load_more'] ) ? $_POST['load_more'] : '',
			'load_more_text' => isset( $_POST['text'] ) ? $_POST['text'] : '',
			'products'       => isset( $_POST['type'] ) ? $_POST['type'] : '',
		];

		$products = self::get_products_loop( $atts );

		wp_send_json_success( $products );
	}

	/**
	 * Load products
	 */
	public static function ajax_load_products_list() {
		$atts = array(
			'columns'      => isset( $_POST['columns'] ) ? intval( $_POST['columns'] ) : '',
			'products'     => isset( $_POST['products'] ) ? $_POST['products'] : '',
			'order'        => isset( $_POST['order'] ) ? $_POST['order'] : '',
			'orderby'      => isset( $_POST['orderby'] ) ? $_POST['orderby'] : '',
			'per_page'     => isset( $_POST['per_page'] ) ? intval( $_POST['per_page'] ) : '',
			'product_cats' => isset( $_POST['product_cats'] ) ? $_POST['product_cats'] : '',
		);

		$products = self::get_products_loop( $atts, 'product-list' );

		wp_send_json_success( $products );
	}

	/**
	 * Load products deals
	 */
	public static function ajax_load_products_deal() {
		check_ajax_referer( 'teckzone_get_products_deal', 'nonce' );
		$settings = $_POST['product_deal'];

		$atts = [
			'per_page'       => intval( $settings['per_page'] ),
			'columns'        => intval( $settings['columns'] ),
			'product_cats'   => $settings['product_cats'],
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'page'           => isset( $_POST['page'] ) ? intval( $_POST['page'] ) : 1,
			'pagination'     => isset( $_POST['pagination'] ) ? $_POST['pagination'] : '',
			'load_more_text' => isset( $_POST['text'] ) ? $_POST['text'] : '',
			'product_type'   => isset( $_POST['type'] ) ? $_POST['type'] : 'deals',
		];

		$products = self::get_product_deals( $atts );

		wp_send_json_success( $products );
	}

	/**
	 * Loop over products
	 *
	 * @param array $products_ids
	 */
	public static function get_loop_deals( $products_ids, $template ) {
		update_meta_cache( 'post', $products_ids );
		update_object_term_cache( $products_ids, 'product' );

		$original_post = $GLOBALS['post'];

		woocommerce_product_loop_start();

		foreach ( $products_ids as $product_id ) {
			$GLOBALS['post'] = get_post( $product_id ); // WPCS: override ok.

			setup_postdata( $GLOBALS['post'] );
			wc_get_template_part( 'content', $template );
		}

		$GLOBALS['post'] = $original_post; // WPCS: override ok.
		woocommerce_product_loop_end();

		wp_reset_postdata();
		woocommerce_reset_loop();
	}

	/**
	 * Get the product deals
	 *
	 * @return string.
	 */
	public static function get_product_deals( $settings, $template = 'product',$size = 'full' ) {
		$per_page   = intval( $settings['per_page'] );
		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'posts_per_page'      => $per_page,
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'fields'              => 'ids',
			'post__in'            => (array) wc_get_product_ids_on_sale(),
			'meta_query'          => WC()->query->get_meta_query(),
			'tax_query'           => WC()->query->get_tax_query(),
			'orderby'             => $settings['orderby'],
			'order'               => $settings['order'],
		);

		if ( isset( $settings['pagination'] ) && $settings['pagination'] == 'yes' ) {
			$query_args['paged']         = isset( $settings['page'] ) ? $settings['page'] : 1;
			$query_args['no_found_rows'] = false;
		}

		if ( in_array( $settings['product_type'], array( 'day', 'week', 'month' ) ) ) {
			$date = '+1 day';
			if ( $settings['product_type'] == 'week' ) {
				$date = '+7 day';
			} else if ( $settings['product_type'] == 'month' ) {
				$date = '+1 month';
			}
			$query_args['meta_query'] = apply_filters(
				'teckzone_product_deals_meta_query', array_merge(
					WC()->query->get_meta_query(), array(
						array(
							'key'     => '_deal_quantity',
							'value'   => 0,
							'compare' => '>',
						),
						array(
							'key'     => '_sale_price_dates_to',
							'value'   => 0,
							'compare' => '>',
						),
						array(
							'key'     => '_sale_price_dates_to',
							'value'   => strtotime( $date ),
							'compare' => '<=',
						),
					)
				)
			);
		} elseif ( $settings['product_type'] == 'deals' ) {
			$query_args['meta_query'] = apply_filters(
				'teckzone_product_deals_meta_query', array_merge(
					WC()->query->get_meta_query(), array(
						array(
							'key'     => '_deal_quantity',
							'value'   => 0,
							'compare' => '>',
						)
					)
				)
			);
		}

		if ( $settings['product_cats'] ) {
			$query_args['tax_query'] = array_merge(
				WC()->query->get_tax_query(), array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => explode( ',', $settings['product_cats'] ),
					),
				)
			);
		}

		$deals = new \WP_Query( $query_args );

		if ( ! $deals->have_posts() ) {
			return '';
		}

		global $woocommerce_loop;

		$woocommerce_loop['name'] = 'teckzone_deals_product_elementor';

		$woocommerce_loop['columns'] = isset( $settings['columns'] ) && $settings['columns'] ? intval( $settings['columns'] ) : 1;

		ob_start();

		self::get_loop_deals( $deals->posts, $template );

		$total_page = $deals->max_num_pages;

		$html = '<div class="woocommerce">' . ob_get_clean() . '</div>';

		$params = [
			'per_page'     => $per_page,
			'columns'      => isset( $settings['columns'] ) && $settings['columns'] ? intval( $settings['columns'] ) : 1,
			'product_cats' => $settings['product_cats'],
			'orderby'      => $settings['orderby'],
			'order'        => $settings['order'],
		];

		if ( isset( $settings['page'] ) && $settings['page'] ) {
			$params['page'] = absint( $settings['page'] );
		}

		if ( isset( $settings['pagination'] ) && ( 'yes' == $settings['pagination'] ) && $total_page > 1 ) {
			if ( $query_args['paged'] < $total_page ) {
				$html .= sprintf(
					'<div class="load-more">
						<a href="#" class="ajax-load-products" data-page="%s" data-settings="%s" 
							data-type="%s" data-pagination="%s" data-text="%s" data-nonce="%s" rel="nofollow">
							<span class="button-text">%s</span>
							<span class="teckzone-loading"></span>
						</a>
					</div>',
					esc_attr( $query_args['paged'] + 1 ),
					esc_attr( json_encode( $params ) ),
					esc_attr( $settings['product_type'] ),
					esc_attr( $settings['pagination'] ),
					esc_attr( $settings['load_more_text'] ),
					esc_attr( wp_create_nonce( 'teckzone_get_products_deal' ) ),
					$settings['load_more_text']
				);
			}
		}

		return $html;
	}

	/**
	 * Get pagination numeric
	 *
	 * @return string.
	 */

	public static function pagination_numeric( $max_num_pages, $align_class, $text = [] ) {
		if ( $max_num_pages < 2 ) {
			return;
		}
		?>
        <nav class="navigation paging-navigation numeric-navigation <?php echo esc_attr( $align_class ); ?>">
			<?php
			$big  = 999999999;
			$args = array(
				'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'total'     => $max_num_pages,
				'current'   => max( 1, get_query_var( 'paged' ) ),
				'prev_text' => isset( $text['prev'] ) ? $text['prev'] : esc_html__( 'Previous', 'teckzone' ),
				'next_text' => isset( $text['next'] ) ? $text['next'] : esc_html__( 'Next', 'teckzone' ),
				'type'      => 'plain',
			);

			echo paginate_links( $args );
			?>
        </nav>
		<?php
	}

	/**
	 * Product Loop
	 */
	public static function get_products( $atts ) {
		$params   = '';
		$order    = $atts['order'];
		$order_by = $atts['orderby'];
		if ( $atts['products'] == 'featured' ) {
			$params = 'visibility="featured"';
		} elseif ( $atts['products'] == 'best_selling' ) {
			$params = 'best_selling="true"';
		} elseif ( $atts['products'] == 'sale' ) {
			$params = 'on_sale="true"';
		} elseif ( $atts['products'] == 'recent' ) {
			$order    = $order ? $order : 'desc';
			$order_by = $order_by ? $order_by : 'date';
		} elseif ( $atts['products'] == 'top_rated' ) {
			$params = 'top_rated="true"';
		}

		$params .= ' columns="' . intval( $atts['columns'] ) . '" limit="' . intval( $atts['per_page'] ) . '" order="' . $order . '" orderby ="' . $order_by . '"';
		if ( ! empty( $atts['product_cats'] ) ) {
			$cats = $atts['product_cats'];
			if ( is_array( $cats ) ) {
				$cats = implode( ',', $cats );
			}

			$params .= ' category="' . $cats . '" ';
		}

		if ( ! empty( $atts['product_tags'] ) ) {
			$params .= ' tag="' . implode( ',', $atts['product_tags'] ) . '" ';
		}

		if ( ! empty( $atts['ids'] ) ) {
			$params .= ' ids="' . $atts['ids'] . '" ';
		}

		return do_shortcode( '[products ' . $params . ']' );
	}

	public static function products_loop( $products_ids, $template = 'product' ) {
		update_meta_cache( 'post', $products_ids );
		update_object_term_cache( $products_ids, 'product' );

		$original_post = $GLOBALS['post'];

		woocommerce_product_loop_start();

		foreach ( $products_ids as $product_id ) {
			$GLOBALS['post'] = get_post( $product_id ); // WPCS: override ok.
			setup_postdata( $GLOBALS['post'] );
			wc_get_template_part( 'content', $template );
		}

		$GLOBALS['post'] = $original_post; // WPCS: override ok.
		woocommerce_product_loop_end();

		wp_reset_postdata();
		woocommerce_reset_loop();
	}

	/**
	 * Get the product deals
	 *
	 * @return string.
	 */
	public static function get_products_loop( $settings, $template = 'product' ) {
		$per_page   = intval( $settings['per_page'] );
		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'posts_per_page'      => $per_page,
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'meta_query'          => WC()->query->get_meta_query(),
			'tax_query'           => WC()->query->get_tax_query(),
			'orderby'             => $settings['orderby'],
			'order'               => $settings['order'],
			'fields'              => 'ids',
		);

		if ( isset( $settings['load_more'] ) && $settings['load_more'] == 'yes' ) {
			$query_args['paged']         = isset( $settings['page'] ) ? absint( $settings['page'] ) : 1;
			$query_args['no_found_rows'] = false;
		}

		if ( isset( $settings['product_cats'] ) && $settings['product_cats'] ) {
			$query_args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'slug',
					'terms'    => explode( ',', $settings['product_cats'] ),
				)
			);
		}

		if ( isset( $settings['product_brand'] ) && $settings['product_brand'] ) {
			$query_args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_brand',
					'field'    => 'slug', //This is optional, as it defaults to 'term_id'
					'terms'    => explode( ',', $settings['product_brand'] ),
					'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				),
			);
		}

		$type = '';

		if ( isset( $settings['products'] ) ) {
			$type = $settings['products'];

			switch ( $settings['products'] ) {
				case 'recent':
					$query_args['order']   = $settings['order'] ? $settings['order'] : 'DESC';
					$query_args['orderby'] = $settings['orderby'] ? $settings['orderby'] : 'date';

					unset( $query_args['update_post_meta_cache'] );
					break;

				case 'featured':
					if ( version_compare( WC()->version, '3.0.0', '<' ) ) {
						$query_args['meta_query'][] = array(
							'key'   => '_featured',
							'value' => 'yes',
						);
					} else {
						$query_args['tax_query'][] = array(
							'taxonomy' => 'product_visibility',
							'field'    => 'name',
							'terms'    => 'featured',
							'operator' => 'IN',
						);
					}

					unset( $query_args['update_post_meta_cache'] );
					break;

				case 'sale':
					$query_args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
					break;

				case 'best_selling':
					$query_args['meta_key'] = 'total_sales';
					$query_args['orderby']  = 'meta_value_num';
					$query_args['order']    = $settings['order'] ? $settings['order'] : 'DESC';
					break;

				case 'top_rated':
					$query_args['meta_key'] = '_wc_average_rating';
					$query_args['orderby']  = 'meta_value_num';
					$query_args['order']    = $settings['order'] ? $settings['order'] : 'DESC';
					break;
			}
		}

		$products = new \WP_Query( $query_args );

		if ( ! $products->have_posts() ) {
			return '';
		}

		global $woocommerce_loop;

		$woocommerce_loop['columns'] = isset( $settings['columns'] ) && $settings['columns'] ? intval( $settings['columns'] ) : 1;

		ob_start();

		self::products_loop( $products->posts, $template );

		$output = '<div class="woocommerce">' . ob_get_clean() . '</div>';

		$total_page = $products->max_num_pages;

		$params = array(
			'per_page'     => intval( $settings['per_page'] ),
			'columns'      => isset( $settings['columns'] ) && $settings['columns'] ? intval( $settings['columns'] ) : 1,
			'product_cats' => isset( $settings['product_cats'] ) && $settings['product_cats'] ? $settings['product_cats'] : '',
			'orderby'      => isset( $settings['orderby'] ) && $settings['orderby'] ? $settings['orderby'] : '',
			'order'        => isset( $settings['order'] ) && $settings['order'] ? $settings['order'] : '',
			'page'         => isset( $settings['page'] ) && $settings['page'] ? $settings['page'] : 1,
		);

		if ( isset( $settings['load_more'] ) && ( 'yes' == $settings['load_more'] ) && $total_page > 1 ) {
			if ( $query_args['paged'] < $total_page ) {
				$output .= sprintf(
					'<div class="load-more">
						<a href="#" class="ajax-load-products" data-page="%s" data-type="%s" data-load_more="%s" data-text="%s" data-settings="%s" data-nonce="%s" rel="nofollow">
							<span class="button-text">%s</span>
							<span class="teckzone-loading"></span>
						</a>
					</div>',
					esc_attr( $query_args['paged'] + 1 ),
					esc_attr( $type ),
					esc_attr( $settings['load_more'] ),
					esc_attr( $settings['load_more_text'] ),
					esc_attr( json_encode( $params ) ),
					esc_attr( wp_create_nonce( 'teckzone_get_products' ) ),
					$settings['load_more_text']
				);
			}
		}

		return $output;
	}

	/**
	 * Brands Loop
	 */
	public static function brands_loop( $settings ) {
		$taxonomy = 'product_brand';

		$term_count    = get_terms( $taxonomy, [ 'fields' => 'count' ] );
		$max_num_pages = ceil( $term_count / $settings['number'] );

		if ( get_query_var( 'paged' ) ) {
			$paged = get_query_var( 'paged' );
		} elseif ( get_query_var( 'page' ) ) {
			$paged = get_query_var( 'page' );
		} else {
			$paged = 1;
		}

		$offset = ( ( $paged - 1 ) * $settings['number'] );

		$terms = get_terms(
			array(
				'taxonomy' => $taxonomy,
				'orderby'  => $settings['brand_orderby'],
				'order'    => $settings['brand_order'],
				'number'   => $settings['number'],
				'count'    => true,
				'offset'   => $offset
			)
		);

		if ( is_wp_error( $terms ) && ! $terms ) {
			return;
		}

		$output = [];

		foreach ( $terms as $term ) {
			$settings['product_brand'] = $term->slug;

			$thumbnail_id = absint( get_term_meta( $term->term_id, 'brand_thumbnail_id', true ) );

			$thumbnail = '';
			if ( $thumbnail_id ) {
				$thumbnail = sprintf(
					'<a href="%s" class="brand-logo">%s</a>',
					esc_url( get_term_link( $term->term_id, 'product_brand' ) ),
					wp_get_attachment_image( $thumbnail_id, 'shop_catalog' )
				);
			}

			$count = sprintf( _n( '%s product', '%s products', $term->count, 'teckzone' ), number_format_i18n( $term->count ) );

			$output[] = sprintf(
				'<div class="brand-item-wrapper">
					<div class="brand-item">
						<div class="brand-item__header">
							%s
							<div class="brand-info">
								<a href="%s">%s</a>
								<span>%s</span>
							</div>
						</div>
						<div class="brand-item__content">%s</div>
					</div>
				</div>',
				$thumbnail,
				esc_url( get_term_link( $term->term_id, 'product_brand' ) ),
				esc_html( $term->name ),
				$count,
				self::get_products_loop( $settings )
			);
		}

		$load_more = '';
		if ( $max_num_pages > 1 ) {
			$load_more .= '<div class="load-more text-center">';
			$load_more .= get_next_posts_link( esc_html__( 'Show More', 'teckzone' ), $max_num_pages );
			$load_more .= '</div>';
		}

		return sprintf( '<div class="product-brands">%s</div>%s', implode( '', $output ), $load_more );
	}
}

Elementor::instance();