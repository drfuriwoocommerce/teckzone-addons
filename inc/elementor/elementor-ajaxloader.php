<?php

namespace TeckzoneAddons;

use Elementor\Group_Control_Image_Size;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor_AjaxLoader {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'wc_ajax_tz_elementor_get_elements', [ $this, 'elementor_get_elements' ] );
	}

	public static function elementor_get_elements() {
		$output = '';

		if ( isset( $_POST['params'] ) && ! empty( $_POST['params'] ) ) {
			$params   = json_decode( stripslashes( $_POST['params'] ), true );
			$settings = array();
			foreach ( $params as $key => $value ) {
				$settings[ $key ] = $value;
			}

			$els = '';
			if ( isset( $_POST['element'] ) && ! empty( $_POST['element'] ) ) {
				$els = $_POST['element'];
			}

			if ( $els == 'productsCarousel' ) {
				ob_start();
				self::get_products_carousel( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsCarousel2' ) {
				ob_start();
				self::get_products_carousel_2( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsGrid' ) {
				ob_start();
				self::get_products_grid( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsWithCategory' ) {
				ob_start();
				self::get_products_with_category( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsWithCategory2' ) {
				ob_start();
				self::get_products_with_category_2( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsListCarousel' ) {
				ob_start();
				self::get_products_list_carousel( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsCarouselWithBanner' ) {
				ob_start();
				self::get_products_carousel_with_banner( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsCarouselWithBanner2' ) {
				ob_start();
				self::get_products_carousel_with_banner_2( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsCarouselWithCat' ) {
				ob_start();
				self::get_products_carousel_with_category( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsTabCarousel' ) {
				ob_start();
				self::get_products_tab_carousel( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsTabCarousel2' ) {
				ob_start();
				self::get_products_tab_carousel_2( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsTabCarousel3' ) {
				ob_start();
				self::get_products_tab_carousel_3( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsDealGrid' ) {
				ob_start();
				self::get_products_deal_grid( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsDealCarousel' ) {
				ob_start();
				self::get_products_deal_carousel( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'recentlyViewedProductsCarousel' ) {
				ob_start();
				self::get_recently_viewed_products_carousel( $settings );
				$output = ob_get_clean();
            }
		}

		wp_send_json_success( $output );
		die();
	}

	public static function get_products_carousel( $settings ) {
        $settings['columns'] = intval( $settings['slidesToShow'] );
		$products            = Elementor::get_products( $settings );

		$title_icon = '';

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

        $links = $settings['links_group'];
        ?>
        <div class="products-header">
				<h2><?php echo $title_icon . esc_html( $settings['title'] ); ?></h2>
				<?php if ( $links ) : ?>
					<ul class="extra-links">
						<?php
						foreach ( $links as $index => $item ) {
							$link_key = 'extra_link' . $index;
							echo sprintf( '<li>%s</li>', self::get_link_control( $link_key, $item['link_url'], $item['link_text'], [ 'class' => 'extra-link' ] ) );
						}
						?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="products-content"><?php echo $products; ?></div>
        <?php
	}

	public static function get_products_carousel_2( $settings ) {
        $settings['columns'] = intval( $settings['slidesToShow'] );
		$products            = Elementor::get_products( $settings );

		$title_icon = '';

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$links = $settings['links_group'];
        ?>
        <div class="products-header">
			<h2><?php echo $title_icon . esc_html( $settings['title'] ); ?></h2>
			<?php if ( $links ) : ?>
				<div class="extra-links-wrapper">
					<ul class="extra-links">
						<?php
						foreach ( $links as $index => $item ) {
							$link_key = 'extra_link' . $index;
							echo sprintf( '<li>%s</li>', self::get_link_control( $link_key, $item['link_url'], $item['link_text'], [ 'class' => 'extra-link' ] ) );
						}
						?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
		<div class="products-content"><?php echo $products; ?></div>
        <?php
	}
	
	public static function get_products_grid( $settings ) {
        $title = $link = $title_icon = $icon = '';

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['title'] ) {
			$title = '<h2>' . $title_icon . $settings['title'] . '</h2>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $icon;
			$link = self::get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
		}

		$filter = [ ];
		if ( $settings['show_filter'] == 'yes' && ! empty( $settings['product_cats'] ) ) {
			$filter[] = '<ul class="product-filter">';

			$cats = explode( ',', $settings['product_cats'] );

			if ( empty($cats[0]) ) {
				array_shift($cats);
			}

			if ( $settings['show_all'] == 'yes' ) {
				$filter[] = sprintf( '<li class="" data-filter="%s">%s</li>', esc_attr( implode( ',', $cats ) ), $settings['show_all_text'] );
			} else {
				$settings['product_cats'] = $cats[0];
			}

			foreach ( $cats as $cat ) {
				$cat      = get_term_by( 'slug', $cat, 'product_cat' );
				if ( ! is_wp_error($cat) && $cat ) {
					$filter[] = sprintf( '<li class="" data-filter="%s">%s</li>', esc_attr( $cat->slug ), esc_html( $cat->name ) );
				}
			}

			$filter[] = '</ul>';
		}

		$settings['page'] = 1;
		$products         = Elementor::get_products_loop( $settings );

        ?>
        <div class="product-grid__header">
			<?php echo $title . $link; ?>
		</div>
		<div class="product-grid__content">
			<?php echo implode( '', $filter ); ?>
			<div class="products-wrapper">
				<div class="tz-loader">
					<div class="teckzone-loading"></div>
				</div>
				<?php echo $products; ?>
			</div>
		</div>
		<?php
	}

	public static function get_products_with_category( $settings ) {
        $slides = [
			'slidesToShow',
			'slidesToScroll',
		];

		foreach ( $slides as $slide ) {
			$settings[ $slide ] = 1;
		}

		$icon = '';
		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

        ?>
        <div class="header-cat">
			<?php echo $settings['title'] ? '<h2>' . $icon . $settings['title'] . '</h2>' : '' ?>

			<?php if ( ! empty( $settings['links_group'] ) || $settings['view_all_text'] ) : ?>
				<div class="extra-links-wrapper">
					<ul class="extra-links">
						<?php
						$links = $settings['links_group'];
						if ( $links ) {
							foreach ( $links as $index => $item ) {
								$link_key = 'extra_link' . $index;
								echo sprintf( '<li class="extra-link-item">%s</li>', self::get_link_control( $link_key, $item['link_url'], $item['link_text'], [ 'class' => 'extra-link' ] ) );
							}
						}

						// View All
						$link = '';
						if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
							ob_start();
							\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
							$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
						}

						if ( $settings['view_all_text'] ) {
							$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $icon;
							$link = self::get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
						}

						echo sprintf( '<li>%s</li>', $link );
						?>
					</ul>
				</div>
			<?php endif; ?>

			</div>
			<div class="content-wrapper">
				<div class="images-slider">
					<div class="images-list" data-slick="<?php echo esc_attr( wp_json_encode( Elementor::get_data_slick( $settings ) ) ) ?>">
						<?php
						$banners = $settings['banners'];
						if ( $banners ) {
							foreach ( $banners as $index => $item ) {
								if ( empty( $item['image'] ) ) {
									continue;
								}
								$link_key               = 'banner_link' . $index;
								$settings['image']      = $item['image'];
								$settings['image_size'] = 'full';
								$image_url              = Group_Control_Image_Size::get_attachment_image_html( $settings );
								echo self::get_link_control( $link_key, $item['image_link'], $image_url, [ 'class' => 'image-item' ] );
							}
						}
						?>
					</div>
				</div>
				<div class="products-box">
					<?php echo Elementor::get_products( $settings ); ?>
				</div>
			</div>
		<?php
	}
	
	public static function get_products_with_category_2( $settings ) {
        $icon = $view_all_icon = '';
		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$slides = [ 'slidesToShow', 'slidesToScroll' ];

		foreach ( $slides as $slide ) {
			$settings[$slide] = 1;
		}

        ?>
        <div class="header-cat">
			<h2><?php echo self::get_link_control( 'link', $settings['c_link'], $icon . $settings['title'], [ 'class' => 'cat-title' ] ) ?></h2>
			<?php
			$link_text = '<span class="link-text">' . $settings['view_all_text'] . '</span>';
			echo self::get_link_control( 'footer_link', $settings['view_all_link'], $link_text . $view_all_icon, [ 'class' => 'header-link' ] );
			?>
		</div>
		<div class="content-wrapper">
			<div class="sidebar-box">
					<div class="images-slider">
						<div class="images-list" data-slick="<?php echo esc_attr(wp_json_encode( Elementor::get_data_slick( $settings ) ) ) ?>">
							<?php
							$banners = $settings['banners'];
							if ( $banners ) {
								foreach ( $banners as $index => $item ) {
									if ( empty( $item['image'] ) ) {
										continue;
									}
									$link_key               = 'banner_link' . $index;
									$settings['image']      = $item['image'];
									$settings['image_size'] = 'full';
									$image_url              = Group_Control_Image_Size::get_attachment_image_html( $settings );
									echo self::get_link_control( $link_key, $item['image_link'], $image_url, [ 'class' => 'image-item' ] );
								}
							}
							?>
						</div>
					</div>
				<div class="categories-box categories-box--columns-<?php echo $settings['quicklink_columns']; ?>">
					<?php for ( $i = 1; $i < 3; $i ++ ) : ?>
						<div class="category-box category-box--<?php echo esc_attr( $i ) ?>">
							<?php echo $settings['quicklink_title_' . $i] ? sprintf( '<h4>%s</h4>', self::get_link_control( 'quicklink_' . $i, $settings['quicklink_url_' . $i], $settings['quicklink_title_' . $i] ) ) : ''; ?>
							<ul class="extra-links">
								<?php
								$links = $settings['links_group_' . $i];
								if ( $links ) {
									foreach ( $links as $index => $item ) {
										$link_key = 'extra_link_' . $i . $index;
										echo sprintf( '<li>%s</li>', self::get_link_control( $link_key, $item['link_url_' . $i], $item['link_text_' . $i], [ 'class' => 'extra-link' ] ) );
									}
								}
								?>
							</ul>
						</div>
					<?php endfor; ?>
				</div>
			</div>

			<div class="products-box">
				<?php echo Elementor::get_products( $settings ); ?>
			</div>
		</div>
		<?php
	}

	public static function get_products_list_carousel( $settings ) {
		$settings['columns'] = intval( $settings['slidesToShow'] );
		$products            = Elementor::get_products( $settings );

		$icon = $title = '';
		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( ! empty( $settings['title'] ) ) {
			$title = sprintf( '<h2>%s%s</h2>', $icon, $settings['title'] );
		}
		?>
		<div class="products-header">
			<?php echo $title ?>
			<div class="slick-arrows"></div>
		</div>
		<div class="products-content"><?php echo $products; ?></div>
		<?php
	}

	public static function get_products_carousel_with_banner( $settings ) {
        $title = $title_icon = '';

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['title'] ) {
			$title = '<h2>' . $title_icon . $settings['title'] . '</h2>';
		}

		$settings['columns'] = $settings['slidesToShow'];
        ?>
        <div class="cat-header">
			<?php echo $title; ?>

			<div class="header-links">
				<ul class="extra-links">
					<?php
					$links = $settings['heading_links_group'];
					if ( $links ) {
						foreach ( $links as $index => $item ) {
							$link_key = 'heading_link_' . $index;
							echo sprintf( '<li>%s</li>', self::get_link_control( $link_key, $item['heading_link_url'], $item['heading_link_text'], [ 'class' => 'extra-link' ] ) );
						}
					}
					?>
				</ul>
			</div>
		</div>
		<div class="content-wrapper">
			<div class="products-box tz-elementor-product-carousel">
				<?php echo Elementor::get_products( $settings ); ?>
			</div>		
		</div>
			
		<?php
		$quick_links = isset($settings['quick_links_group']) ? $settings['quick_links_group'] : '';
		if ( $quick_links ) {
			echo '<div class="quick-link-box">';
			echo '<ul class="extra-links">';
			foreach ( $quick_links as $index => $item ) {
				$link_key = 'quick_link_' . $index;
				echo sprintf( '<li>%s</li>', self::get_link_control( $link_key, $item['quick_link_url'], $item['quick_link_text'], [ 'class' => 'extra-link' ] ) );
			}

			echo '</ul>';
			echo '</div>';
		}
	}

	public static function get_products_carousel_with_banner_2( $settings ) {
        $title = $link = $title_icon = $view_all_icon = '';

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['title'] ) {
			$title = '<h2>' . $title_icon . $settings['title'] . '</h2>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $view_all_icon;
			$link = self::get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
		}

		$settings['columns'] = $settings['slidesToShow'];
        ?>
        <div class="cat-header">
			<?php echo $title; ?>
			<?php echo $link; ?>
		</div>
		<div class="content-wrapper">
			<div class="products-box tz-elementor-product-carousel">
				<?php echo Elementor::get_products( $settings ); ?>
			</div>
		</div>
			
		<?php
	}

	public static function get_products_carousel_with_category( $settings ) {
        $title = $title_icon = $link = $icon = '';

		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['title'] ) {
			$title = '<h2>' . $title_icon . $settings['title'] . '</h2>';
		}

		if ( $settings['link_icon'] && ! empty( $settings['link_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['link_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['link_text'] ) {
			$text = '<span class="link-text">' . $settings['link_text'] . '</span>' . $icon;
			$link = self::get_link_control( 'header_link', $settings['link'], $text, [ 'class' => 'header-link' ] );
		}

		$settings['columns'] = $settings['slidesToShow'];

		// Banner Carousel Args
		$carousel_args = [
			'slidesToShow'         			=> 1,
			'slidesToScroll'         		=> 1,
			'navigation'         			=> $settings['banners_navigation'],
			'autoplay'           			=> $settings['banners_autoplay'],
			'infinite'           			=> $settings['banners_infinite'],
			'autoplay_speed'     			=> $settings['banners_autoplay_speed'],
			'speed'              			=> $settings['banners_speed'],
			'carousel_responsive_settings'  => $settings['banners_carousel_responsive_settings'],
		];
        ?>
        <div class="cat-header">
			<?php echo $title . $link; ?>
		</div>
		<div class="categories-box">
			<div class="images-slider">
				<div class="images-list" data-slick="<?php echo esc_attr( wp_json_encode( Elementor::get_data_slick( $carousel_args ) ) ) ?>">
					<?php
					$banners = $settings['banners'];
					if ( $banners ) {
						foreach ( $banners as $index => $item ) {
							if ( empty( $item['image'] ) ) {
								continue;
							}
							$link_key               = 'banner_link' . $index;
							$settings['image']      = $item['image'];
							$settings['image_size'] = 'full';
							$image_url              = Group_Control_Image_Size::get_attachment_image_html( $settings );
							echo self::get_link_control( $link_key, $item['image_link'], $image_url, [ 'class' => 'image-item' ] );
						}
					}
					?>
				</div>
			</div>
			<?php
			// Categories Box
			$source = $settings['cats_source'];

			$term_args = [
				'taxonomy' => 'product_cat',
			];
	
			if ( $source == 'default' ) {
				$display = $settings['cats_display'];
				$parent  = '';
				if ( $display == 'parent' ) {
					$parent = 0;
				}
	
				$term_args['orderby'] = $settings['cats_orderby'];
				$term_args['order']   = $settings['cats_order'];
				$term_args['number']  = $settings['cats_number'];
				$term_args['parent']  = $parent;
	
			} else {
				$cats                 = explode( ',', $settings['cats_selected'] );
				$term_args['slug']    = $cats;
				$term_args['orderby'] = 'slug__in';
			}
	
			$terms = get_terms( $term_args );
	
			$product_cats_classes = [ 'product-cats' ];
	
			$term_count     = count( $terms );
	
			$output = [ ];
	
			if ( ! is_wp_error( $terms ) && $terms ) {
				$output[] = '<ul class="' . esc_attr( implode( ' ', $product_cats_classes ) ) . '">';
	
				$i = $term_count;
				foreach ( $terms as $term ) {
					$thumbnail_id = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );
					$count        = '';
	
					if ( $settings['cats_count'] == 'yes' ) {
						$count = '<span class="cat-count">';
						$count .= sprintf( _n( '%s Item', '%s Items', $term->count, 'teckzone' ), number_format_i18n( $term->count ) );
						$count .= '</span>';
					}
	
					// Image Size
					if ( $settings['image_size'] != 'custom' ) {
						$image_size = $settings['image_size'];
					} else {
						$image_size = [
							$settings['image_custom_dimension']['width'],
							$settings['image_custom_dimension']['height'],
						];
					}
	
					$item_classes = [ 'cat-item' ];
	
					$output[] = sprintf(
						'<li class="%s">
							<a href="%s">
								<span class="cat-text">
									<span class="cat-name">%s</span>
									%s
								</span>
								<span class="cat-thumb">%s</span>
							</a>
						</li>',
						esc_attr( implode( ' ', $item_classes ) ),
						esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
						esc_html( $term->name ),
						$count,
						wp_get_attachment_image( $thumbnail_id, $image_size )
					);
	
					$i --;
				}
	
				$output[] = '</ul>';
			}
	
			echo implode( '', $output );
			?>
		</div>
		<div class="quick-link-box">
			<ul class="extra-links">
				<?php
				$links = $settings['quick_links_group'];
				if ( $links ) {
					foreach ( $links as $index => $item ) {
						$link_key = 'extra_link_' . $index;
						echo sprintf( '<li>%s</li>', self::get_link_control( $link_key, $item['quick_link_url'], $item['quick_link_text'], [ 'class' => 'extra-link' ] ) );
					}
				}
				?>
			</ul>
		</div>
		<div class="products-box" data-settings="<?php echo esc_attr( wp_json_encode( Elementor::get_data_slick( $settings ) ) ); ?>">
			<?php echo Elementor::get_products( $settings ); ?>
		</div>
			
		<?php
	}

	public static function get_products_tab_carousel( $settings ) {
		$icon = '';
		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$output      = [ ];
		$header_tabs = [ ];
		if ( ! empty( $settings['title'] ) ) {
			$header_tabs[] = sprintf( '<h2>%s%s</h2>', $icon, $settings['title'] );
		}

		$header_tabs[] = '<div class="tabs-header-nav">';

		$tab_content = [ ];

		$header_tabs[] = '<ul class="tabs-nav">';
		$i             = 0;
		if ( $settings['product_tabs_source'] == 'special_products' ) {
			$tabs = $settings['special_products_tabs'];

			if ( $tabs ) {
				foreach ( $tabs as $index => $item ) {

					if ( isset( $item['title'] ) ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $item['tab_products'] ), esc_html( $item['title'] ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $item['tab_products'],
						'order'        => ! empty( $item['tab_order'] ) ? $item['tab_order'] : '',
						'orderby'      => ! empty( $item['tab_orderby'] ) ? $item['tab_orderby'] : '',
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $settings['product_cats'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf(
							'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
							esc_attr( $item['tab_products'] ),
							Elementor::get_products_loop( $tab_atts, 'product-list' )
						);
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
								esc_attr( $item['tab_products'] ),
								Elementor::get_products_loop( $tab_atts, 'product-list' )
							);
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="teckzone-loading"></div></div>',
								esc_attr( $item['tab_products'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;
				}
			}
		} else {
			$cats = $settings['product_cats_tabs'];
			if ( $cats ) {
				foreach ( $cats as $tab ) {
					$term = get_term_by( 'slug', $tab['product_cat'], 'product_cat' );
					if ( ! is_wp_error( $term ) && $term ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $tab['product_cat'] ), esc_html( $term->name ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $settings['products'],
						'order'        => $settings['order'],
						'orderby'      => $settings['orderby'],
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $tab['product_cat'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf(
							'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
							esc_attr( $tab['product_cat'] ),
							Elementor::get_products_loop( $tab_atts, 'product-list' )
						);
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
								esc_attr( $tab['product_cat'] ),
								Elementor::get_products_loop( $tab_atts, 'product-list' )
							);
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="teckzone-loading"></div></div>',
								esc_attr( $tab['product_cat'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;

				}
			}
		}

		$header_tabs[] = '</ul>';

		$header_tabs[] = '</div>';

		$output[] = sprintf( '<div class="tabs-header">%s</div>', implode( ' ', $header_tabs ) );
		$output[] = sprintf( '<div class="tabs-content">%s</div>', implode( ' ', $tab_content ) );

		echo implode( '', $output );
	}

	public static function get_products_tab_carousel_2( $settings ) {
		$output      = [ ];
		$header_tabs = [ ];
		if ( ! empty( $settings['title'] ) ) {
			$header_tabs[] = sprintf( '<h2>%s</h2>', $settings['title'] );
		}

		$header_tabs[] = '<div class="tabs-header-nav">';

		$tab_content = [ ];

		$header_tabs[] = '<ul class="tabs-nav">';
		$i             = 0;
		if ( $settings['product_tabs_source'] == 'special_products' ) {
			$tabs = $settings['special_products_tabs'];

			if ( $tabs ) {
				foreach ( $tabs as $index => $item ) {

					if ( isset( $item['title'] ) ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $item['tab_products'] ), esc_html( $item['title'] ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $item['tab_products'],
						'order'        => ! empty( $item['tab_order'] ) ? $item['tab_order'] : '',
						'orderby'      => ! empty( $item['tab_orderby'] ) ? $item['tab_orderby'] : '',
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $settings['product_cats'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf(
							'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
							esc_attr( $item['tab_products'] ),
							Elementor::get_products_loop( $tab_atts, 'product-list' )
						);
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
								esc_attr( $item['tab_products'] ),
								Elementor::get_products_loop( $tab_atts, 'product-list' )
							);
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="teckzone-loading"></div></div>',
								esc_attr( $item['tab_products'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;
				}
			}
		} else {
			$cats = $settings['product_cats_tabs'];
			if ( $cats ) {
				foreach ( $cats as $tab ) {
					$term = get_term_by( 'slug', $tab['product_cat'], 'product_cat' );
					if ( ! is_wp_error( $term ) && $term ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $tab['product_cat'] ), esc_html( $term->name ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $settings['products'],
						'order'        => $settings['order'],
						'orderby'      => $settings['orderby'],
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $tab['product_cat'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf(
							'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
							esc_attr( $tab['product_cat'] ),
							Elementor::get_products_loop( $tab_atts, 'product-list' )
						);
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
								esc_attr( $tab['product_cat'] ),
								Elementor::get_products_loop( $tab_atts, 'product-list' )
							);
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="teckzone-loading"></div></div>',
								esc_attr( $tab['product_cat'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;

				}
			}

		}

		$header_tabs[] = '</ul>';

		$header_tabs[] = '</div>';

		$output[] = sprintf( '<div class="tabs-header">%s</div>', implode( ' ', $header_tabs ) );
		$output[] = sprintf( '<div class="tabs-content">%s</div>', implode( ' ', $tab_content ) );

		echo implode( '', $output );
	}

	public static function get_products_tab_carousel_3( $settings ) {
		$output      = [ ];
		$header_tabs = [ ];
		if ( ! empty( $settings['title'] ) ) {
			$header_tabs[] = sprintf( '<h2>%s</h2>', $settings['title'] );
		}

		$header_tabs[] = '<div class="tabs-header-nav">';

		$tab_content = [ ];

		$header_tabs[] = '<ul class="tabs-nav">';
		$i             = 0;
		if ( $settings['product_tabs_source'] == 'special_products' ) {
			$tabs = $settings['special_products_tabs'];

			if ( $tabs ) {
				foreach ( $tabs as $index => $item ) {

					if ( isset( $item['title'] ) ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $item['tab_products'] ), esc_html( $item['title'] ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $item['tab_products'],
						'order'        => ! empty( $item['tab_order'] ) ? $item['tab_order'] : '',
						'orderby'      => ! empty( $item['tab_orderby'] ) ? $item['tab_orderby'] : '',
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $settings['product_cats'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf( '<div class="tabs-panel tabs-%s tab-loaded">%s</div>', esc_attr( $item['tab_products'] ), Elementor::get_products( $tab_atts ) );
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf( '<div class="tabs-panel tabs-%s tab-loaded">%s</div>', esc_attr( $item['tab_products'] ), Elementor::get_products( $tab_atts ) );
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="teckzone-loading"></div></div>',
								esc_attr( $item['tab_products'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;
				}
			}
		} else {
			$cats = $settings['product_cats_tabs'];
			if ( $cats ) {
				foreach ( $cats as $tab ) {
					$term = get_term_by( 'slug', $tab['product_cat'], 'product_cat' );
					if ( ! is_wp_error( $term ) && $term ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $tab['product_cat'] ), esc_html( $term->name ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $settings['products'],
						'order'        => $settings['order'],
						'orderby'      => $settings['orderby'],
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $tab['product_cat'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf( '<div class="tabs-panel tabs-%s tab-loaded">%s</div>', esc_attr( $tab['product_cat'] ), Elementor::get_products( $tab_atts ) );
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf( '<div class="tabs-panel tabs-%s tab-loaded">%s</div>', esc_attr( $tab['product_cat'] ), Elementor::get_products( $tab_atts ) );
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="teckzone-loading"></div></div>',
								esc_attr( $tab['product_cat'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;

				}
			}
		}

		$header_tabs[] = '</ul>';

		$header_tabs[] = '</div>';

		$output[] = sprintf( '<div class="tabs-header">%s</div>', implode( ' ', $header_tabs ) );
		$output[] = sprintf( '<div class="tabs-content">%s</div>', implode( ' ', $tab_content ) );

		echo implode( '', $output );
	}

	public static function get_products_deal_grid( $settings ) {
        $filter = [ ];
		if ( $settings['show_filter'] == 'yes' && ! empty( $settings['product_cats'] ) ) {
			$filter[] = '<ul class="product-filter">';

			$cats = explode( ',', $settings['product_cats'] );

			if ( empty($cats[0]) ) {
				array_shift($cats);
			}

			if ( $settings['show_all'] == 'yes' ) {
				$filter[] = sprintf( '<li class="" data-filter="%s">%s</li>', esc_attr( implode( ',', $cats ) ), $settings['show_all_text'] );
			} else {
				$settings['product_cats'] = $cats[0];
			}

			foreach ( $cats as $cat ) {
				$cat      = get_term_by( 'slug', $cat, 'product_cat' );
				$filter[] = sprintf( '<li class="" data-filter="%s">%s</li>', esc_attr( $cat->slug ), esc_html( $cat->name ) );
			}

			$filter[] = '</ul>';
		}

		// Cat header
		$icon = $view_all_icon = $link = '';

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon title-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $view_all_icon;
			$link = self::get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
		}

		// Countdown
		$now         = strtotime( current_time( 'Y-m-d H:i:s' ) );
		$expire_date = strtotime( '00:00 +1 day' );
		if ( $settings['product_type'] == 'week' ) {
			$expire_date = strtotime( '00:00 next monday' );
		} elseif ( $settings['product_type'] == 'month' ) {
			$expire_date = strtotime( '00:00 first day of next month' );
		}

		$expire = $expire_date - $now;

		$settings['page'] = 1;

		$product_deals = Elementor::get_product_deals( $settings );

		if ( empty( $product_deals ) ) {
			return;
		}

		$deals = array(
			'days'    => esc_html__( 'days', 'teckzone' ),
			'hours'   => esc_html__( 'hours', 'teckzone' ),
			'minutes' => esc_html__( 'mins', 'teckzone' ),
			'seconds' => esc_html__( 'secs', 'teckzone' ),
		);

        ?>
        <div class="cat-header">
			<div class="header-content">
				<h2 class="cat-title"><?php echo $icon . esc_html( $settings['title'] ); ?></h2>

				<div class="countdown-wrapper">
					<?php if ( $settings['ends_in_position'] == 'outside' ) : ?>
						<span class="ends-text"><?php echo esc_html( $settings['ends_in'] ) ?></span>
					<?php endif; ?>
					<div class="header-countdown">
						<?php if ( $settings['ends_in_position'] == 'inside' ) : ?>
							<span class="ends-text"><?php echo esc_html( $settings['ends_in'] ) ?></span>
						<?php endif; ?>

						<div class="teckzone-countdown" data-expire="<?php echo esc_attr( $expire ); ?>" data-text="<?php echo esc_attr( wp_json_encode( $deals ) ) ?>"></div>
					</div>
				</div>
				<div class="short-desc"><?php echo esc_html( $settings['short_desc'] ) ?></div>
			</div>
			<?php echo $link; ?>
		</div>
		<div class="products-content">
			<?php echo implode( '', $filter ); ?>
			<div class="products-wrapper">
				<div class="tz-loader">
					<div class="teckzone-loading"></div>
				</div>
				<?php echo $product_deals; ?>
			</div>
		</div>
		<?php
	}
	
	public static function get_products_deal_carousel( $settings ) {
        $icon = $view_all_icon = $link = '';

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $view_all_icon;
			$link = self::get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
		}

		$now         = strtotime( current_time( 'Y-m-d H:i:s' ) );
		$expire_date = strtotime( '00:00 +1 day' );
		if ( $settings['product_type'] == 'week' ) {
			$expire_date = strtotime( '00:00 next monday' );
		} elseif ( $settings['product_type'] == 'month' ) {
			$expire_date = strtotime( '00:00 first day of next month' );
		}
		$expire = $expire_date - $now;

		$deals = array(
			'days'    => esc_html__( 'days', 'teckzone' ),
			'hours'   => esc_html__( 'hours', 'teckzone' ),
			'minutes' => esc_html__( 'mins', 'teckzone' ),
			'seconds' => esc_html__( 'secs', 'teckzone' ),
		);

		$settings['columns']    = intval( $settings['slidesToShow'] );
		$settings['pagination'] = '';
		$product_deals          = Elementor::get_product_deals( $settings );

        ?>
        <div class="cat-header">
			<div class="header-content">
				<h2 class="cat-title"><?php echo $icon . esc_html( $settings['title'] ); ?></h2>

				<div class="countdown-wrapper">
					<?php if ( $settings['ends_in_position'] == 'outside' ) : ?>
						<span class="ends-text"><?php echo esc_html( $settings['ends_in'] ) ?></span>
					<?php endif; ?>
					<div class="header-countdown">
						<?php if ( $settings['ends_in_position'] == 'inside' ) : ?>
							<span class="ends-text"><?php echo esc_html( $settings['ends_in'] ) ?></span>
						<?php endif; ?>

						<div class="teckzone-countdown" data-expire="<?php echo esc_attr( $expire ); ?>"
								data-text="<?php echo esc_attr( wp_json_encode( $deals ) ) ?>"></div>
					</div>
				</div>
				<div class="short-desc"><?php echo esc_html( $settings['short_desc'] ) ?></div>
			</div>
			<?php echo $link; ?>
		</div>
		<div class="products-content">
			<?php echo $product_deals; ?>
		</div>
		<?php
	}

	public static function get_recently_viewed_products_carousel ( $settings ) {
		$viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : [ ];
		$viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );

		$empty = false;

		if ( ! $viewed_products ) {
			$empty = true;
		}		

		$output = [ ];

		$per_page   = intval( $settings['per_page'] );
		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'posts_per_page'      => $per_page,
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'fields'              => 'ids',
			'post__in'            => $viewed_products,
			'orderby'             => 'post__in',
		);

		$products = new \WP_Query( $query_args );

		ob_start();

		Elementor::get_loop_deals( $products->posts, 'product' );

		$output[] = ob_get_clean();

		// Footer link
		$link = $icon = '';

		if ( $settings['link_icon'] && ! empty( $settings['link_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['link_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$link_text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $icon;
			$link      = self::get_link_control( 'footer_link', $settings['view_all_link'], $link_text, [ 'class' => 'footer-link' ] );
		}

		if ( $link ) {
			$link = sprintf( '<div class="recently-button-products">%s</div>', $link );
		}

		// Empty content
		$empty_content = sprintf(
			'<div class="recently-empty-products text-center">
				<div class="empty-desc">%s</div>
				%s
			</div>',
			wp_kses( $settings['empty_product_description'], wp_kses_allowed_html( 'post' ) ),
			self::get_link_control( 'empty_button', $settings['empty_product_link'], $settings['empty_product_text'], [ 'class' => 'btn-primary' ] )
		);

		$products_html = sprintf(
			'<div class="recently-viewed-products">
				<div class="recently-has-products">
					%s%s
				</div>
			</div>',
			implode( '', $output ),
			$link
		);

		?>
			<?php if ( ! empty( $settings['title'] ) ) : ?>
				<h3 class="recently-title"><?php echo $settings['title']; ?></h3>
			<?php endif; ?>
			<div class="recently-viewed-content">
				<?php
				if ( $empty == true ) {
					echo $empty_content;
				} else {
					echo $products_html;
				}
				?>
			</div>
		<?php
	}

    /**
	 * Render link control output
	 *
	 * @param       $link_key
	 * @param       $url
	 * @param       $content
	 * @param array $attr
	 *
	 * @return string
	 */
	public static function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$tag = 'a';

		if ( empty( $attr['href'] ) ) {
			$tag = 'span';
		}

        $attributes = [];
        
        foreach( $attr as $name => $v ) {
            $attributes[] = $name . '="' . esc_attr( $v ) . '"';
        }
        
		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, implode( ' ', $attributes ), $content );
	}
}

new Elementor_AjaxLoader();