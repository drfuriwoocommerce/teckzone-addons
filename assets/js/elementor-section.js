(function ($) {
    'use strict';
    
    /*******************************
    ******* Section Sticky *********
    *******************************/
    var sectionSticky = function( $scope ) {
        if ( ! $scope.hasClass( 'tz-sticky-on' ) ) {
            return;
        }

        var data = $scope.data('settings'),
			$wpAdminBar = elementorFrontend.elements.$wpAdminBar;

        var offSet = data.tz_sticky_offset ? data.tz_sticky_offset : 0;
        
        if ($wpAdminBar.length && 'top' === data.tz_sticky && 'fixed' === $wpAdminBar.css('position')) {
            offSet += $wpAdminBar.height();
        }

        var options = {
            getWidthFrom : $('body'),
            responsiveWidth : true,
            topSpacing : offSet
        };

        if ( data.tz_sticky_z_index ) {
            options.zIndex = data.tz_sticky_z_index;
        }

        $scope.sticky(options);

        $scope.on( 'sticky-end', function() {
			$(window).on('resize').trigger('resize');
        });

        window.addEventListener('resize', function (event) {
            $scope.unstick();

            var newOffSet = data.tz_sticky_offset ? data.tz_sticky_offset : 0;

            if ($wpAdminBar.length && 'top' === data.tz_sticky && 'fixed' === $wpAdminBar.css('position')) {
                newOffSet += $wpAdminBar.height();
            }

            options.topSpacing = newOffSet;

            $scope.sticky(options);

            $scope.sticky('update');
		});
    };
    
    var departmentMenuStickyHandler = function( $scope ) {
        var $deparmentMenu = $scope.find( '.teckzone-menu-department' );

        if ( typeof $deparmentMenu === 'undefined' || ! $deparmentMenu.length ) {
            return;
        }

        var dropDown = $deparmentMenu.data( 'menu_dropdown' );

        $scope.on( 'sticky-start', function() {
            if ( dropDown === 'yes' ) {
                $deparmentMenu.removeClass('menu-show');
            }
        });

        $scope.on( 'sticky-end', function() {
            if ( dropDown === 'yes' ) {
                $deparmentMenu.addClass('menu-show');
            }
        });
    };
	
    /**
     * Elementor JS Hooks
     */
    $(window).on("elementor/frontend/init", function () {
        elementorFrontend.hooks.addAction( 'frontend/element_ready/section', sectionSticky );
        elementorFrontend.hooks.addAction( 'frontend/element_ready/section', departmentMenuStickyHandler );
    });
})
(jQuery);