(function ( $ ) {
	'use strict';

	var ControlTZautocomplete = elementor.modules.controls.BaseData.extend( {
		onReady        : function () {

			this.tzAutocomplete( this );

			this.tzRemoveData( this );

			this.tzSortable( this );

			this.tzOnRender( this );
		},
		tzAutocomplete : function ( self ) {
			var $input_value = self.$el.find( '.tz_autocomplete_value' ),
				self_value = $input_value.val(),
				step = '',
				item_value = '';

			self.$el.find( '.tz_autocomplete_param' ).autocomplete( {
				minLength: 2,
				source   : function ( request, response ) {
					$.ajax( {
						url     : ajaxurl,
						dataType: 'json',
						method  : 'post',
						data    : {
							action: 'tz_get_autocomplete_suggest',
							term  : request.term,
							source: self.model.get( 'source' )
						},
						success : function ( data ) {
							response( data.data );
						}
					} )
				},
				response : function ( event, ui ) {
					self.$el.find( '.tz_autocomplete' ).removeClass( 'loading' );
				},
				search   : function ( event, ui ) {
					self.$el.find( '.tz_autocomplete' ).addClass( 'loading' );
				},
				select   : function ( event, ui ) {

					item_value = ui.item.value;

					if ( item_value === 'nothing-found' ) {
						return false;
					}
					self_value = $input_value.val();
					if ( self_value !== '' ) {
						step = ',';
					}

					var template = '<li class="tz_autocomplete-label" data-value="' + item_value + '">' +
						'<span class="tz_autocomplete-data">' + ui.item.label + '</span>' +
						'<a href="#" class="tz_autocomplete-remove">×</a>' +
						'</li>';

					if ( self.model.get( 'multiple' ) ) {
						self.$el.find( '.tz_autocomplete' ).append( template );
						self_value = self_value + step + item_value;
					} else {
						if ( self.$el.find( '.tz_autocomplete .tz_autocomplete-label' ).length > 0 ) {
							self.$el.find( '.tz_autocomplete .tz_autocomplete-label' ).replaceWith( template );
						} else {
							self.$el.find( '.tz_autocomplete' ).append( template );
						}
						self.$el.find( '.tz_autocomplete .tz_autocomplete-label' ).replaceWith( template );
						self_value = item_value;
					}

					self.$el.find( '.tz_autocomplete_param' ).val( '' );

					self.setValue( self_value );

					$input_value.val(self_value);

					return false;
				},
				open     : function ( event ) {
					$( event.target ).data( 'uiAutocomplete' ).menu.activeMenu.addClass( 'elementor-autocomplete-menu tz-autocomplete-menu' );
				}
			} ).autocomplete( 'instance' )._renderItem = function ( ul, item ) {
				return $( '<li>' )
					.attr( 'data-value', item.value )
					.append( item.label )
					.appendTo( ul );
			};
			return self_value;
		},
		tzRemoveData   : function ( self ) {
			var $input_value = self.$el.find( '.tz_autocomplete_value' );
			self.$el.find( '.tz_autocomplete' ).on( 'click', '.tz_autocomplete-remove', function ( e ) {
				e.preventDefault();
				var $this = $( this ),
					self_value = '';

				$this.closest( '.tz_autocomplete-label' ).remove();

				self.$el.find( '.tz_autocomplete' ).find( '.tz_autocomplete-label' ).each( function () {
					self_value = self_value + ',' + $( this ).data( 'value' );
				} );
				$input_value.val(self_value);
				self.setValue( self_value );

			} );
		},
		tzSortable     : function ( self ) {
			var self_value = '';
			if ( self.model.get( 'sortable' ) ) {
				self.$el.find( '.tz_autocomplete' ).sortable( {
					items : 'li.tz_autocomplete-label',
					update: function ( event, ui ) {

						self_value = '';

						self.$el.find( '.tz_autocomplete' ).find( 'li.tz_autocomplete-label' ).each( function () {
							self_value = self_value + ',' + $( this ).data( 'value' );
						} );

						self.setValue( self_value );
					}
				} );
			}
		},
		tzOnRender     : function ( self ) {
			var $input_value = self.$el.find( '.tz_autocomplete_value' ),
				self_value = $input_value.val();

			$.ajax( {
				url     : ajaxurl,
				dataType: 'json',
				method  : 'post',
				data    : {
					action: 'tz_get_autocomplete_render',
					term  : self_value,
					source: self.model.get( 'source' )
				},
				success : function ( data ) {
					if ( data ) {
						self.$el.find( '.tz_autocomplete' ).append( data.data );
						self.$el.find( '.tz_autocomplete' ).find( 'li.tz_autocomplete-loading' ).remove();
					}
				}
			} );
		},
		onBeforeDestroy: function () {
			if ( this.ui.input.data( 'autocomplete' ) ) {
				this.ui.input.autocomplete( 'destroy' );
			}

			this.$el.remove();
		}
	} );
	elementor.addControlView( 'tzautocomplete', ControlTZautocomplete );

})
( jQuery );