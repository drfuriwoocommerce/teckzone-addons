(function ($) {
	'use strict';

	/**
	 * Product Deals Carousel 2
	 */
	var productDealsCarousel2Handler = function ($scope, $) {
		$scope.find('.tz-product-deals-carousel-2').each(function () {
			var $selector = $(this),
				$gallery = $selector.find('.woocommerce-product-gallery');

			var options = {
				selector: '.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image',
				allowOneSlide: false,
				animation: "slide",
				animationLoop: false,
				animationSpeed: 500,
				controlNav: "thumbnails",
				directionNav: false,
				rtl: false,
				slideshow: false,
				smoothHeight: true,
				start: function () {
					$gallery.css('opacity', 1);
				}
			};

			$gallery.flexslider(options);

			$selector.find('.deal-expire-countdown').each(function () {
				$(document).trigger('deal_expire_countdown', $(this));
			});

			$gallery.each(function () {
				var $el = $(this);
				$el.imagesLoaded(function () {

					var $thumbnail = $el.find('.flex-control-thumbs');

					setTimeout(function () {
						if ($thumbnail.length < 1) {
							return;
						}
						var columns = $el.data('columns');
						var count = $thumbnail.find('li').length;
						if (count > columns) {
							$thumbnail.not('.slick-initialized').slick({
								slidesToShow: columns,
								slidesToScroll: 1,
								focusOnSelect: true,
								infinite: false,
								arrows: true,
								prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
								nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>',
								responsive: [
									{
										breakpoint: 768,
										settings: {
											slidesToShow: 4
										}
									},
									{
										breakpoint: 480,
										settings: {
											slidesToShow: 3
										}
									}
								]
							});
						} else {
							$thumbnail.addClass('no-slick');
						}
					}, 100);

				});
			});

			$selector.find('.woocommerce-product-gallery__image').on('click', function () {
				return false;
			});
		});
	};

	/**
	 * Product Deals Carousel 3
	 */
	var productDealsCarousel3Handler = function ($scope, $) {
		$scope.find('.tz-product-deals-carousel-3').each(function () {
			var $selector = $(this),
				$gallery = $selector.find('.woocommerce-product-gallery');

			var options = {
				selector: '.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image',
				allowOneSlide: false,
				animation: "slide",
				animationLoop: false,
				animationSpeed: 500,
				controlNav: "thumbnails",
				directionNav: false,
				rtl: false,
				slideshow: false,
				smoothHeight: true,
				start: function () {
					$gallery.css('opacity', 1);
				}
			};

			$gallery.flexslider(options);

			$selector.find('.deal-expire-countdown').each(function () {
				$(document).trigger('deal_expire_countdown', $(this));
			});

			$gallery.each(function () {
				var $el = $(this);
				$el.imagesLoaded(function () {

					var $thumbnail = $el.find('.flex-control-thumbs');

					setTimeout(function () {
						if ($thumbnail.length < 1) {
							return;
						}
						var columns = $el.data('columns');
						var count = $thumbnail.find('li').length;
						if (count > columns) {
							$thumbnail.not('.slick-initialized').slick({
								slidesToShow: columns,
								slidesToScroll: 1,
								focusOnSelect: true,
								infinite: false,
								arrows: true,
								prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
								nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>',
								responsive: [
									{
										breakpoint: 768,
										settings: {
											slidesToShow: 4
										}
									},
									{
										breakpoint: 480,
										settings: {
											slidesToShow: 3
										}
									}
								]
							});
						} else {
							$thumbnail.addClass('no-slick');
						}
					}, 100);

				});
			});

			$selector.find('.woocommerce-product-gallery__image').on('click', function () {
				return false;
			});
		});
	};

	var productTabsActive = function ($selector) {
		var $el     = $selector.find( '.tabs-nav a' ),
			$panels = $selector.find( '.tabs-panel' );

			$el.filter( ':first' ).addClass( 'active' );
			$selector.find( '.tabs-nav' ).find( 'li' ).filter( ':first' ).addClass( 'active' );
			$panels.filter( ':first' ).addClass( 'active' );

		$selector.find('.tabs-nav').on('click', 'a', function (e) {
			e.preventDefault();
			var $this = $(this),
				currentTab = $this.data('href');

			if ($this.hasClass('active')) {
				return;
			}

			$selector.find('.tabs-nav a').removeClass('active');
			$this.addClass('active');
			$selector.find('.tabs-panel').removeClass('active');
			$selector.find('.tabs-' + currentTab).addClass('active');

			$selector.find('.slick-slider').slick('setPosition');
		});
	};

	/**
	 * CountDown
	 */
	var countDownHandler = function ($scope, $) {
		$scope.find('.teckzone-countdown').mf_countdown();
	};

	/**
	 * Counter
	 * @param $scope
	 * @param $
	 */
	var counterHandler = function ($scope, $) {
		$scope.find('.teckzone-counter').each(function () {
			var $selector = $(this);
			$selector.find('.value').counterUp({
				delay: 20,
				time: 800
			});

		});
	};

	/**
	 * Masonry
	 * @param $scope
	 * @param $
	 */
	var imageMasonry = function ($scope, $) {
		$scope.find('.teckzone-image-masonry').each(function () {
			var $selector = $(this),
				columns = $selector.data('columns');
			$selector.imagesLoaded(function () {
				$selector.find('.gallery-wrapper').masonryGrid({
					'columns': columns,
				});
			});
		});
	};

	/**
	 * Carousel
	 * @param $scope
	 * @param $
	 */
	var imageCarousel = function ($scope, $) {
		$scope.find('.teckzone-image-slides').each(function () {
			var $selector = $(this),
				options = {
					arrows: false,
					dots: true,
					prevArrow: '<span class="slick-prev-arrow"><i class="icon-chevron-left"></i></span>',
					nextArrow: '<span class="slick-next-arrow"><i class="icon-chevron-right"></i></span>'
				};
			$selector.find('.gallery-wrapper').not('.slick-initialized').slick(options);
		})
	};

	/**
	 * Timeline
	 * @param $scope
	 * @param $
	 */
	var timeline = function ($scope, $) {
		$scope.find('.teckzone-timeline').each(function () {
			var $selector = $(this),
				options = {
					prevArrow: '',
					nextArrow: '',
				};

			$selector.find('ul.timeline__date').not('.slick-initialized').slick(options);
		})
	};

	/**
	 * Testimonials
	 * @param $scope
	 * @param $
	 */
	var testimonial = function ($scope, $) {
		$scope.find('.teckzone-testimonials').each(function () {
			var $selector = $(this),
				options = {
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
				};
			$selector.find('.list-testimonials').not('.slick-initialized').slick(options);
		})
	};

	/**
	 * Testimonials
	 * @param $scope
	 * @param $
	 */
	var testimonial2 = function ($scope, $) {
		$scope.find('.teckzone-testimonials-2, .teckzone-testimonials-3').each(function () {
			var $selector = $(this),
				$arrow_wrapper = $(this).find('.slick-nav__arrows'),
				$dot_wrapper = $(this).find('.slick-nav__dots'),
				options = {
					appendArrows: $arrow_wrapper,
					appendDots: $dot_wrapper,
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
				};
			$selector.find('.list-testimonials').not('.slick-initialized').slick(options);
		})
	};

	/**
	 * Tab
	 * @param $scope
	 * @param $
	 */
	var tab = function ($scope, $) {
		$scope.find('.teckzone-tab-list').each(function () {
			var $selector = $(this),
				tabMnu = $selector.find('.tab__nav .box-nav'),
				tabContent = $selector.find('.tab__content .box-content');

			$selector.find('.tab__nav .box-nav:nth-child(' + 1 + ')').addClass('active');
			$selector.find('.tab__content .box-content:nth-child(' + 1 + ')').addClass('active');

			tabMnu.each(function (i) {
				$(this).attr('data-tab', 'tab' + i);
			});
			tabContent.each(function (i) {
				$(this).attr('data-tab', 'tab' + i);
			});

			tabMnu.click(function () {
				var tabData = $(this).data('tab');
				$selector.find(tabContent).removeClass('active');
				$selector.find(tabContent).filter('[data-tab=' + tabData + ']').addClass('active');
			});

			$selector.find('.tab__nav > .box-nav').click(function () {
				var before = $('.tab__nav .box-nav.active');
				before.removeClass('active');
				$(this).addClass('active');
			});
		})
	};

	var videoLightBox = function ($scope, $) {
		$scope.find('.teckzone-video').each(function () {

			var $selector = $(this).find('.play-banner');
			$selector.magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 300,
				preloader: false,
				fixedContentPos: false
			});
		})
	};

	var slideCarousel = function ($scope, $) {
		$scope.find('.teckzone-slides-wrapper').each(function () {
			var $selector = $(this).find('.teckzone-slides'),
				elementSettings = $selector.data('slider_options'),
				$arrow_wrapper = $(this).find('.arrows-inner'),
				slidesToShow = parseInt(elementSettings.slidesToShow);

			$selector.not('.slick-initialized').slick({
				rtl: $('body').hasClass('rtl'),
				slidesToShow: slidesToShow,
				arrows: elementSettings.arrows,
				appendArrows: $arrow_wrapper,
				dots: elementSettings.dots,
				infinite: elementSettings.infinite,
				prevArrow: '<span class="slick-prev-arrow"><i class="icon-chevron-left"></i></span>',
				nextArrow: '<span class="slick-next-arrow"><i class="icon-chevron-right"></i></span>',
				autoplay: elementSettings.autoplay,
				autoplaySpeed: parseInt(elementSettings.autoplaySpeed),
				speed: parseInt(elementSettings.speed),
				fade: elementSettings.fade,
				pauseOnHover: elementSettings.pauseOnHover,
				responsive: []
			});

			$selector.imagesLoaded(function () {
				$selector.closest('.teckzone-slides-wrapper').removeClass('loading');
			});

			var animation = $selector.data('animation');

			if (animation) {
				$selector
					.on('beforeChange', function () {
						var $sliderContent = $selector.find('.teckzone-slide-content'),
							$sliderPriceBox = $selector.find('.teckzone-slide-price-box');

						$sliderContent.removeClass('animated' + ' ' + animation).hide();

						$sliderPriceBox.removeClass('animated zoomIn').hide();
					})
					.on('afterChange', function (event, slick, currentSlide) {
						var $currentSlide = $(slick.$slides.get(currentSlide)).find('.teckzone-slide-content'),
							$currentPriceBox = $(slick.$slides.get(currentSlide)).find('.teckzone-slide-price-box');

						$currentSlide.show().addClass('animated' + ' ' + animation);

						$currentPriceBox.show().addClass('animated zoomIn');
					});
			}
		});
	};

	/**
	 * Testimonials
	 * @param $scope
	 * @param $
	 */
	var postsCarousel = function ($scope, $) {
		$scope.find('.teckzone-posts-carousel').each(function () {
			var $selector = $(this),

				options = {
					arrows: true,
					dots: false,
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>',
					appendArrows: $selector.find('.box-nav')
				};

			$selector.find('.post-list').not('.slick-initialized').slick(options);
		})
	};

	var imagesCarousel = function ($scope, $) {
		$scope.find('.teckzone-images-carousel--slide').each(function () {
			var $selector = $(this),
				options = {
					arrows: true,
					dots: false,
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>',
					appendArrows: $selector.find('.slick-arrows')
				};

			$selector.find('.images-list').not('.slick-initialized').slick(options);
		});
	};

	var trendingSearchCarousel = function ($scope, $) {
		$scope.find('.teckzone-elementor-trending-search-carousel').each(function () {
			var $selector = $(this),
				options = {
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
				};

			if ($selector.hasClass('teckzone-trending-search-carousel-2') || $selector.hasClass('teckzone-trending-search-carousel-3')) {
				options.appendArrows = $selector.find('.slick-arrows');
			}

			$selector.find('.collection-list').not('.slick-initialized').slick(options);
		});
	};

	var promotionHandler = function ($scope, $) {
		$scope.find('.teckzone-promotion').each(function () {
			var $selector = $(this);

			$selector.on('click', '.pro-close', function (e) {
				e.preventDefault();

				$(this).closest('.teckzone-promotion').slideUp().addClass('invisible');
			});

		});
	};

	var searchTogglePanel = function ($scope, $) {
		$scope.find('.tz-search-form--mobile').each(function () {
			var $selector = $(this),
				$searchMobile = $('.tz-search-form--mobile').not($selector);

			$selector.on('click', '.open-search-panel', function (e) {
				e.preventDefault();
				var $this = $(this);

				$searchMobile.removeClass('display-search-panel');
				$searchMobile.find('.open-search-panel').removeClass('active');
				$searchMobile.find('.search-panel-content').removeClass('open');

				$this.toggleClass('active');
				$this.siblings('.search-panel-content').toggleClass('open');
				$selector.addClass('display-search-panel');

				$(document.body)
				.toggleClass('open-search-mobile')
				.removeClass('open-cart-mobile open-menu-mobile')
				.trigger('teckzone_toggle_search_mobile');

				if ( ! $this.siblings('.search-panel-content').hasClass('open') ) {
					$(document.body).removeClass('open-search-mobile');
				}
			});

			$selector.on('click', '.close-search-panel', function (e) {
				e.preventDefault();

				var $this = $(this);
				$this.closest('.search-panel-content').removeClass('open');
				$this.closest('.search-panel-content').siblings('.open-search-panel').removeClass('active');
				$selector.removeClass('display-search-panel');
				$(document.body).removeClass('open-search-mobile');
			});

			$selector.find('.teckzone-off-canvas-layer').on('click', function () {
				var $this = $(this);

				$selector.removeClass('display-search-panel');
				$this.siblings('.search-panel-content').removeClass('open');
				$this.siblings('.open-search-panel').removeClass('active');
				$(document.body).removeClass('open-search-mobile');
			});

			$(document).on('teckzone_toggle_menu_mobile teckzone_toggle_mini_cart_mobile', function(){
				$selector.find('.open-search-panel').removeClass('active');
				$selector.find('.search-panel-content').removeClass('open');
			});
		});
	};

	var searchFormHandler = function ($scope, $) {
		$scope.find('.tz-search-form').each(function () {
			var $selector = $(this),
				elementSettings = $selector.data('settings');
			$selector.on('change', '.product-cat-dd', function () {
				var value = $(this).find('option:selected').text().trim();
				$selector.find('.product-cat-label').html(value);
			});

			$selector.find('.products-search').submit(function () {
				if ($(this).find('.product-cat-dd').val() == '0') {
					$(this).find('.product-cat-dd').removeAttr('name');
				}
			});

			if (elementSettings.ajax_search === 'no') {
				return;
			}

			var xhr = null,
				$form = $selector.closest('form.form-search'),
				searchCache = {};

			$selector.on('keyup', '.search-field', function (e) {
				var valid = false;

				if (typeof e.which == 'undefined') {
					valid = true;
				} else if (typeof e.which == 'number' && e.which > 0) {
					valid = !e.ctrlKey && !e.metaKey && !e.altKey;
				}

				if (!valid) {
					return;
				}

				if (xhr) {
					xhr.abort();
				}

				var $currentForm = $(this).closest('.form-search'),
					$topContent = $(this).closest('.top-content'),
					$search = $currentForm.find('input.search-field');

				if ($search.val().length < 2) {
					$currentForm.removeClass('searching searched actived found-products found-no-product invalid-length');
					$topContent.removeClass('searching searched actived found-products found-no-product invalid-length');
					$(document.body).removeClass('tz-search-mobile-success');
				}

				search($currentForm);
			}).on('change', '.product-cat-dd', function () {
				if (xhr) {
					xhr.abort();
				}

				var $currentForm = $(this).closest('.form-search');

				search($currentForm);
			}).on('focusout', '.search-field', function () {
				var $currentForm = $(this).closest('.form-search'),
					$topContent = $(this).closest('.top-content'),
					$search = $currentForm.find('input.search-field');

				if ($search.val().length < 2) {
					$currentForm.removeClass('searching searched actived found-products found-no-product invalid-length');
					$topContent.removeClass('searching searched actived found-products found-no-product invalid-length');
					$(document.body).removeClass('tz-search-mobile-success');
				}
			});

			$selector.on('click', '.close-search-results', function (e) {
				e.preventDefault();
				$selector.find('.search-field').val('');
				$selector.find('.form-search').removeClass('searching searched actived found-products found-no-product invalid-length');
				$(document.body).removeClass('tz-search-mobile-success');
			});

			/**
			 * Private function for search
			 */
			function search($currentForm) {
				var $search = $currentForm.find('input.search-field'),
					$topContent = $currentForm.closest('.top-content'),
					keyword = $search.val(),
					cat = 0,
					$results = $currentForm.find('.search-results');

				if ($selector.hasClass('tz-search-form--mobile')) {
					$results = $selector.find('.top-content .search-results');
				}

				if ($currentForm.find('.product-cat-dd').length > 0) {
					cat = $currentForm.find('.product-cat-dd').val();
				}


				if (keyword.trim().length < 2) {
					$currentForm.removeClass('searching found-products found-no-product').addClass('invalid-length');
					$topContent.removeClass('searching found-products found-no-product').addClass('invalid-length');
					return;
				}

				$currentForm.removeClass('found-products found-no-product').addClass('searching');
				$topContent.removeClass('found-products found-no-product').addClass('searching');

				var keycat = keyword + cat;

				if (keycat in searchCache) {
					var result = searchCache[keycat];

					$currentForm.removeClass('searching');
					$topContent.removeClass('searching');

					$currentForm.addClass('found-products');
					$topContent.addClass('found-products');

					$results.html(result.products);

					$(document.body).trigger('teckzone_ajax_search_request_success', [$results]);

					$currentForm.removeClass('invalid-length');
					$topContent.removeClass('invalid-length');

					$currentForm.addClass('searched actived');
					$topContent.addClass('searched actived');
				} else {
					var data = {
							'term': keyword,
							'cat': cat,
							'ajax_search_number': elementSettings.ajax_search_number,
							'search_type': elementSettings.search_for
						},
						ajax_url = teckzoneData.ajax_url.toString().replace('%%endpoint%%', 'teckzone_instance_search_form');

					xhr = $.post(
						ajax_url,
						data,
						function (response) {
							var $products = response.data;

							$currentForm.removeClass('searching');
							$topContent.removeClass('searching');


							$currentForm.addClass('found-products');
							$topContent.addClass('found-products');

							$results.html($products);

							$currentForm.removeClass('invalid-length');
							$topContent.removeClass('invalid-length');

							$(document.body).trigger('teckzone_ajax_search_request_success', [$results]);

							// Cache
							searchCache[keycat] = {
								found: true,
								products: $products
							};

							$currentForm.addClass('searched actived');
							$topContent.addClass('searched actived');

							$(document.body).addClass('tz-search-mobile-success');
						}
					);
				}
			}
		});
	};

	var recentViewedProductsCarousel = function ($scope, $) {
		$scope.find('.tz-product-recently-viewed-carousel').each(function () {
			var $el = $(this);

			if ($el.hasClass('tz-empty-product')) {
				return;
			}

			getProductCarousel($el);
		});
	};

	var recentlyViewedProducts = function ($scope, $) {
		$scope.find('.teckzone-header-recently-viewed').each(function () {
			var $el = $(this), found = true;

			$el.on('hover', function () {
                if (found) {
					loadAjaxRecently($el);
                    found = false;
                }
            });
		});
		$scope.find('.teckzone-content-recently-viewed').each(function () {
			var $el = $(this);

			$(this).addClass('loaded');
			loadAjaxRecently($el);
		});

		$scope.find('.teckzone-footer-recently-viewed').each(function () {
			var $el = $(this),
				found = true,
				$content = $el.find('.recently-viewed-inner'),
				$layer = $el.find('.overlay');

			$el.find('.recently-title').on('click', function (e) {
				e.preventDefault();

				$layer.toggleClass('opened');

				$content.slideToggle(400, function () {
					if (found) {
						loadAjaxRecently($el);
						found = false;
					}
				});

				$(this).toggleClass('active');
			});

			$layer.on('click', function () {
				$(this).removeClass('opened');
				$content.slideUp(400);
				$el.find('.recently-title').removeClass('active');
			});
		});

		$scope.find('.tz-product-recently-viewed-carousel').each(function () {
			var $el = $(this),
				$checkProduct = $el.find('.recently-has-products').length;

			if ($checkProduct < 0) {
				$el.find('.recently-viewed-products').remove();
			} else {
				$el.find('.recently-empty-products').remove();
			}

			getProductCarousel($el);
		});
	};

	var recentlyViewedProductsAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-recently-viewed-carousel--default');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} 

		function productCarouselInfinite($el) {

			$(window).on('scroll', function () {

				if ($el.hasClass('no-infinite')) {
					return;						
				}				

				var offSet = 0;

				if ($el.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($el);

					$el.addClass('no-infinite');						
				}

			}).trigger('scroll');
		}

		function getProductAjax($el) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}			

			var dataSettings = $el.find('.tz-product-recently-viewed-carousel-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'recentlyViewedProductsCarousel'
				},
				success: function (response) {

					$el.html(response.data);
					getProductCarousel($el);
				}
			});

			
		}

	}

	function loadAjaxRecently($selector) {
		var $recently = $selector.find('.recently-has-products'),
			elementSettings = $selector.data('number');

		var data = {
				numbers: elementSettings.numbers,
				nonce: teckzoneData.nonce
			},
			ajax_url = teckzoneData.ajax_url.toString().replace('%%endpoint%%', 'teckzone_header_recently_viewed');

		$.post(
			ajax_url,
			data,
			function (response) {
				var $data = $(response.data);

				$recently.html($data);

				if ($recently.find('.product-list').hasClass('no-products')) {
					$selector.find('.recently-viewed-products').remove();
				} else {
					$selector.find('.recently-empty-products').remove();
				}

				getProductCarousel($selector);
				$selector
					.addClass('products-loaded')
					.find('.teckzone-loading--wrapper').remove();
			}
		);
	}

	var productsCarousel = function ($scope, $) {
		$scope.find('.tz-elementor-product-carousel').each(function () {
			var $el = $(this),
				extendOptions = {};

			if ($el.hasClass('tz-product-deals-carousel-2') ||
				$el.hasClass('tz-product-deals-carousel-3')
			) {
				extendOptions.appendArrows = $el.find('.slick-arrows');
			}

			getProductCarousel($el, extendOptions);
		});
	};

	var productsCategoriesCarousel = function ($scope, $) {
		$scope.find('.teckzone-product-categories-carousel').each(function () {
			var $el = $(this),
				sliderSelector = $el.find('ul.product-cats');

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			sliderSelector.not('.slick-initialized').slick(options);
		});
	};

	/**
	 * Get Product AJAX
	 */
	var getProductsAJAXHandler = function ($el, $tabs) {
		var tab = $el.data('href'),
			$content = $tabs.find('.tabs-' + tab);

		if ($content.hasClass('tab-loaded')) {
			return;
		}

		var data = {},
			elementSettings = $content.data('settings'),
			ajax_url = teckzoneData.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_load_products');

		$.each(elementSettings, function (key, value) {
			data[key] = value;
		});

		$.post(
			ajax_url,
			data,
			function (response) {
				if (!response) {
					return;
				}

				$content.html(response.data);

				getProductCarousel($content.closest('.tz-elementor-product-carousel'));

				$content.addClass('tab-loaded');
			}
		);
	};

	/**
	 * Get Products List AJAX
	 */
	var getProductsListAJAXHandler = function ($el, $tabs) {
		var tab = $el.data('href'),
			$content = $tabs.find('.tabs-' + tab);

		if ($content.hasClass('tab-loaded')) {
			return;
		}

		var data = {},
			elementSettings = $content.data('settings'),
			ajax_url = teckzoneData.ajax_url.toString().replace('%%endpoint%%', 'teckzone_ajax_load_products_list');

		$.each(elementSettings, function (key, value) {
			data[key] = value;
		});

		$.post(
			ajax_url,
			data,
			function (response) {
				if (!response) {
					return;
				}

				$content.html(response.data);

				getProductCarousel($content.closest('.tz-elementor-product-carousel'));

				$content.addClass('tab-loaded');
			}
		);
	};

	/**
	 * Load Products
	 */
	var loadProductsGrid = function ($selector) {
		$selector.each(function () {
			var $this = $(this),
				ajax_url = teckzoneData.ajax_url.toString().replace('%%endpoint%%', 'teckzone_ajax_load_products');

			// Load Products
			$this.on('click', 'a.ajax-load-products', function (e) {
				e.preventDefault();

				var $el = $(this);

				if ($el.hasClass('loading')) {
					return;
				}

				$el.addClass('loading');

				$.post(
					ajax_url,
					{
						page: $el.data('page'),
						settings: $el.data('settings'),
						type: $el.data('type'),
						load_more: $el.data('load_more'),
						text: $el.data('text'),
						nonce: $el.data('nonce')
					},
					function (response) {
						if (!response) {
							return;
						}

						$el.removeClass('loading');

						var $data = $(response.data),
							$products = $data.find('ul.products > li'),
							$button = $data.find('.ajax-load-products'),
							$container = $el.closest('.tz-products-grid'),
							$grid = $container.find('ul.products');

						// If has products
						if ($products.length) {
							// Add classes before append products to grid
							$products.addClass('product');

							for (var index = 0; index < $products.length; index++) {
								$($products[index]).css('animation-delay', index * 100 + 100 + 'ms');
							}

							$products.addClass('teckzoneFadeInUp teckzoneAnimation');
							$grid.append($products);

							if ($button.length) {
								$el.replaceWith($button);
							} else {
								$el.slideUp();
								$el.closest('.load-more').addClass('loaded');
							}
						}
					}
				);
			});

			// AJAX Filter
			$this.find('ul.product-filter li:first').addClass('active');
			$this.on('click', 'ul.product-filter li', function (e) {
				e.preventDefault();

				var $el = $(this);

				if ($el.hasClass('loading')) {
					return;
				}

				$el.addClass('active').siblings('.active').removeClass('active');

				var filter = $el.attr('data-filter'),
					$wrapper = $this.find('.products-wrapper');

				var data = {
					nonce: $this.data('nonce'),
					text: $this.data('text'),
					type: $this.data('type')
				};

				data.settings = $this.data('settings');
				data.settings.product_cats = filter;

				data.load_more = $this.data('load_more');

				$this.addClass('loading');

				$.post(
					ajax_url,
					data,
					function (response) {
						if (!response) {
							return;
						}

						var $data = $(response.data),
							$products = $data.find('ul.products > li'),
							$button = $data.find('.ajax-load-products');

						$this.removeClass('loading');

						// If has products
						if ($products.length) {
							// Add classes before append products to grid
							$products.addClass('product');

							for (var index = 0; index < $products.length; index++) {
								$($products[index]).css('animation-delay', index * 100 + 100 + 'ms');
							}

							$products.addClass('teckzoneFadeInUp teckzoneAnimation');

							$wrapper.children('div.woocommerce, .load-more').remove();
							$button.css('animation-delay', $products.length * 100 + 100 + 'ms').addClass('teckzoneFadeIn teckzoneAnimation');
							$wrapper.append($data);
						}
					}
				);
			});
		});
	};

	/**
	 * Load Products
	 */
	var loadProductsDeals = function ($seleclor) {
		$seleclor.each(function () {
			var $this = $(this),
				ajax_url = teckzoneData.ajax_url.toString().replace('%%endpoint%%', 'teckzone_ajax_load_products_deal');

			// Load products
			$this.on('click', 'a.ajax-load-products', function (e) {
				e.preventDefault();

				var $el = $(this);

				if ($el.hasClass('loading')) {
					return;
				}

				$el.addClass('loading');

				var data = {
					page: $el.data('page'),
					product_deal: $el.data('settings'),
					type: $el.data('type'),
					pagination: $el.data('pagination'),
					text: $el.data('text'),
					nonce: $el.data('nonce')
				};

				$.post(
					ajax_url,
					data,
					function (response) {
						if (!response) {
							return;
						}

						$el.removeClass('loading');

						var $data = $(response.data),
							$products = $data.find('ul.products > li'),
							$button = $data.find('.ajax-load-products'),
							$container = $el.closest('.tz-product-deals-grid'),
							$grid = $container.find('ul.products');

						// If has products
						if ($products.length) {
							// Add classes before append products to grid
							$products.addClass('product');

							for (var index = 0; index < $products.length; index++) {
								$($products[index]).css('animation-delay', index * 100 + 100 + 'ms');
							}

							$products.addClass('teckzoneFadeInUp teckzoneAnimation');
							$grid.append($products);

							if ($button.length) {
								$el.replaceWith($button);
							} else {
								$el.slideUp();
								$el.closest('.load-more').addClass('loaded');
							}
						}
					}
				);
			});

			// AJAX Filter
			$this.find('ul.product-filter li:first').addClass('active');
			$this.on('click', 'ul.product-filter li', function (e) {
				e.preventDefault();

				var $el = $(this);

				if ($el.hasClass('loading')) {
					return;
				}

				$el.addClass('active').siblings('.active').removeClass('active');

				var filter = $el.attr('data-filter'),
					$wrapper = $this.find('.products-wrapper');

				var data = {
					nonce: $this.data('nonce'),
					text: $this.data('text'),
					type: $this.data('type')
				};

				data.product_deal = $this.data('settings');
				data.product_deal.product_cats = filter;

				data.pagination = $this.data('pagination');

				$this.addClass('loading');

				$.post(
					ajax_url,
					data,
					function (response) {
						if (!response) {
							return;
						}

						var $data = $(response.data),
							$products = $data.find('ul.products > li'),
							$button = $data.find('.load-more');

						$this.removeClass('loading');

						// Add classes before append products to grid
						$products.addClass('product');

						for (var index = 0; index < $products.length; index++) {
							$($products[index]).css('animation-delay', index * 100 + 100 + 'ms');
						}

						$products.addClass('teckzoneFadeInUp teckzoneAnimation');
						$button.css('animation-delay', $products.length * 100 + 200 + 'ms').addClass('teckzoneFadeIn teckzoneAnimation');
						$wrapper.children('div.woocommerce, .load-more').remove();
						$wrapper.append($data);
					}
				);
			});
		});
	};

	/**
	 *    Load Brands
	 */
	var loadBrands = function ($scope, $) {
		$scope.find('.tz-products-of-brands').each(function () {
			var $this = $(this),
				text = $this.data('text');

			$this.find('.load-more a').html('<span class="button-text">' + text + '</span>' + '<span class="teckzone-loading"></span>');

			$this.on('click', '.load-more a', function (e) {
				e.preventDefault();

				var $el = $(this),
					elementId = $el.closest('.elementor-element').data('id');

				if ($el.data('requestRunning')) {
					return;
				}

				$el.data('requestRunning', true);

				var $pagination = $el.closest('.load-more'),
					$products = $pagination.prev('.product-brands');

				$el.addClass('loading');

				$.get(
					$el.attr('href'),
					function (response) {
						var $wrapper = $(response).find('.elementor-element[data-id="' + elementId.toString() + '"]'),
							content = $wrapper.find('.product-brands').children('.brand-item-wrapper'),
							$pagination_html = $wrapper.find('.load-more').html();

						$pagination.html($pagination_html);

						for (var index = 0; index < content.length; index++) {
							$(content[index]).css({
								'animation-delay': index * 100 + 100 + 'ms'
							});
						}

						content.addClass('teckzoneFadeInUp');

						$products.append(content);

						getProductCarousel(content.closest('.tz-products-of-brands'));

						$pagination.find('a')
							.data('requestRunning', false)
							.html('<span class="button-text">' + text + '</span>' + '<span class="teckzone-loading"></span>');

						$el.removeClass('loading');

						if (!$pagination_html) {
							$pagination.addClass('loaded');
						}
					}
				);
			});
		});
	};

	var getProductCarousel = function ($selector, extendOptions = {}) {
		var $slider = $selector.find('ul.products,ul.product-list'),
			dataSettings = $selector.data('settings');

		var data = JSON.stringify(dataSettings);

		var options = {
			prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
			nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
		};

		$.extend(true, options, extendOptions);

		$slider.attr('data-slick', data);
		$slider.not('.slick-initialized').slick(options);
	};

	/**
	 * Mini Cart
	 */
	var miniCartPanel = function($scope, $) {
		$scope.find('.teckzone-cart-mobile').each(function () {
			var $selector = $(this),
				$cartMobile = $('.teckzone-cart-mobile').not($selector);

			if ( ! $selector.hasClass('teckzone-cart-mobile--panel') ) {
				return;
			}

			$selector.on( 'click', 'a.mini-cart-url', function(e){
				e.preventDefault();
				var $this = $(this);

				$cartMobile.removeClass('display-cart-panel');
				$cartMobile.find('a.mini-cart-url').removeClass('active');
				$cartMobile.find('.mini-cart-content').removeClass('open');
				
				$this.toggleClass('active');
				$this.siblings('.mini-cart-content').toggleClass('open');
				$selector.addClass('display-cart-panel');

				$(document.body)
				.addClass('open-cart-mobile')
				.removeClass('open-menu-mobile open-search-mobile')
				.trigger('teckzone_toggle_mini_cart_mobile');

				if ( ! $this.siblings('.mini-cart-content').hasClass('open') ) {
					$(document.body).removeClass('open-cart-mobile');
				}
			});

			$selector.find('.teckzone-off-canvas-layer').on('click', function (e) {
				var $this = $(this);
				
				$this.siblings('.mini-cart-content').removeClass('open');
				$this.siblings('a.mini-cart-url').removeClass('active');
				$selector.removeClass('display-cart-panel');
				$(document.body).removeClass('open-cart-mobile');
			});

			$selector.on('click', 'a.close-cart-panel', function (e) {
				e.preventDefault();
				var $this = $(this);

				$this.closest('.mini-cart-content').removeClass('open');
				$this.closest('.mini-cart-content').siblings('a.mini-cart-url').removeClass('active');
				$selector.removeClass('display-cart-panel');
				$(document.body).removeClass('open-cart-mobile');
			});

			$(document).on('teckzone_toggle_menu_mobile teckzone_toggle_search_mobile', function(){
				$selector.find('a.mini-cart-url').removeClass('active');
				$selector.find('.mini-cart-content').removeClass('open');
			});
		});
	};

	/**
	 * Menu Mobile
	 */
	var menuMobile = function ($scope, $) {
		$scope.find('.tz-menu-mobile--elementor').each(function () {
			var $mobileMenu = $(this),
				$otherMobileMenu = $('.tz-menu-mobile--elementor').not($mobileMenu);

			$mobileMenu.on('click', '.menu-icon-js', function (e) {
				e.preventDefault();
				var $this = $(this);

				$otherMobileMenu.removeClass('display-mobile-menu');
				$otherMobileMenu.find('.menu-icon-js').removeClass('active');
				$otherMobileMenu.find('.menu-mobile-wrapper').removeClass('open');

				$this.toggleClass('active');
				$this.siblings('.menu-mobile-wrapper').toggleClass('open');
				$mobileMenu.addClass('display-mobile-menu');

				$(document.body)
				.addClass('open-menu-mobile')
				.removeClass('open-cart-mobile open-search-mobile')
				.trigger('teckzone_toggle_menu_mobile');

				if ( ! $this.siblings('.menu-mobile-wrapper').hasClass('open') ) {
					$(document.body).removeClass('open-menu-mobile');
				}
			});

			$mobileMenu.find('.teckzone-off-canvas-layer').on('click', function () {
				var $this = $(this);

				$mobileMenu.removeClass('display-mobile-menu');
				$this.siblings('.menu-mobile-wrapper').removeClass('open');
				$this.siblings('.menu-icon-js').removeClass('active');
				$(document.body).removeClass('open-menu-mobile');
			});

			$mobileMenu.on('click', '.close-canvas-mobile-panel', function (e) {
				e.preventDefault();
				var $this = $(this);

				$mobileMenu.removeClass('display-mobile-menu');
				$this.closest('.menu-mobile-wrapper').removeClass('open');
				$this.closest('.menu-mobile-wrapper').siblings('.menu-icon-js').removeClass('active');
				$(document.body).removeClass('open-menu-mobile');
			});

			$(document).on('teckzone_toggle_mini_cart_mobile teckzone_toggle_search_mobile', function(){
				$mobileMenu.find('.menu-icon-js').removeClass('active');
				$mobileMenu.find('.menu-mobile-wrapper').removeClass('open');
			});

			// Sub Menu
			$mobileMenu.find('.menu .menu-item-has-children').prepend('<span class="toggle-menu-children"><i class="icon-chevron-down"></i></span>');

			$mobileMenu.on('click', '.toggle-menu-children', function (e) {
				e.preventDefault();
				openSubMenus($(this));
			});

			function openSubMenus($el) {
				$el.closest('li').siblings().find('ul').slideUp();
				$el.closest('li').siblings().removeClass('active');
				$el.closest('li').siblings().find('li').removeClass('active');

				$el.closest('li').children('ul').slideToggle();
				$el.closest('li').toggleClass('active');
			}
		});
	};

	/**
	 * Ajax Handle
	 */
	var productsCarouselAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-carousel');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductCarouselAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductCarouselAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-carousel-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsCarousel'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsCarousel2AjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-products-carousel-2');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductCarouselAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductCarouselAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-products-carousel-2-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsCarousel2'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsGridAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-products-grid');

		if (!$selector.hasClass('no-infinite')) {
			productGridInfinite($selector);
		} else {
			loadProductsGrid($selector);
		}

		function productGridInfinite($selector) {
			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductGridlAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductGridlAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-products-grid-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsGrid'
				},
				success: function (response) {
					$selector.html(response.data);
					loadProductsGrid($selector)
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsWithCategoryAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-with-category');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			bannerCarousel($selector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-with-category-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsWithCategory'
				},
				success: function (response) {
					$selector.html(response.data);
					bannerCarousel($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}

		function bannerCarousel( $selector ) {
			var $slider = $selector.find('.images-list');

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			$slider.not('.slick-initialized').slick(options);
		}
	};

	var productsWithCategory2AjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-with-category-2');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			bannerCarousel($selector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-with-category-2-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsWithCategory2'
				},
				success: function (response) {
					$selector.html(response.data);
					bannerCarousel($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}

		function bannerCarousel( $selector ) {
			var $slider = $selector.find('.images-list');

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			$slider.not('.slick-initialized').slick(options);
		}
	};

	var productsListCarouselAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-list-carousel'),
			extendOptions = {};

		extendOptions.appendArrows = $selector.find('.slick-arrows');
			
		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector, extendOptions);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-list-carousel-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsListCarousel'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector, extendOptions);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsCarouselWithBannerAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.teckzone-products-carousel-with-banner');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductCarouselAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductCarouselAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.teckzone-products-carousel-with-banner-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsCarouselWithBanner'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsCarouselWithBanner2AjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.teckzone-products-carousel-with-banner-2');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductCarouselAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductCarouselAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.teckzone-products-carousel-with-banner-2-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsCarouselWithBanner2'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsWithCategoryCarouselAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.teckzone-products-carousel-with-category'),
			$bannersSelector = $selector.find('.images-list'),
			$productsSelector = $selector.find('.products-box');

		var options = {
			prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
			nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
		};

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			$bannersSelector.not('.slick-initialized').slick(options);
			getProductCarousel($productsSelector);
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductCarouselAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductCarouselAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.teckzone-products-carousel-with-category-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');

			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsCarouselWithCat'
				},
				success: function (response) {
					$selector.html(response.data);

					var $imageList = $selector.find('.images-list');

					$imageList.not('.slick-initialized').slick(options);
					getProductCarousel($selector);
					//$selector.removeAttr('data-settings');
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsTabCarouselAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-tab-carousel');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
			productTabsActive($selector);
			$selector.find('.tabs-nav').on('click', 'a', function (e) {
				getProductsListAJAXHandler($(this), $selector);
			});
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-tab-carousel-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');

			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsTabCarousel'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					productTabsActive($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});

			$( document.body ).on( 'teckzone_get_products_ajax_success', function () {
				$selector.find('.slick-slider').slick('setPosition');
			});
		}
	};

	var productsTabCarousel2AjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-tab-carousel-2');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
			productTabsActive($selector);
			$selector.find('.tabs-nav').on('click', 'a', function (e) {
				getProductsListAJAXHandler($(this), $selector);
			});
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-tab-carousel-2-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');

			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsTabCarousel2'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					productTabsActive($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});

			$( document.body ).on( 'teckzone_get_products_ajax_success', function () {
				$selector.find('.slick-slider').slick('setPosition');
			});
		}
	};

	var productsTabCarousel3AjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-tab-carousel-3');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
			productTabsActive($selector);
			$selector.find('.tabs-nav').on('click', 'a', function (e) {
				getProductsAJAXHandler($(this), $selector);
			});
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-tab-carousel-3-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');

			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsTabCarousel3'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					productTabsActive($selector);
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});

			$( document.body ).on( 'teckzone_get_products_ajax_success', function () {
				$selector.find('.slick-slider').slick('setPosition');
			});
		}
	};

	var productsDealGridAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-deals-grid');

		if (!$selector.hasClass('no-infinite')) {
			productGridInfinite($selector);
		} else {
			loadProductsDeals($selector);
			$selector.find('.teckzone-countdown').mf_countdown();
		}

		function productGridInfinite($selector) {
			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductGridlAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductGridlAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-deals-grid-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsDealGrid'
				},
				success: function (response) {
					$selector.html(response.data);
					loadProductsDeals($selector);
					$selector.find('.teckzone-countdown').mf_countdown();
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var productsDealCarouselAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.tz-product-deals-day');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
			$selector.find('.teckzone-countdown').mf_countdown();
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductCarouselAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductCarouselAjax($selector) {

			if (typeof tz_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.tz-product-deals-day-loading').data('settings'),
				ajax_url = tz_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'tz_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsDealCarousel'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					$selector.find('.teckzone-countdown').mf_countdown();
					$(document.body).trigger('teckzone_get_products_ajax_success');
				}
			});
		}
	};

	var collapseEffect = function($scope, $) {
		var $seleclor = $scope.find( '.teckzone-list-links' ),
			data = $seleclor.data( 'effect' );

		if ( typeof data === 'undefined' || data.collapse_effect !== 'yes' ) {
			return;
		}

		var currentDeviceMode = elementorFrontend.getCurrentDeviceMode(),
			activeDevices = data.collapse_effect_on,
			status = data.content_status;

		if (-1 !== activeDevices.indexOf(currentDeviceMode)) {
			active($seleclor);
		} else {
			deactive($seleclor);
		}

		function active( $seleclor ) {
			$seleclor.find( '.title' ).css({
				'cursor' : 'pointer'
			});

			if ( status !== 'yes' ) {
				$seleclor.find( '.list-wrapper' ).hide();
			} else {
				$seleclor.find( '.title' ).addClass( 'open' );
				$seleclor.find( '.list-wrapper' ).addClass( 'open' );
			}

			$seleclor.on( 'click', '.title', function(e){
				e.preventDefault();

				var $this = $(this);

				$this.toggleClass( 'open' );
				$this.siblings( '.list-wrapper' ).slideToggle().toggleClass( 'open' );
			} );
		}

		function deactive( $seleclor ) {
			$seleclor.find( '.title .teckzone-icon' ).html('');
		}
	}

	var map = function ($scope, $) {
        $scope.find('.teckzone-map').each(function () {
            var el = $(this),
                elsMap = el.data('map'),
                wrapper = $(this).attr('id');

            mapboxgl.accessToken = elsMap.token;

            var mapboxClient = mapboxSdk( { accessToken: mapboxgl.accessToken } ),
                local = elsMap.local;

            mapboxClient.geocoding.forwardGeocode( {
                query       : local,
                autocomplete: false,
                limit       : 1
            } )
                .send()
                .then( function ( response ) {
                    if ( response && response.body && response.body.features && response.body.features.length ) {
                        var feature = response.body.features[0];

                        var map = new mapboxgl.Map( {
                            container: wrapper,
                            style    : 'mapbox://styles/mapbox/'+ elsMap.mode +'-v10',
                            center   : feature.center,
                            zoom     : elsMap.zom
                        } );
                        map.on( 'load', function () {
                            map.loadImage( elsMap.marker, function ( error, image ) {
                                if ( error ) throw error;
                                map.addImage( 'marker', image );
                                map.addLayer( {
                                    "id"    : "points",
                                    "type"  : "symbol",
                                    "source": {
                                        "type": "geojson",
                                        "data": {
                                            "type"    : "FeatureCollection",
                                            "features": [{
                                                "type"    : "Feature",
                                                "geometry": {
                                                    "type"       : "Point",
                                                    "coordinates": feature.center
                                                }
                                            }]
                                        }
                                    },
                                    "layout": {
                                        "icon-image": "marker",
                                        "icon-size" : 1
                                    }
                                } );
                            } );
                        } );

                        // disable map zoom when using scroll
                        map.scrollZoom.disable();

                        var geocoder = new MapboxGeocoder( {
                            accessToken: mapboxgl.accessToken
                        } );

                        map.addControl( geocoder );

                        // After the map style has loaded on the page, add a source layer and default
                        // styling for a single point.
                        map.on( 'load', function () {
                            map.addSource( 'single-point', {
                                "type": "geojson",
                                "data": {
                                    "type"    : "FeatureCollection",
                                    "features": []
                                }
                            } );

                            map.addLayer( {
                                "id"    : "point",
                                "source": "single-point",
                                "type"  : "circle",
                                "paint" : {
                                    "circle-radius": 10,
                                    "circle-color" : "#007cbf"
                                }
                            } );

                            map.setPaintProperty( 'water', 'fill-color', elsMap.color_1 );
                            map.setPaintProperty( 'building', 'fill-color', elsMap.color_2 );

                            map.addControl( new mapboxgl.NavigationControl() );
                            // Listen for the `result` event from the MapboxGeocoder that is triggered when a user
                            // makes a selection and add a symbol that matches the result.
                            geocoder.on( 'result', function ( ev ) {
                                map.getSource( 'single-point' ).setData( ev.result.geometry );
                            } );
                        } );
                    }
                } );
        });
    };

	/**
	 * Elementor JS Hooks
	 */
	$(window).on("elementor/frontend/init", function () {
		// Countdown
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-countdown.default",
			countDownHandler
		);

		// Counter
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-counter.default",
			counterHandler
		);

		// Masonry
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-image-masonry.default",
			imageMasonry
		);

		// Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-image-slides.default",
			imageCarousel
		);

		// Timeline
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-timeline.default",
			timeline
		);

		// Testimonials
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-testimonials.default",
			testimonial
		);

		// Testimonials 2
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-testimonials-2.default",
			testimonial2
		);

		// Testimonials 3
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/teckzone-testimonials-3.default",
			testimonial2
		);

		// Tab
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-tab-list.default",
			tab
		);

		// Video
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-video.default",
			videoLightBox
		);

		// Slider
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-slides.default",
			slideCarousel
		);

		// Posts Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-posts-carousel.default",
			postsCarousel
		);

		// Image Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-images-carousel.default",
			imagesCarousel
		);

		// Trending Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-trending-search-carousel.default",
			trendingSearchCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-trending-search-carousel-2.default",
			trendingSearchCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-trending-search-carousel-3.default",
			trendingSearchCarousel
		);

		// Promotion
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-promotion.default",
			promotionHandler
		);

		// Search Form
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-search-form.default",
			searchFormHandler
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-search-form-mobile.default",
			searchFormHandler
		);
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-search-form-mobile.default",
			searchTogglePanel
		);

		// Recently View Products
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-recently-viewed-products.default",
			recentlyViewedProducts
		);
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-content-recently-viewed.default",
			recentlyViewedProducts
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/tz-product-recently-viewed-carousel.default",
			recentlyViewedProductsAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-footer-viewed-products.default",
			recentlyViewedProducts
		);
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/tz-product-recently-viewed-carousel.default",
			recentViewedProductsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/tz-product-recently-viewed-carousel-2.default",
			recentViewedProductsCarousel
		);

		// Product Deals Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel.default",
			productsDealCarouselAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-2.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-3.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-2.default",
			productDealsCarousel2Handler
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-3.default",
			productDealsCarousel3Handler
		);

		// Product Deals Grid
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-grid.default",
			productsDealGridAjaxHandle
		);

		// Product Grid
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-grid.default",
			productsGridAjaxHandle
		);

		// Product Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-carousel.default",
			productsCarouselAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-2.default",
			productsCarousel2AjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-with-category.default",
			productsWithCategoryCarouselAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-with-banner.default",
			productsCarouselWithBannerAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-with-banner-2.default",
			productsCarouselWithBanner2AjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-list-carousel.default",
			productsListCarouselAjaxHandle
		);

		// Product With Category
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-with-category.default",
			productsWithCategoryAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-with-category-2.default",
			productsWithCategory2AjaxHandle
		);

		// Product tab carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel.default",
			productsTabCarouselAjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel-2.default",
			productsTabCarousel2AjaxHandle
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel-3.default",
			productsTabCarousel3AjaxHandle
		);

		// Product Categories Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-categories-carousel.default",
			productsCategoriesCarousel
		);

		// Product Brand
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-brands.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-brands.default",
			loadBrands
		);

		// Cart Panel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/teckzone-cart-mobile.default",
			miniCartPanel
		);

		// Menu Mobile
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-menu-mobile.default",
			menuMobile
		);

		// List Link Collapes Effect
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-list-links.default",
			collapseEffect
		);

		// map
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/techzone-map.default",
            map
        );
	});
})
(jQuery);